/**
 * <p>
 * This package contains the public API into the CmdLineImpl class.  Create an instance of the class using any one of
 * the
 * specialized constructors.
 * </p>
 * <h3>Build instructions</h3>
 * <ul>
 * <li>version number - update the version number in these files
 * <ul>
 * <li>pom.xml</li><li>package-info.java</li><li>docs/conf.py</li>
 * </ul>
 * <li>submit a pull-request to master</li>
 * <li>after the bitbucket pipeline is complete then got to bintray and push to maven central</li>
 * </ul>
 *
 * <p><a href="https://commandlineinterface.readthedocs.io">Read the documentation</a></p>
 *
 * @author cdegreef
 * @since 6.5.4
 */
package com.codetaco.cli;
