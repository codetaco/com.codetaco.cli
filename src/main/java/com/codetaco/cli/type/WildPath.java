package com.codetaco.cli.type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A path pattern can contain three types of wild cards; ?, *, and **. This is
 * not a regex.
 */
public final class WildPath {
    private static final Logger logger = LoggerFactory.getLogger(WildPath.class);

    private static final boolean DEBUG = false;

    private final static Matcher pathSegmentMatcher = Pattern.compile("[^\\\\/]+").matcher("");
    private final static Matcher wildCardInSegmentMatcher = Pattern.compile("[*?]").matcher("");
    private final static Matcher firstSegmentIsAbsoluteMatcher = Pattern.compile("^([\\\\/]|.:).*").matcher("");

    private static Pattern convertDirWildCardToRegx(String wildcard) {
        if (wildcard == null || wildcard.length() == 0) {
            return Pattern.compile("\\.");
        }

        String regex;
        regex = wildcard;

        regex = regex.replaceAll("\\\\", "\\\\\\\\");
        regex = regex.replaceAll("[.]", "\\\\.");
        regex = regex.replaceAll("[?]", ".");
        regex = regex.replaceAll("\\*\\*", ".`");
        regex = regex.replaceAll("[*]", "[^\\\\\\\\]*");
        regex = regex.replaceAll("`", "*");

        return Pattern.compile("^" + regex + "$", Pattern.CASE_INSENSITIVE);
    }

    static Pattern convertFileWildCardToRegx(String wildcard) {
        String regex = wildcard;
        regex = regex.replaceAll("[.]", "\\\\.");
        regex = regex.replaceAll("[?]", ".");
        regex = regex.replaceAll("[*]", ".*");
        return Pattern.compile("^" + regex + "$", Pattern.CASE_INSENSITIVE);
    }

    private List<String> pathSegments = new ArrayList<>();
    private String startingPath;
    private Matcher directoryMatcher;
    private Matcher fileMatcher;
    private List<File> files = new ArrayList<>();
    private boolean directorySearchRequired;

    private int scanDirCount;
    private int scanFileCount;

    public WildPath(String pattern) {
        parsePattern(pattern);

        File exactRequest = new File(pattern);
        File dir = exactRequest.getParentFile();
        if (dir == null) {
            dir = new File(".");
        }

        Pattern dirPattern = convertDirWildCardToRegx(dir.toString());
        Pattern filePattern = convertFileWildCardToRegx(exactRequest.getName());

        logger.trace("user supplied {}", pattern);
        logger.trace("dirPattern {}", dirPattern.toString());
        logger.trace("filePattern {}", filePattern.toString());

        directoryMatcher = dirPattern.matcher("");
        fileMatcher = filePattern.matcher("");
    }

    public List<File> files() {
        List<File> files1 = files(new File(startingPath));
        logger.trace("wildfile counts: dir({}) files({}) matched({})",
                     scanDirCount, scanFileCount, files1.size());
        return files1;
    }

    private List<File> files(File directory) {
        scanDirCount++;

        File[] foundDirectories = directory.listFiles(
          new FileFilter() {
              @Override
              public boolean accept(File pathname) {
                  return pathname.isDirectory();
              }
          });
        File[] foundFiles = null;
        {
            foundFiles = directory.listFiles(
              new FileFilter() {
                  @Override
                  public boolean accept(File pathname) {
                      if (pathname.isDirectory()) {
                          return false;
                      }

                      scanFileCount++;

                      // logger.trace("matching file " +
                      // pathname.getPath());

                      try {
                          directoryMatcher.reset(pathname.getParentFile().getPath());
                          boolean directoryMatchFlag = directoryMatcher.matches();
                          // if (directoryMatchFlag)
                          // logger.trace(" dir matches: {}",
                          // pathname.getParentFile().getPath());
                          // else
                          // logger.trace(" dir NOT matching: {} with {} "
                          // , pathname.getParentFile().getPath()
                          // , directoryMatcher.pattern().pattern());
                          if (directoryMatchFlag) {
                              fileMatcher.reset(pathname.getName());
                              return fileMatcher.matches();
                          }
                          return false;
                      } finally {
                          if (DEBUG) {
                              System.out.println();
                          }
                      }
                  }
              });
        }

        if (isDirectorySearchRequired() && foundDirectories != null) {
            for (File oneDir : foundDirectories) {
                files(oneDir);
            }
        }

        if (foundFiles != null) {
            for (File oneFile : foundFiles) {
                files.add(oneFile);
            }
        }
        return files;
    }

    private boolean isDirectorySearchRequired() {
        return directorySearchRequired;
    }

    private synchronized void parsePattern(String pattern) {
        pathSegmentMatcher.reset(pattern);
        while (pathSegmentMatcher.find()) {
            pathSegments.add(pathSegmentMatcher.group());
        }
        /*
         * The last "segment" must always be the file specification.
         */
        pathSegments.remove(pathSegments.size() - 1);
        /*
         * Find the longest absolute path from the beginning of the specified
         * path.
         */
        StringBuilder startingPathBuilder = new StringBuilder();
        firstSegmentIsAbsoluteMatcher.reset(pattern);
        if (firstSegmentIsAbsoluteMatcher.matches()) {
            startingPathBuilder.append("/");
        }
        for (String segment : pathSegments) {
            wildCardInSegmentMatcher.reset(segment);
            if (wildCardInSegmentMatcher.find()) {
                directorySearchRequired = true;
                break;
            }
            startingPathBuilder.append(segment);
            startingPathBuilder.append("/");
        }
        startingPath = startingPathBuilder.toString();
        if (startingPath.length() == 0) {
            startingPath = ".";
        }
    }

    String startingPath() {
        return startingPath;
    }
}
