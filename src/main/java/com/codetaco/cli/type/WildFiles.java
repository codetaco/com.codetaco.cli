package com.codetaco.cli.type;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Wild files.
 */
public final class WildFiles {

    private final List<String> userSuppliedPatterns;
    private List<File> files;

    /**
     * Instantiates a new Wild files.
     */
    public WildFiles() {
        userSuppliedPatterns = new ArrayList<>();
    }

    /**
     * This is used in Sort to override the provided list of files with one of them for each run.
     *
     * @param aSingleKnownFile the a single known file
     */
    public WildFiles(File aSingleKnownFile) {
        userSuppliedPatterns = new ArrayList<>();
        files = new ArrayList<>();
        files.add(aSingleKnownFile);
    }

    public void addPattern(String userPattern) {
        userSuppliedPatterns.add(userPattern);
    }

    /**
     * Files list.
     *
     * @return the list
     * @throws ParseException the parse exception
     * @throws IOException    the io exception
     */
    public List<File> files() throws ParseException, IOException {
        if (files == null) {
            files = new ArrayList<>();
            for (String pattern : userSuppliedPatterns) {
                files.addAll(new WildPath(pattern).files());
            }
        }
        return files;
    }

    public String getPattern(int userSuppliedIndex) {
        return userSuppliedPatterns.get(userSuppliedIndex);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        boolean firstTime = true;
        for (String pattern : userSuppliedPatterns) {
            if (!firstTime) {
                sb.append(" ");
            }
            sb.append(pattern);
            firstTime = false;
        }
        return sb.toString();
    }
}
