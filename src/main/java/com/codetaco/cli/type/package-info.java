/**
 * This is a package of special variable types that provide commonly needed
 * command-line interfaces.  Make a variable of one of these types and annotated
 * it with the @Arg annotation to have it processed.
 *
 * @author cdegreef
 * @since 6.5.4
 */
package com.codetaco.cli.type;
