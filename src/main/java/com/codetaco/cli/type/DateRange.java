package com.codetaco.cli.type;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.cli.annotation.ArgCallback;
import com.codetaco.date.CalendarFactory;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * This class provides a range of two dates and methods to determine if another date is within the range. This is
 * inclusive
 * of the dates on either end of the range.
 */
public final class DateRange {

    @Arg(shortName = 's')
    private LocalDate startDate;

    @Arg
    private String[] startAdjustments;

    @Arg(shortName = 'e')
    private LocalDate endDate;

    @Arg
    private String[] endAdjustments;

    /**
     * Instantiates a new Date range.
     */
    public DateRange() {
    }

    LocalDate getStartDate() {
        return startDate;
    }

    LocalDate getEndDate() {
        return endDate;
    }

    /**
     * Contains boolean.
     *
     * @param date the date
     * @return the boolean
     */
    public boolean contains(Date date) {
        return contains(CalendarFactory.asLocalDate(date));
    }

    /**
     * Contains boolean.
     *
     * @param date the date
     * @return the boolean
     */
    public boolean contains(LocalDate date) {
        if (startDate.compareTo(date) > 0) {
            return false;
        }
        return !(endDate.compareTo(date) < 0);
    }

    /**
     * Contains boolean.
     *
     * @param date the date
     * @return the boolean
     */
    public boolean contains(LocalDateTime date) {
        return contains(date.toLocalDate());
    }

    /**
     * Contains boolean.
     *
     * @param date the date
     * @return the boolean
     */
    public boolean contains(ZonedDateTime date) {
        return contains(CalendarFactory.asLocalDate(date));
    }

    /**
     * Contains boolean.
     *
     * @param date the date
     * @return the boolean
     */
    public boolean contains(String date) {
        return contains(CalendarFactory.asLocalDate(date));
    }

    @ArgCallback(postParse = true)
    private void prepareAfterParse() throws ParseException {
        if (startDate == null) {
            if (endDate == null) {
                throw new ParseException("Start or End date must be set on a DateRange", -1);
            } else {
                if (endAdjustments != null) {
                    endDate = CalendarFactory.asLocalDate(endDate, endAdjustments);
                }
                if (startAdjustments != null) {
                    startDate = CalendarFactory.asLocalDate(endDate, startAdjustments);
                } else {
                    startDate = CalendarFactory.asLocalDate(endDate);
                }
            }
        } else {
            if (startAdjustments != null) {
                startDate = CalendarFactory.asLocalDate(startDate, startAdjustments);
            }
            if (endDate == null) {
                if (endAdjustments != null) {
                    endDate = CalendarFactory.asLocalDate(startDate, endAdjustments);
                } else {
                    endDate = CalendarFactory.asLocalDate(startDate);
                }
            } else if (endAdjustments != null) {
                endDate = CalendarFactory.asLocalDate(endDate, endAdjustments);
            }
        }
        if (endDate.isBefore(startDate)) {
            throw new ParseException("start date must be <= end date in a DateRange", 0);
        }
    }

}
