package com.codetaco.cli.type;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.cli.annotation.ArgCallback;
import com.codetaco.date.CalendarFactory;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * This class provides a range of two times and methods to determine if another time is within the range. This is
 * inclusive
 * of the times on either end of the range.  If the provided parameters include a date part then that date part is
 * ignored
 * for purposes of the comparisons.
 * <p>
 * TimeRange class.
 * </p>
 */
public final class TimeRange {

    @Arg(shortName = 's')
    private LocalTime startTime;

    @Arg
    private String[] startAdjustments;

    @Arg(shortName = 'e')
    private LocalTime endTime;

    @Arg
    private String[] endAdjustments;

    LocalTime getStartTime() {
        return startTime;
    }

    LocalTime getEndTime() {
        return endTime;
    }

    /**
     * Instantiates a new Time range.
     */
    public TimeRange() {
    }

    /**
     * Contains boolean.
     *
     * @param date the date
     * @return the boolean
     */
    public boolean contains(Date date) {
        return contains(CalendarFactory.asLocalTime(date));
    }

    /**
     * Contains boolean.
     *
     * @param date the date
     * @return the boolean
     */
    public boolean contains(LocalTime date) {
        if (startTime.compareTo(date) > 0) {
            return false;
        }
        return !(endTime.compareTo(date) < 0);
    }

    /**
     * Contains boolean.
     *
     * @param date the date
     * @return the boolean
     */
    public boolean contains(LocalDateTime date) {
        return contains(CalendarFactory.asLocalTime(date));
    }

    /**
     * Contains boolean.
     *
     * @param date the date
     * @return the boolean
     */
    public boolean contains(ZonedDateTime date) {
        return contains(CalendarFactory.asLocalTime(date));
    }

    /**
     * Contains boolean.
     *
     * @param date the date
     * @return the boolean
     */
    public boolean contains(String date) {
        return contains(CalendarFactory.asLocalTime(date));
    }

    @ArgCallback(postParse = true)
    private void prepareAfterParse() throws ParseException {
        if (startTime == null) {
            if (endTime == null) {
                throw new ParseException("Start or End time must be set on a TimeRange", -1);
            } else {
                if (endAdjustments != null) {
                    endTime = CalendarFactory.asLocalTime(endTime, endAdjustments);
                }
                if (startAdjustments != null) {
                    startTime = CalendarFactory.asLocalTime(endTime, startAdjustments);
                } else {
                    startTime = CalendarFactory.asLocalTime(endTime);
                }
            }
        } else {
            if (startAdjustments != null) {
                startTime = CalendarFactory.asLocalTime(startTime, startAdjustments);
            }
            if (endTime == null) {
                if (endAdjustments != null) {
                    endTime = CalendarFactory.asLocalTime(startTime, endAdjustments);
                } else {
                    endTime = CalendarFactory.asLocalTime(startTime);
                }
            } else if (endAdjustments != null) {
                endTime = CalendarFactory.asLocalTime(endTime, endAdjustments);
            }
        }
        if (endTime.isBefore(startTime)) {
            throw new ParseException("start Time must be <= end time in a TimeRange", 0);
        }
    }

}
