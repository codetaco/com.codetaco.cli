package com.codetaco.cli;

/**
 * The enum Parser type.
 */
public enum ParserType {
    /**
     * Command line parser type.
     */
    COMMAND_LINE,
    /**
     * Xml parser type.
     */
    XML,
    /**
     * Name space parser type.
     */
    NAME_SPACE
}
