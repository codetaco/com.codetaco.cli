/**
 * This is a package of annotations to use the command line parser. @Arg is the annotation to put on each
 * variable to make it available for this library.
 *
 * @author cdegreef
 * @since 6.5.4
 */
package com.codetaco.cli.annotation;
