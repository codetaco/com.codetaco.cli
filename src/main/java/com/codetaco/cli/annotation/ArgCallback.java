package com.codetaco.cli.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation to mark a method that will be called at the specified time
 * in the processing of the cli parsing.
 *
 * @author Chris DeGreef fedupforone@gmail.com
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ArgCallback {
    boolean postParse() default false;
}
