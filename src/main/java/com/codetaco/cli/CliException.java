package com.codetaco.cli;

/**
 * The type Cli exception.
 */
public final class CliException extends RuntimeException {

    /**
     * The type Builder.
     */
    public static class Builder {

        private Throwable cause;

        /**
         * Cause builder.
         *
         * @param cause the cause
         * @return the builder
         */
        public Builder cause(Throwable cause) {
            this.cause = cause;
            return this;
        }

        /**
         * Build cli exception.
         *
         * @return the cli exception
         */
        public CliException build() {
            if (cause instanceof CliException) {
                return (CliException) cause;
            }
            return new CliException(cause);
        }
    }

    /**
     * Builder builder.
     *
     * @return the builder
     */
    static public Builder builder() {
        return new Builder();
    }

    private CliException() {
    }

    private CliException(String message) {
        super(message);
    }

    private CliException(String message, Throwable cause) {
        super(message, cause);
    }

    private CliException(Throwable cause) {
        super(cause);
    }

    @Override
    public String getMessage() {
        return getCause().getMessage();
    }

    private CliException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
