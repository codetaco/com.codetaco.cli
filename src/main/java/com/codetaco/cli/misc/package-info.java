/**
 * This is a package of utility classes that are used by this implementation but can
 * also be used of on the public API.
 *
 * @author cdegreef
 * @since 6.5.4
 */
package com.codetaco.cli.misc;
