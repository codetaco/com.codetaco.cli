package com.codetaco.cli.misc;

/**
 * The type Byte util.
 */
public final class ByteUtil {
    /**
     * These literals represent the number that happens to be the same as its
     * index.
     */
    private static final String[] ByteLiteral = {
      "null",
      "soh",
      "stx",
      "etx",
      "eot",
      "enq",
      "ack",
      "bel",
      "bs",
      "ht",
      "lf",
      "vt",
      "ff",
      "cr",
      "so",
      "si",
      "dle",
      "dc1",
      "dc2",
      "dc3",
      "dc4",
      "nak",
      "syn",
      "etb",
      "can",
      "em",
      "sub",
      "esc",
      "fs",
      "gs",
      "rs",
      "us",
      "sp"
    };

    /**
     * As literal string.
     *
     * @param aByte the a byte
     * @return the string
     */
    public static String asLiteral(byte aByte) {
        if (aByte < ByteLiteral.length) {
            return ByteLiteral[aByte] + "(" + (int) aByte + ")";
        }
        return (char) aByte + "(" + (int) aByte + ")";
    }

    /**
     * Byte for literal byte.
     *
     * @param literal the literal
     * @return the byte
     */
    public static byte byteForLiteral(String literal) {
        for (int b = 0; b < ByteLiteral.length; b++) {
            if (ByteLiteral[b].equalsIgnoreCase(literal)) {
                return (byte) b;
            }
        }
        return -1;
    }

    /**
     * Byte to lit string.
     *
     * @param number the number
     * @return the string
     */
    public static String byteToLit(String number) {
        byte aByte = (byte) Integer.parseInt(number);

        if (aByte < ByteLiteral.length) {
            return ByteLiteral[aByte];
        }
        return "" + (int) aByte;
    }

    /**
     * Append string builder.
     *
     * @param sb the sb
     * @return the string builder
     */
    public static StringBuilder append(StringBuilder sb) {
        for (int b = 0; b < ByteLiteral.length; b++) {
            sb.append(ByteLiteral[b].toUpperCase());
            sb.append("(");
            sb.append(b);
            sb.append(") ");
        }
        return sb;
    }
}
