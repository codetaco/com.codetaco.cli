package com.codetaco.cli.impl.criteria;

import com.codetaco.cli.impl.usage.UsageBuilder;

import java.util.List;

/**
 * <p>
 * ListCriteria class.
 * </p>
 *
 * @param <E>
 * @author Chris DeGreef fedupforone@gmail.com
 */
public class ListCriteria<E> implements ICmdLineArgCriteria<E> {
    List<E> list;

    /**
     * <p>
     * Constructor for ListCriteria.
     * </p>
     *
     * @param listOfValidValues a {@link java.util.List} object.
     */
    public ListCriteria(List<E> listOfValidValues) {
        list = listOfValidValues;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void asDefinitionText(StringBuilder sb) {
        sb.append(" --list");
        for (E item : list) {
            sb.append(" '");
            sb.append(item.toString());
            sb.append("'");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void asSetter(StringBuilder sb) {
        sb.append(".setListCriteria(new String[] {");
        boolean firstTime = true;
        for (E item : list) {
            if (!firstTime) {
                sb.append(",");
            }
            sb.append("\"");
            sb.append(item.toString());
            sb.append("\"");
            firstTime = false;
        }
        sb.append("})");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ListCriteria<E> clone() throws CloneNotSupportedException {
        ListCriteria<E> clone = (ListCriteria<E>) super.clone();
        return clone;
    }

    /**
     * <p>
     * Getter for the field <code>list</code>.
     * </p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<E> getList() {
        return list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSelected(Comparable<E> value, boolean caseSensitive) {
        if (!list.isEmpty() && list.get(0) instanceof String
              && !caseSensitive) {
            return list.contains(((String) value).toLowerCase());
        }
        return list.contains(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E normalizeValue(E value, boolean caseSensitive) {
        E bestSoFar = null;

        /*
         * Only string lists make sense to normalize.
         */
        if (!(value instanceof String)) {
            return value;
        }

        String stringValue = (String) value;
        for (E listItem : list) {
            String stringListItem = (String) listItem;

            stringValue = stringValue.toLowerCase();
            stringListItem = stringListItem.toLowerCase();

            if (stringListItem.startsWith(stringValue)) {
                if (stringListItem.length() == stringValue.length()) {
                    /*
                     * exactly matches something in the list, take the list item
                     * so that true normalization occurs
                     */
                    if (caseSensitive) {
                        return listItem;
                    }
                    return value;
                }
                if (bestSoFar != null) {
                    /*
                     * Ambiguous list value. This will cause an error later.
                     */
                    return value;
                }
                /*
                 * We must take what is in the list in cases where the user
                 * enters in a shortened version of the listitem.
                 */
                bestSoFar = listItem;
            }
        }
        if (bestSoFar != null)
            /*
             * Only one thing matched so lets return the normalized value
             * instead. This will cause the validation to succeed.
             */ {
            return bestSoFar;
        }
        /*
         * This will most likely cause a validation error later.
         */
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        if (list == null) {
            return "null list";
        }
        return list.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void usage(UsageBuilder str, int indentLevel) {
        str.append("Possible choices are: ");
        for (E item : list) {
            str.append(item.toString()).append(" ");
        }
    }

}
