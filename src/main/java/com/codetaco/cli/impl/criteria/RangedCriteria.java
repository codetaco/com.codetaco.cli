package com.codetaco.cli.impl.criteria;

import com.codetaco.cli.impl.usage.UsageBuilder;

/**
 * <p>
 * RangedCriteria class.
 * </p>
 *
 * @param <E>
 * @author Chris DeGreef fedupforone@gmail.com
 */
public class RangedCriteria<E> implements ICmdLineArgCriteria<E> {

    E min;
    E max;

    /**
     * <p>
     * Constructor for RangedCriteria.
     * </p>
     *
     * @param inclusiveMin a E object.
     * @param inclusiveMax a E object.
     */
    public RangedCriteria(E inclusiveMin, E inclusiveMax) {
        min = inclusiveMin;
        max = inclusiveMax;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void asDefinitionText(StringBuilder sb) {
        if (min != null) {
            if (max != null) {
                sb.append(" --range '" + min.toString() + "' '" + max.toString() + "'");
            } else {
                sb.append(" --range '" + min.toString() + "'");
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void asSetter(StringBuilder sb) {
        sb.append(".setRangeCriteria(new String[] {");
        if (min != null) {
            if (max != null) {
                sb.append("\"" + min.toString() + "\", \"" + max.toString() + "\"");
            } else {
                sb.append("\"" + min.toString() + "\"");
            }
        } else {
            sb.append("\"" + max.toString() + "\"");
        }
        sb.append("})");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RangedCriteria<E> clone() throws CloneNotSupportedException {
        RangedCriteria<E> clone = (RangedCriteria<E>) super.clone();
        return clone;
    }

    /**
     * <p>
     * Getter for the field <code>max</code>.
     * </p>
     *
     * @return a E object.
     */
    public E getMax() {
        return max;
    }

    /**
     * <p>
     * Getter for the field <code>min</code>.
     * </p>
     *
     * @return a E object.
     */
    public E getMin() {
        return min;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSelected(Comparable<E> value, boolean caseSensitive) {
        if (value instanceof String) {
            if (min != null) {
                if (((String) value).toLowerCase().compareTo((String) min) < 0) {
                    return false;
                }
            }
            if (max != null) {
                return ((String) value).toLowerCase().compareTo((String) max) <= 0;
            }
        } else {
            if (min != null) {
                if (value.compareTo(min) < 0) {
                    return false;
                }
            }
            if (max != null) {
                return value.compareTo(max) <= 0;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Normalization is not possible for ranges.
     */
    @Override
    public E normalizeValue(E value, boolean caseSensitive) {
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void usage(UsageBuilder str, int indentLevel) {
        if (min != null) {
            if (max != null) {
                str.append("The value must be from " + min.toString() + " to " + max.toString() + " inclusive");
            } else {
                str.append("The value must be at least " + min.toString() + ".");
            }
        } else {
            str.append("The value must be less than or equal to " + max.toString() + ".");
        }
    }
}
