package com.codetaco.cli.impl;

import com.codetaco.cli.impl.parser.IParserInput;
import com.codetaco.cli.impl.type.ICmdLineArg;

import java.io.File;
import java.text.ParseException;
import java.util.List;

public interface ICmdLine extends Comparable<ICmdLine>, ICmdLineArg<ICmdLine> {

    void add(ICmdLineArg<?> arg);

    void add(int index, ICmdLineArg<?> arg);

    void addDefaultIncludeDirectory(File directory);

    List<ICmdLineArg<?>> allArgs();

    ICmdLineArg<?> arg(String commandToken);

    ICmdLineArg<?> argForVariableName(String variableName);

    void assignVariables(Object target);

    char getCommandPrefix();

    String getName();

    List<ParseException> getParseExceptions();

    int indexOf(ICmdLineArg<?> arg);

    Object parse(IParserInput cmd);

    Object parse(IParserInput cmd, Object target);

    Object parse(Object target, String... args);

    Object parse(String... args);

    void pull(Object variableSource);

    void remove(ICmdLineArg<?> arg);

    void remove(int argIndex);

    @Override
    ICmdLine clone() throws CloneNotSupportedException;

    @Override
    int size();
}
