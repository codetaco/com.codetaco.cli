package com.codetaco.cli.impl.directive;

import com.codetaco.cli.impl.input.Token;

import java.io.IOException;
import java.text.ParseException;

public abstract class DirectiveCommand {
    final protected String data;

    public DirectiveCommand(String _data) {
        this.data = _data;
    }

    abstract Token replaceToken(Token[] tokens, int replacingFromTokenIndex, int replaceToTokenIndex)
      throws ParseException, IOException;
}
