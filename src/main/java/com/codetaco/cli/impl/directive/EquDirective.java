package com.codetaco.cli.impl.directive;

import com.codetaco.cli.impl.input.Token;
import com.codetaco.date.impl.TemporalHelper;
import com.codetaco.math.Equ;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;

/**
 * This class parses phrases that will be used to run an equation. That result
 * is returned as a literal token to be inserted into the command line at the
 * same position as this directive.
 *
 * @author Chris DeGreef fedupforone@gmail.com
 */
public class EquDirective extends DirectiveCommand {
    private final static Logger logger = LoggerFactory.getLogger(EquDirective.class.getName());

    public EquDirective(String data) {
        super(data);
    }

    @Override
    public Token replaceToken(Token[] tokens,
                              int replacingFromTokenIndex,
                              int replaceToTokenIndex) throws ParseException {
        Equ equ = Equ.builder(Object.class).equation(data).build();
        String resultAsATokenValue = null;
        try {
            Object equResult;
            equResult = equ.evaluate();
            if (equResult instanceof ZonedDateTime) {
                ZonedDateTime zonedDateTime = (ZonedDateTime) equResult;
                if (zonedDateTime.toLocalDate().equals(LocalDate.MIN)) {
                    resultAsATokenValue = TemporalHelper.getOutputTF().format(zonedDateTime);
                } else if (zonedDateTime.toLocalTime().equals(LocalTime.MIN)) {
                    resultAsATokenValue = TemporalHelper.getOutputDF().format(zonedDateTime);
                } else {
                    resultAsATokenValue = TemporalHelper.getOutputDTF().format(zonedDateTime);
                }
            } else {
                resultAsATokenValue = equResult.toString();
            }

        } catch (Exception e) {
            logger.error("{}", e.getMessage(), e);
            throw new ParseException(e.getMessage(), 0);
        }

        return new Token(tokens[replacingFromTokenIndex].charCommand(),
                         resultAsATokenValue,
                         tokens[replacingFromTokenIndex].getInputStartX(),
                         tokens[replaceToTokenIndex].getInputEndX(),
                         true);
    }
}
