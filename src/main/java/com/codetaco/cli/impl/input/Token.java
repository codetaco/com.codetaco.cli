package com.codetaco.cli.impl.input;

import com.codetaco.cli.impl.CmdLineImpl;
import com.codetaco.cli.impl.type.ICmdLineArg;
import org.apache.commons.codec.language.Metaphone;

public class Token {
    private final char commandPrefix;
    private final int inputStartX;
    private final int inputEndX;
    private String value;
    private boolean literal;
    private boolean used;
    private boolean charCommand;
    private boolean wordCommand;
    private String cachedWordCommand;

    public Token(final char _commandPrefix, final String _value) {
        this(_commandPrefix, _value, 0, _value.length() - 1, false);
    }

    public Token(
      final char _commandPrefix,
      final String _value,
      final int startIndex,
      final int endIndex,
      final boolean forceLiteral) {
        super();
        commandPrefix = _commandPrefix;
        value = _value;
        used = false;
        inputStartX = startIndex;
        inputEndX = endIndex - 1;

        if (forceLiteral) {
            literal = true;
        } else if (!isParserDirective()) {
            switch (dashes()) {
                case 1:
                    charCommand = true;
                    break;
                case 2:
                    wordCommand = true;
                    break;
                default:
                    literal = true;
            }
        }
    }

    public char charCommand() {
        if (!isCharCommand() || isUsed()) {
            return 0;
        }
        return getValue().charAt(1);
    }

    private int dashes() {
        if (getValue() == null || isCommand() || getValue().length() == 0) {
            return 0;
        }
        final int tokenSize = getValue().length();
        if (tokenSize > 1 && getValue().charAt(0) == commandPrefix) {
            if (tokenSize > 2 && getValue().charAt(1) == commandPrefix) {
                return 2;
            }
            return 1;
        }
        return 0;
    }

    public int getInputEndX() {
        return inputEndX;
    }

    public int getInputStartX() {
        return inputStartX;
    }

    public String getValue() {
        return value;
    }

    public String getWordCommand() {
        if (cachedWordCommand != null) {
            return cachedWordCommand;
        }

        if (!isWordCommand() || getValue().length() < 3) {
            return null;
        }
        cachedWordCommand = getValue().substring(2);
        return cachedWordCommand;
    }

    private boolean isCharCommand() {
        return charCommand;
    }

    public boolean isCharCommand(final ICmdLineArg<?> argDef) {
        return argDef.getKeychar() != null && isCharCommand() && (charCommand() == argDef.getKeychar());
    }

    public boolean isCommand() {
        return isCharCommand() | isWordCommand();
    }

    public boolean isGroupEnd() {
        if (getValue() == null || isCommand() || getValue().length() != 1) {
            return false;
        }
        return getValue().charAt(0) == ')' || getValue().charAt(0) == ']';
    }

    public boolean isGroupStart() {
        if (getValue() == null || isCommand() || getValue().length() != 1) {
            return false;
        }
        return getValue().charAt(0) == '(' || getValue().charAt(0) == '[';
    }

    public boolean isIncludeFile() {
        return value != null
                 && value.length() > 0
                 && CmdLineImpl.INCLUDE_FILE_PREFIX.charAt(0) == value.charAt(0);
    }

    public boolean isLiteral() {
        return literal;
    }

    public boolean isParserDirective() {
        if (getValue() == null || isCommand() || getValue().length() == 0) {
            return false;
        }
        if (getValue().charAt(0) == '_') {
            return true;
        }
        return getValue().length() == 1 && getValue().charAt(0) == '=';
    }

    public boolean isUsed() {
        return used;
    }

    public boolean isWordCommand() {
        return wordCommand;
    }

    public boolean isWordCommand(final ICmdLineArg<?> argDef) {
        if (argDef.getKeyword() == null
              || isUsed()
              || !isWordCommand()
              || getValue().length() < 3) {
            return false;
        }
        return exactWordMatch(argDef)
                 || camelCapsMatch(argDef)
                 || metaphoneMatch(argDef);
    }

    private boolean exactWordMatch(ICmdLineArg<?> argDef) {
        return getWordCommand().length() == argDef.getKeyword().length()
                 && getWordCommand().equals(argDef.getKeyword().toLowerCase());
    }

    private boolean metaphoneMatch(ICmdLineArg<?> argDef) {
        return argDef.isMetaphoneAllowed()
                 && argDef.getMetaphone() != null
                 && new Metaphone().metaphone(getWordCommand()).equals(argDef.getMetaphone());
    }

    private boolean camelCapsMatch(ICmdLineArg<?> argDef) {
        return argDef.getCamelCaps() != null
                 && argDef.getCamelCaps().matcher(getWordCommand()).matches();
    }

    public String remainderValue() {
        if (isCharCommand()) {
            if (getValue().length() == 1) {
                return null;
            }
            return getValue().substring(1);
        }
        if (isWordCommand()) {
            return null;
        }
        return getValue();
    }

    public void removeCharCommand() {
        if (!isCharCommand()) {
            return;
        }
        if (getValue() == null) {
            return;
        }
        if (getValue().length() < 2) {
            return;
        }
        if (getValue().length() == 2) {
            setValue(null);
            setUsed(true);
            return;
        }
        setValue("-" + getValue().substring(2));
    }

    public void setUsed(final boolean _used) {
        used = _used;
    }

    protected void setValue(final String _value) {
        value = _value;
    }

    @Override
    public String toString() {
        if (isUsed()) {
            return getValue() + "[" + inputStartX + "," + inputEndX + "] used";
        }
        return getValue() + "[" + inputStartX + "," + inputEndX + "]";
    }
}
