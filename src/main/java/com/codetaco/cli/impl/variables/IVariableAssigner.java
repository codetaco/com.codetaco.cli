package com.codetaco.cli.impl.variables;

import com.codetaco.cli.impl.type.CmdLineCLA;
import com.codetaco.cli.impl.type.ICmdLineArg;

import java.text.ParseException;

public interface IVariableAssigner {
    void assign(ICmdLineArg<?> arg, Object target) throws ParseException;

    Object newGroupVariable(CmdLineCLA group, Object target, ICmdLineArg<?> factoryValueArg)
      throws ParseException;
}
