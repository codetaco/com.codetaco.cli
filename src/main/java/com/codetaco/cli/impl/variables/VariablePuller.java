package com.codetaco.cli.impl.variables;

import com.codetaco.cli.impl.ICmdLine;
import com.codetaco.cli.impl.type.CmdLineCLA;
import com.codetaco.cli.impl.type.ICmdLineArg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.List;

public class VariablePuller {
    private final static Logger logger = LoggerFactory.getLogger(VariablePuller.class.getName());

    static private VariablePuller instance;

    public static VariablePuller getInstance() {
        if (instance == null) {
            instance = new VariablePuller();
        }
        return instance;
    }

    public void pull(ICmdLineArg<?> arg, Object variableSource)
      throws ParseException, IllegalArgumentException, IllegalAccessException {
        arg.reset();
        if (arg.isSystemGenerated()) {
            return;
        }

        String errMsg = "pulling "
                                + arg.getVariable()
                                + " from "
                                + variableSource.getClass().getName();

        Field field = VariableAssigner.findFieldInAnyParentOrMyself(arg, variableSource.getClass(), errMsg);
        if (field == null) {
            return;
        }

        boolean wasAccessible = field.isAccessible();

        try {
            field.setAccessible(true);

            if (field.getType() == List.class) {
                List<Object> values = (List<Object>) field.get(variableSource);
                if (values != null) {
                    for (Object value : values) {
                        if (arg instanceof CmdLineCLA) {
                            ICmdLine cmdline = (((CmdLineCLA) arg).templateCmdLine).clone();
                            arg.setObject(cmdline);
                            for (ICmdLineArg<?> innerArg : cmdline.allArgs()) {
                                pull(innerArg, value);
                            }
                        } else {
                            arg.setObject(value);
                        }
                    }
                }

            } else if (field.getType().isArray()) {
                Object[] values = (Object[]) field.get(variableSource);
                if (values != null) {
                    for (Object value : values) {
                        if (arg instanceof CmdLineCLA) {
                            ICmdLine cmdline = (((CmdLineCLA) arg).templateCmdLine).clone();
                            arg.setObject(cmdline);
                            for (ICmdLineArg<?> innerArg : cmdline.allArgs()) {
                                pull(innerArg, value);
                            }
                        } else {
                            arg.setObject(value);
                        }
                    }
                }
            } else {
                Object value = field.get(variableSource);
                if (value != null) {
                    if (arg instanceof CmdLineCLA) {
                        ICmdLine cmdline = (((CmdLineCLA) arg).templateCmdLine).clone();
                        arg.setObject(cmdline);
                        for (ICmdLineArg<?> innerArg : cmdline.allArgs()) {
                            pull(innerArg, value);
                        }
                    } else
                        /*
                         * Simple objects are handled here
                         */ {
                        arg.setObject(value);
                    }
                }
            }

        } catch (CloneNotSupportedException e) {
            logger.error(errMsg, e);
        } finally {
            field.setAccessible(wasAccessible);
        }
    }
}
