package com.codetaco.cli.impl.parser;

import com.codetaco.cli.impl.input.Token;

public interface IParserInput {

    Token[] parseTokens();

    String substring(int inclusiveStart, int exclusiveEnd);
}
