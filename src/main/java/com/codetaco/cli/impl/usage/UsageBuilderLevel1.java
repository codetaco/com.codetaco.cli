package com.codetaco.cli.impl.usage;

import com.codetaco.cli.impl.CmdLineImpl;
import com.codetaco.cli.impl.ICmdLine;
import com.codetaco.cli.impl.type.CmdLineCLA;
import com.codetaco.cli.impl.type.ICmdLineArg;

import java.util.Iterator;

public class UsageBuilderLevel1 extends UsageBuilder {

    UsageBuilderLevel1() {
        super();
    }

    void nameIt(char commandPrefix,
                ICmdLineArg<?> arg) {
        if (!arg.isRequired()) {
            append("[");
        }
        if (!arg.isPositional()) {
            if (arg.getKeyword() != null) {
                if (arg.getKeychar() != null && arg.getKeychar() != ' ') {
                    append(""
                             + commandPrefix
                             + arg.getKeychar()
                             + " "
                             + commandPrefix
                             + commandPrefix
                             + arg.getKeyword());
                } else {
                    append("" + commandPrefix + commandPrefix + arg.getKeyword());
                }
            } else if (arg.getKeychar() != null && arg.getKeychar() != ' ') {
                append("" + commandPrefix + arg.getKeychar());
            } else {
                append("unnamed");
            }
        }

        String n = arg.getClass().getSimpleName();
        // ClassnameCLA is how the arguments must be named
        String name = n.substring(0, n.length() - 3);
        if (!("Boolean".equalsIgnoreCase(name))) {
            if (!arg.isPositional()) {
                append(" ");
            }

            if ("CmdLine".equals(name)) {
                append("()");
            } else {
                append("<");
                append(name.toLowerCase());
                append(">");
            }
        }
        if (arg.isMultiple()) {
            append("...");
        }
        if (!arg.isRequired()) {
            append("]");
        }
    }

    @Override
    void prettyPrint(ICmdLine icmdLine) {
        CmdLineImpl cmdLine = (CmdLineImpl) icmdLine;
        usageHeader((cmdLine).getCommandPrefix(),
                    cmdLine,
                    0);
        showEachOwnedArg((cmdLine).allArgs().iterator(),
                         cmdLine.getCommandPrefix(),
                         cmdLine,
                         0);
    }

    private void showEachOwnedArg(Iterator<ICmdLineArg<?>> aIter,
                                  char commandPrefix,
                                  ICmdLineArg<?> cmdLine,
                                  int indentLevel) {
        int cnt = 0;
        while (aIter.hasNext()) {
            ICmdLineArg<?> arg = aIter.next();

            if (arg.isSystemGenerated()) {
                continue;
            }

            if (cnt++ > 0) {
                newLine(indentLevel);
            }

            if (arg instanceof CmdLineCLA) {
                usageDetail(commandPrefix, (CmdLineCLA) arg, indentLevel + 1);
            } else {
                usageDetail(commandPrefix, arg, indentLevel + 1);
            }
        }
    }

    private void usageDetail(char commandPrefix,
                             CmdLineCLA cmdLine,
                             int indentLevel) {
        usageDetail(commandPrefix,
                    ((ICmdLineArg<?>) cmdLine),
                    indentLevel);
        newLine(indentLevel);
        showEachOwnedArg((cmdLine.templateCmdLine).allArgs().iterator(),
                         commandPrefix,
                         cmdLine,
                         indentLevel);
    }

    public void usageDetail(char commandPrefix,
                            ICmdLineArg<?> arg,
                            int _indentLevel) {
        nameIt(commandPrefix, arg);
        String help = arg.getHelp();
        if (help != null) {
            if (help.length() > 40) {
                help = help.substring(0, 40);
            }
            allign(29);
            append(help);
            unallign();
        }
    }

    void usageHeader(char commandPrefix,
                     ICmdLine icmdLine,
                     int indentLevel) {
    }
}
