package com.codetaco.cli.impl.usage;

import com.codetaco.cli.impl.CmdLineImpl;
import com.codetaco.cli.impl.ICmdLine;
import com.codetaco.cli.impl.type.ICmdLineArg;

/**
 * <p>
 * UsageBuilderLevel2 class.
 * </p>
 *
 * @author Chris DeGreef fedupforone@gmail.com
 * @since 4.3.4
 */
public class UsageBuilderLevel2 extends UsageBuilderLevel1 {
    /**
     * <p>
     * Constructor for UsageBuilderLevel2.
     * </p>
     */
    public UsageBuilderLevel2() {
        super();
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * usageDetail.
     * </p>
     */
    @Override
    public void usageDetail(
      char commandPrefix,
      ICmdLineArg<?> arg,
      int _indentLevel) {
        nameIt(commandPrefix, arg);
        String help = arg.getHelp();
        if (help != null && help.trim().length() > 0) {
            allign(29);
            append(help);
            unallign();
            newLine();
        }
    }

    /**
     * @param commandPrefix
     * @param icmdLine
     * @param indentLevel
     */
    @Override
    void usageHeader(
      char commandPrefix,
      ICmdLine icmdLine,
      int indentLevel) {
        CmdLineImpl commandLineParser = (CmdLineImpl) icmdLine;

        if (commandLineParser.getName() != null) {
            append(commandLineParser.getName());
            newLine(indentLevel);
            newLine();
        }
    }
}
