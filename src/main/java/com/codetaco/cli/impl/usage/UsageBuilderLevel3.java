package com.codetaco.cli.impl.usage;

import com.codetaco.cli.impl.CmdLineImpl;
import com.codetaco.cli.impl.ICmdLine;
import com.codetaco.cli.impl.type.AbstractCLA;
import com.codetaco.cli.impl.type.EnumCLA;
import com.codetaco.cli.impl.type.ICmdLineArg;
import com.codetaco.cli.misc.ByteUtil;
import com.codetaco.date.impl.TemporalHelper;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

/**
 * <p>
 * UsageBuilderLevel2 class.
 * </p>
 *
 * @author Chris DeGreef fedupforone@gmail.com
 * @since 4.3.4
 */
public class UsageBuilderLevel3 extends UsageBuilderLevel2 {
    /**
     * <p>
     * Constructor for UsageBuilderLevel2.
     * </p>
     */
    public UsageBuilderLevel3() {
        super();
    }

    private void showEnumCriteria(String instanceClass) {
        if (instanceClass != null) {
            try {
                append("Allowable values: ");
                Object[] constants = CmdLineImpl.ClassLoader.loadClass(instanceClass).getEnumConstants();
                for (int o = 0; o < constants.length; o++) {
                    if (o > 0) {
                        append(", ");
                    }
                    append(constants[o].toString());
                }
            } catch (ClassNotFoundException e) {
                append("ENUM CLASS NOT FOUND: " + instanceClass);
            }
        }
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * usageDetail.
     * </p>
     */
    @Override
    public void usageDetail(
      char commandPrefix,
      ICmdLineArg<?> arg,
      int _indentLevel) {
        nameIt(commandPrefix, arg);

        allign(29);
        String help = arg.getHelp();
        if (help != null && help.trim().length() > 0) {
            append(help);
        }

        append(" ");

        usageDetailModifiers(commandPrefix, arg, _indentLevel + 1);

        if (arg.getFormat() != null) {
            append("The value must adhere to \"").append(arg.getFormat() + "\". ");
        }

        unallign();
        newLine();
    }

    /**
     * @param commandPrefix
     * @param arg
     * @param indentLevel
     */
    void usageDetailModifiers(
      char commandPrefix,
      ICmdLineArg<?> arg,
      int indentLevel) {

        String n = arg.getClass().getSimpleName();
        // ClassnameCLA is how the arguments must be named
        String name = n.substring(0, n.length() - 3);
        if ("Boolean".equals(name)) {
            return;
        }

        if (arg.isMultiple()) {
            if (arg.getMultipleMax() == Integer.MAX_VALUE) {
                if (arg.isRequired()) {
                    append("Although multiple values are allowed, you must specify at least "
                             + arg.getMultipleMin()
                             + ". ");
                } else {
                    append("Although no value is required, if you specify one you must specify at least "
                             + arg.getMultipleMin()
                             + ". ");
                }
            } else if (arg.isRequired()) {
                append("It is required that you specify at least "
                         + arg.getMultipleMin()
                         + " and at most "
                         + arg.getMultipleMax()
                         + " values. ");
            } else {
                append("Although no value is required, if you specify one you must specify at least "
                         + arg.getMultipleMin()
                         + " and at most "
                         + arg.getMultipleMax()
                         + " values. ");
            }
        }

        if (arg.isCaseSensitive()) {
            append("Upper vs lower case matters. ");
        }

        if (arg.getCriteria() != null) {
            arg.getCriteria().usage(this, indentLevel);
            append(". ");
        }

        if (arg instanceof EnumCLA) {
            showEnumCriteria(arg.getInstanceClass());
            append(". ");
        }

        if (arg instanceof AbstractCLA) {
            AbstractCLA<?> acla = (AbstractCLA<?>) arg;
            if (acla.getDefaultValues() != null && !acla.getDefaultValues().isEmpty()) {
                append("If unspecified, the default will be");
                for (Object dv : acla.getDefaultValues()) {
                    append(" ");
                    if (dv instanceof Byte) {
                        append(ByteUtil.asLiteral(((Byte) dv).byteValue()));
                    } else if (dv instanceof Calendar) {
                        append(TemporalHelper.getOutputSDF().format(((Calendar) dv).getTime()));
                    } else if (dv instanceof Date) {
                        append(TemporalHelper.getOutputSDF().format((Date) dv));
                    } else {
                        append(dv.toString());
                    }
                }
                append(". ");
            }
        }
    }

    /**
     * @param commandPrefix
     * @param icmdLine
     * @param indentLevel
     */
    @Override
    void usageHeader(
      char commandPrefix,
      ICmdLine icmdLine,
      int indentLevel) {
        CmdLineImpl commandLineParser = (CmdLineImpl) icmdLine;

        if (commandLineParser.getName() != null) {
            append(commandLineParser.getName());
            append(". ");
        }

        if (indentLevel == 0 && commandLineParser.getHelp() != null) {
            append(commandLineParser.getHelp());
            newLine(indentLevel);
        }
        newLine(indentLevel);
        if (indentLevel == 0 && !commandLineParser.getDefaultIncludeDirectories().isEmpty()) {
            newLine(indentLevel);
            append("Import path =");
            for (File dir : commandLineParser.getDefaultIncludeDirectories()) {
                append(" ");
                append(dir.getAbsolutePath());
                append(";");
            }
            newLine(indentLevel);
        }
    }
}
