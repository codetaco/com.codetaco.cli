package com.codetaco.cli.impl.type;

import com.codetaco.cli.CliException;
import com.codetaco.date.CalendarFactory;
import com.codetaco.date.impl.TemporalHelper;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeCLA extends AbstractCLA<LocalDateTime> {
    DateTimeFormatter dtf;

    @Override
    public LocalDateTime convert(final String valueStr,
                                 final boolean _caseSensitive,
                                 final Object target) {
        try {
            if (dtf == null) {
                if (getFormat() != null) {
                    try {
                        dtf = DateTimeFormatter.ofPattern(getFormat());
                    } catch (final Exception e) {
                        throw new ParseException("date format: " + e.getMessage(), 0);
                    }
                }
            }

            try {
                if (dtf == null) {
                    return CalendarFactory.asLocalDateTime(valueStr);
                }
                return LocalDateTime.parse(valueStr, dtf);
            } catch (final Exception e) {
                throw new ParseException(toString() + " " + getFormat() + ": " + e.getMessage(), 0);
            }
        } catch (Exception e) {
            throw CliException.builder().cause(e).build();
        }
    }

    @Override
    public String defaultInstanceClass() {
        return "java.time.LocalDateTime";
    }

    @Override
    protected void exportCommandLineData(final StringBuilder out,
                                         final int occ) {
        out.append('"');
        out.append(TemporalHelper.getOutputDTF().format(getValue(occ)));
        out.append('"');
    }

    @Override
    protected void exportNamespaceData(final String prefix,
                                       final StringBuilder out,
                                       final int occ) {
        out.append(prefix);
        out.append("=");
        out.append(TemporalHelper.getOutputDTF().format(getValue(occ)));
        out.append("\n");
    }

    @Override
    protected void exportXmlData(final StringBuilder out,
                                 final int occ) {
        out.append(TemporalHelper.getOutputDTF().format(getValue(occ)));
    }

    @Override
    public String genericClassName() {
        return "java.time.LocalDateTime";
    }

    @Override
    public LocalDateTime[] getValueAsLocalDateTimeArray() {
        final LocalDateTime[] result = new LocalDateTime[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r);
        }

        return result;
    }

    @Override
    public boolean supportsCaseSensitive() {
        return true;
    }

    @Override
    public boolean supportsFormat() {
        return true;
    }

    @Override
    public boolean supportsInList() {
        return false;
    }
}
