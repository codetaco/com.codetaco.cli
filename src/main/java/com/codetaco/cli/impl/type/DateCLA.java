package com.codetaco.cli.impl.type;

import com.codetaco.cli.CliException;
import com.codetaco.date.CalendarFactory;
import com.codetaco.date.impl.TemporalHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateCLA extends AbstractCLA<Date> {
    SimpleDateFormat sdf;

    @Override
    public Date convert(final String valueStr,
                        final boolean _caseSensitive,
                        final Object target) {
        try {
            if (sdf == null) {
                if (getFormat() != null) {
                    try {
                        sdf = new SimpleDateFormat(getFormat());
                    } catch (final Exception e) {
                        throw new ParseException("date format: " + e.getMessage(), 0);
                    }
                }
            }

            try {
                if (sdf == null) {
                    return CalendarFactory.asDate(valueStr);
                }
                return sdf.parse(valueStr);
            } catch (final Exception e) {
                throw new ParseException(toString() + " " + getFormat() + ": " + e.getMessage(), 0);
            }
        } catch (Exception e) {
            throw CliException.builder().cause(e).build();
        }
    }

    @Override
    public String defaultInstanceClass() {
        return "java.util.Date";
    }

    @Override
    protected void exportCommandLineData(final StringBuilder out,
                                         final int occ) {
        synchronized (TemporalHelper.getOutputSDF()) {
            out.append('"');
            out.append(TemporalHelper.getOutputSDF().format(getValue(occ)));
            out.append('"');
        }
    }

    @Override
    protected void exportNamespaceData(final String prefix,
                                       final StringBuilder out,
                                       final int occ) {
        synchronized (TemporalHelper.getOutputSDF()) {
            out.append(prefix);
            out.append("=");
            out.append(TemporalHelper.getOutputSDF().format(getValue(occ)));
            out.append("\n");
        }
    }

    @Override
    protected void exportXmlData(final StringBuilder out,
                                 final int occ) {
        synchronized (TemporalHelper.getOutputSDF()) {
            out.append(TemporalHelper.getOutputSDF().format(getValue(occ)));
        }
    }

    @Override
    public String genericClassName() {
        return "java.util.Date";
    }

    @Override
    public Date[] getValueAsDateArray() {
        final Date[] result = new Date[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r);
        }

        return result;
    }

    @Override
    public boolean supportsCaseSensitive() {
        return true;
    }

    @Override
    public boolean supportsFormat() {
        return true;
    }

    @Override
    public boolean supportsInList() {
        return false;
    }
}
