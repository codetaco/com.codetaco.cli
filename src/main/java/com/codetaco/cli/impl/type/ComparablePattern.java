package com.codetaco.cli.impl.type;

import java.util.regex.Pattern;

public class ComparablePattern implements Comparable<Pattern> {
    public static ComparablePattern compile(String valueStr) {
        ComparablePattern cp = new ComparablePattern();
        cp.delegate = Pattern.compile(valueStr);
        return cp;
    }

    public static ComparablePattern compile(String valueStr, int caseInsensitive) {
        ComparablePattern cp = new ComparablePattern();
        cp.delegate = Pattern.compile(valueStr, caseInsensitive);
        return cp;
    }

    Pattern delegate;

    @Override
    public int compareTo(Pattern o) {
        return delegate.pattern().compareTo(o.pattern());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ComparablePattern other = (ComparablePattern) obj;
        if (delegate.pattern() == null) {
            return other.delegate.pattern() == null;
        } else {
            return delegate.pattern().equals(other.delegate.pattern());
        }
    }

    @Override
    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = prime * result + ((delegate.pattern() == null)
                                     ? 0
                                     : delegate.pattern().hashCode());
        return result;
    }

    public String pattern() {
        return delegate.pattern();
    }

    @Override
    public String toString() {
        return delegate.pattern();
    }
}
