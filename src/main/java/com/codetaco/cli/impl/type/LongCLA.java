package com.codetaco.cli.impl.type;

import com.codetaco.cli.CliException;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class LongCLA extends AbstractCLA<Long> {
    protected NumberFormat FMTin = NumberFormat.getNumberInstance();
    protected NumberFormat FMTout = new DecimalFormat("0");

    @Override
    public Long convert(String valueStr,
                        boolean _caseSensitive,
                        Object target) {
        try {
            return FMTin.parse(valueStr).longValue();
        } catch (Exception e) {
            throw CliException.builder().cause(e).build();
        }
    }

    @Override
    public String defaultInstanceClass() {
        return "long";
    }

    @Override
    protected Long naturalDefault() {
        return (long) 0;
    }

    @Override
    protected void exportCommandLineData(StringBuilder out,
                                         int occ) {
        if (getValue(occ) < 0) {
            out.append("'");
        }
        out.append(FMTout.format(getValue(occ)));
        if (getValue(occ) < 0) {
            out.append("'");
        }
    }

    @Override
    protected void exportNamespaceData(String prefix,
                                       StringBuilder out,
                                       int occ) {
        out.append(prefix);
        out.append("=");
        out.append(FMTout.format(getValue(occ)));
        out.append("\n");
    }

    @Override
    protected void exportXmlData(StringBuilder out,
                                 int occ) {
        out.append(FMTout.format(getValue(occ)));
    }

    @Override
    public String genericClassName() {
        return "java.lang.Long";
    }

    @Override
    public long[] getValueAslongArray() {
        long[] result = new long[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r).longValue();
        }

        return result;
    }

    @Override
    public Long[] getValueAsLongArray() {
        Long[] result = new Long[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r);
        }

        return result;
    }
}
