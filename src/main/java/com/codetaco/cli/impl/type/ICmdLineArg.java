package com.codetaco.cli.impl.type;

import com.codetaco.cli.impl.criteria.ICmdLineArgCriteria;
import com.codetaco.cli.impl.input.Token;
import com.codetaco.math.Equ;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public interface ICmdLineArg<E> {
    void applyDefaults();

    void asDefinedType(StringBuilder sb);

    Object asEnum(String name, Object[] possibleConstants);

    Object[] asEnumArray(String name, Object[] possibleConstants);

    ICmdLineArg<E> clone() throws CloneNotSupportedException;

    E convert(String valueStr);

    E convert(String valueStr, boolean caseSensitive, Object target);

    String defaultInstanceClass();

    void exportCommandLine(StringBuilder str);

    void exportNamespace(String prefix, StringBuilder str);

    void exportXml(String prefix, StringBuilder str);

    String genericClassName();

    Pattern getCamelCaps();

    ICmdLineArgCriteria<?> getCriteria();

    List<E> getDefaultValues();

    Object getDelegateOrValue();

    Object getDelegateOrValue(int occurrence);

    String getEnumClassName();

    String getFactoryArgName();

    String getFactoryMethodName();

    String getFormat();

    String getHelp();

    String getInstanceClass();

    Character getKeychar();

    String getKeyword();

    Object getMetaphone();

    int getMultipleMax();

    int getMultipleMin();

    int getUniqueId();

    E getValue();

    List<E> getValues();

    E getValue(int index);

    byte[] getValueAsbyteArray();

    Byte[] getValueAsByteArray();

    Calendar[] getValueAsCalendarArray();

    Character[] getValueAsCharacterArray();

    char[] getValueAscharArray();

    Date[] getValueAsDateArray();

    DateTimeFormatter getValueAsDateTimeFormatter();

    DateTimeFormatter[] getValueAsDateTimeFormatterArray();

    double[] getValueAsdoubleArray();

    Double[] getValueAsDoubleArray();

    Equ getValueAsEquation();

    Equ[] getValueAsEquationArray();

    File[] getValueAsFileArray();

    URL[] getValueAsURLArray();

    float[] getValueAsfloatArray();

    Float[] getValueAsFloatArray();

    int[] getValueAsintArray();

    Integer[] getValueAsIntegerArray();

    LocalDate[] getValueAsLocalDateArray();

    LocalDateTime[] getValueAsLocalDateTimeArray();

    ZonedDateTime[] getValueAsZonedDateTimeArray();

    LocalTime[] getValueAsLocalTimeArray();

    long[] getValueAslongArray();

    Long[] getValueAsLongArray();

    Pattern getValueAsPattern();

    Pattern[] getValueAsPatternArray();

    SimpleDateFormat getValueAsSimpleDateFormat();

    SimpleDateFormat[] getValueAsSimpleDateFormatArray();

    String[] getValueAsStringArray();

    String getVariable();

    boolean hasValue();

    boolean isCaseSensitive();

    boolean isMetaphoneAllowed();

    boolean isMultiple();

    boolean isParsed();

    boolean isPositional();

    boolean isRequired();

    boolean isRequiredValue();

    boolean isSystemGenerated();

    void reset();

    ICmdLineArg<E> resetCriteria();

    int salience(Token word);

    ICmdLineArg<E> setCaseSensitive(boolean bool);

    ICmdLineArg<E> setDefaultValue(String defaultValue);

    ICmdLineArg<E> setEnumCriteria(String enumClassName);

    ICmdLineArg<E> setEnumCriteriaAllowError(String enumClassName);

    ICmdLineArg<E> setFactoryArgName(String argName);

    ICmdLineArg<E> setFactoryMethodName(String methodName);

    ICmdLineArg<E> setFormat(String format);

    ICmdLineArg<E> setHelp(String p_helpString);

    ICmdLineArg<E> setInstanceClass(String p_instanceClassString);

    ICmdLineArg<E> setKeychar(final Character _keychar);

    ICmdLineArg<E> setKeyword(final String _keyword);

    ICmdLineArg<E> setListCriteria(String[] values);

    ICmdLineArg<E> setMetaphoneAllowed(boolean bool);

    ICmdLineArg<E> setMultiple(boolean bool);

    ICmdLineArg<E> setMultiple(int min);

    ICmdLineArg<E> setMultiple(int min, int max);

    void setObject(Object value);

    ICmdLineArg<E> setParsed(boolean bool);

    ICmdLineArg<E> setPositional(boolean bool);

    ICmdLineArg<E> setRangeCriteria(String min, String max);

    ICmdLineArg<E> setRegxCriteria(String pattern);

    ICmdLineArg<E> setRequired(boolean bool);

    ICmdLineArg<E> setRequiredValue(boolean bool);

    ICmdLineArg<E> setSystemGenerated(boolean bool);

    void setType(ClaType claType);

    void setUniqueId(int i);

    void setValue(E value);

    void setValue(int index, E value);

    ICmdLineArg<E> setVariable(String p_variableString);

    int size();

    boolean supportsCaseSensitive();

    boolean supportsDefaultValues();

    boolean supportsExcludeArgs();

    boolean supportsFactoryArgName();

    boolean supportsFactoryMethod();

    boolean supportsFormat();

    boolean supportsHelp();

    boolean supportsInList();

    boolean supportsInstanceClass();

    boolean supportsLongName();

    boolean supportsMatches();

    boolean supportsMetaphone();

    boolean supportsMultimax();

    boolean supportsMultimin();

    boolean supportsPositional();

    boolean supportsRange();

    boolean supportsRequired();

    boolean supportsShortName();

    void useDefaults();
}
