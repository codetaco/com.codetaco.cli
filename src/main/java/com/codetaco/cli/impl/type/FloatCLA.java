package com.codetaco.cli.impl.type;

import com.codetaco.cli.CliException;

import java.text.NumberFormat;

public class FloatCLA extends AbstractCLA<Float> {
    protected NumberFormat FMTin = NumberFormat.getNumberInstance();
    protected NumberFormat FMTout = NumberFormat.getNumberInstance();

    @Override
    public Float convert(String valueStr,
                         boolean _caseSensitive,
                         Object target) {
        try {
            return FMTin.parse(valueStr).floatValue();
        } catch (Exception e) {
            throw CliException.builder().cause(e).build();
        }
    }

    @Override
    public String defaultInstanceClass() {
        return "float";
    }

    @Override
    protected Float naturalDefault() {
        return (float) 0.0;
    }

    @Override
    protected void exportCommandLineData(StringBuilder out,
                                         int occ) {
        if (getValue(occ) < 0.0) {
            uncompileQuoter(out, FMTout.format(getValue(occ)).replaceAll(",", ""));
        } else {
            out.append(FMTout.format(getValue(occ)).replaceAll(",", ""));
        }
    }

    @Override
    protected void exportNamespaceData(String prefix,
                                       StringBuilder out,
                                       int occ) {
        out.append(prefix);
        out.append("=");
        out.append(FMTout.format(getValue(occ)).replaceAll(",", ""));
        out.append("\n");
    }

    @Override
    protected void exportXmlData(StringBuilder out,
                                 int occ) {
        out.append(FMTout.format(getValue(occ)).replaceAll(",", ""));
    }

    @Override
    public String genericClassName() {
        return "java.lang.Float";
    }

    @Override
    public float[] getValueAsfloatArray() {
        float[] result = new float[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r).floatValue();
        }

        return result;
    }

    @Override
    public Float[] getValueAsFloatArray() {
        Float[] result = new Float[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r);
        }

        return result;
    }
}
