package com.codetaco.cli.impl.type;

import com.codetaco.cli.type.WildFiles;

public class WildFilesCLA extends AbstractCLA<WildFiles> {
    WildFiles wildFile = new WildFiles();

    @Override
    public WildFiles convert(final String valueStr,
                             final boolean _caseSensitive,
                             final Object target) {
        wildFile.addPattern(valueStr);
        return wildFile;
    }

    @Override
    public String defaultInstanceClass() {
        return "com.codetaco.cli.WildFiles";
    }

    @Override
    protected void exportCommandLineData(final StringBuilder out,
                                         final int occ) {
        uncompileQuoter(out, getValue(occ).getPattern(0));
    }

    @Override
    protected void exportNamespaceData(final String prefix,
                                       final StringBuilder out,
                                       final int occ) {
        out.append(prefix);
        out.append("=");
        out.append(getValue(occ));
        out.append("\n");
    }

    @Override
    protected void exportXmlData(final StringBuilder out,
                                 final int occ) {
        xmlEncode(getValue(occ).getPattern(0), out);
    }

    @Override
    public String genericClassName() {
        return "com.codetaco.cli.WildFiles";
    }

    @Override
    public void reset() {
        super.reset();
        wildFile = new WildFiles();
    }

    @Override
    public boolean supportsCaseSensitive() {
        return true;
    }
}
