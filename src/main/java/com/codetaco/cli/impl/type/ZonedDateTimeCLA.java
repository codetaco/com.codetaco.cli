package com.codetaco.cli.impl.type;

import com.codetaco.cli.CliException;
import com.codetaco.date.impl.TemporalHelper;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class ZonedDateTimeCLA extends AbstractCLA<ZonedDateTime> {
    DateTimeFormatter dtf;

    @Override
    public ZonedDateTime convert(final String valueStr,
                                 final boolean _caseSensitive,
                                 final Object target) {
        try {
            if (dtf == null) {
                if (getFormat() != null) {
                    try {
                        dtf = DateTimeFormatter.ofPattern(getFormat());
                    } catch (final Exception e) {
                        throw new ParseException("date format: " + e.getMessage(), 0);
                    }
                }
            }

            try {
                if (dtf == null) {
                    return TemporalHelper.parseWithPredefinedParsers(valueStr);
                }
                return ZonedDateTime.parse(valueStr, dtf);
            } catch (final Exception e) {
                throw new ParseException(toString() + " " + getFormat() + ": " + e.getMessage(), 0);
            }
        } catch (Exception e) {
            throw CliException.builder().cause(e).build();
        }
    }

    @Override
    public String defaultInstanceClass() {
        return "java.time.ZonedDateTime";
    }

    @Override
    protected void exportCommandLineData(final StringBuilder out,
                                         final int occ) {
        out.append('"');
        out.append(TemporalHelper.getOutputDTF().format(getValue(occ)));
        out.append('"');
    }

    @Override
    protected void exportNamespaceData(final String prefix,
                                       final StringBuilder out,
                                       final int occ) {
        out.append(prefix);
        out.append("=");
        out.append(TemporalHelper.getOutputDTF().format(getValue(occ)));
        out.append("\n");
    }

    @Override
    protected void exportXmlData(final StringBuilder out,
                                 final int occ) {
        out.append(TemporalHelper.getOutputDTF().format(getValue(occ)));
    }

    @Override
    public String genericClassName() {
        return "java.time.ZonedDateTime";
    }

    @Override
    public ZonedDateTime[] getValueAsZonedDateTimeArray() {
        final ZonedDateTime[] result = new ZonedDateTime[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r);
        }

        return result;
    }

    @Override
    public boolean supportsCaseSensitive() {
        return true;
    }

    @Override
    public boolean supportsFormat() {
        return true;
    }

    @Override
    public boolean supportsInList() {
        return false;
    }
}
