package com.codetaco.cli.impl.type;

import com.codetaco.cli.CliException;

import java.text.NumberFormat;

public class DoubleCLA extends AbstractCLA<Double> {
    protected NumberFormat FMT = NumberFormat.getNumberInstance();

    @Override
    public Double convert(final String valueStr,
                          final boolean _caseSensitive,
                          final Object target) {
        try {
            return FMT.parse(valueStr).doubleValue();
        } catch (Exception e) {
            throw CliException.builder().cause(e).build();
        }
    }

    @Override
    public String defaultInstanceClass() {
        return "double";
    }

    @Override
    protected Double naturalDefault() {
        return 0.0;
    }

    @Override
    protected void exportCommandLineData(final StringBuilder out,
                                         final int occ) {
        if (getValue(occ) < 0.0) {
            uncompileQuoter(out, FMT.format(getValue(occ)).replaceAll(",", ""));
        } else {
            out.append(FMT.format(getValue(occ)).replaceAll(",", ""));
        }
    }

    @Override
    protected void exportNamespaceData(final String prefix,
                                       final StringBuilder out,
                                       final int occ) {
        out.append(prefix);
        out.append("=");
        out.append(FMT.format(getValue(occ)).replaceAll(",", ""));
        out.append("\n");
    }

    @Override
    protected void exportXmlData(final StringBuilder out,
                                 final int occ) {
        out.append(FMT.format(getValue(occ)).replaceAll(",", ""));
    }

    @Override
    public String genericClassName() {
        return "java.lang.Double";
    }

    @Override
    public double[] getValueAsdoubleArray() {
        final double[] result = new double[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r).doubleValue();
        }

        return result;
    }

    @Override
    public Double[] getValueAsDoubleArray() {
        final Double[] result = new Double[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r);
        }

        return result;
    }

}
