package com.codetaco.cli.impl.type;

import com.codetaco.cli.CliException;

import java.text.ParseException;
import java.util.regex.Pattern;

public class EnumCLA extends StringCLA {

    @Override
    public Object asEnum(final String enumClassFieldName,
                         final Object[] possibleConstants) {
        try {
            return stringToEnumConstant(enumClassFieldName, possibleConstants, getValue());
        } catch (Exception e) {
            throw CliException.builder().cause(e).build();
        }
    }

    @Override
    public Enum<?>[] asEnumArray(final String enumClassFieldName,
                                 final Object[] possibleConstants) {
        try {
            final Enum<?>[] enumArray = new Enum<?>[size()];
            for (int v = 0; v < size(); v++) {
                enumArray[v] = (Enum<?>) stringToEnumConstant(enumClassFieldName, possibleConstants, getValue(v));
            }
            return enumArray;
        } catch (Exception e) {
            throw CliException.builder().cause(e).build();
        }
    }

    @Override
    public String convert(final String valueStr,
                          final boolean _caseSensitive,
                          final Object target) {
        if (_caseSensitive) {
            return valueStr;
        }
        return valueStr.toLowerCase();
    }

    @Override
    public String defaultInstanceClass() {
        return enumClassName;
    }

    @Override
    protected void exportCommandLineData(final StringBuilder out,
                                         final int occ) {
        uncompileQuoter(out, getValue(occ));
    }

    @Override
    protected void exportNamespaceData(final String prefix,
                                       final StringBuilder out,
                                       final int occ) {
        out.append(prefix);
        out.append("=");
        out.append(getValue(occ));
        out.append("\n");
    }

    @Override
    protected void exportXmlData(final StringBuilder out,
                                 final int occ) {
        out.append(getValue(occ));
    }

    @Override
    public String genericClassName() {
        return "java.lang.String";
    }

    private Object stringToEnumConstant(final String enumClassFieldName,
                                        final Object[] possibleConstants,
                                        final String enumConstantName) throws ParseException {
        Object selectedConstant = null;

        final StringBuilder possibleValues = new StringBuilder();
        for (int c = 0; c < possibleConstants.length; c++) {
            final String econst = possibleConstants[c].toString();
            if (c > 0) {
                possibleValues.append(", ");
            }
            possibleValues.append(econst);
        }

        /*
         * Try an exact match
         */
        for (int c = 0; c < possibleConstants.length; c++) {
            String econst = possibleConstants[c].toString().toLowerCase();
            if (econst.equalsIgnoreCase(enumConstantName)) {
                selectedConstant = possibleConstants[c];
            }
        }
        if (selectedConstant != null) {
            return selectedConstant;
        }

        /*
         * Try camel caps
         */
        for (int c = 0; c < possibleConstants.length; c++) {
            Pattern econst = AbstractCLA.createCamelCapVersionOfKeyword(possibleConstants[c].toString());
            if (econst.matcher(enumConstantName).matches()) {
                if (selectedConstant != null) {
                    throw new ParseException("\""
                                               + enumConstantName
                                               + "\" is not a unique enum constant for variable \""
                                               + enumClassFieldName
                                               + "\" ("
                                               + possibleValues.toString()
                                               + ")",
                                             0);
                }
                selectedConstant = possibleConstants[c];
            }
        }
        if (selectedConstant != null) {
            return selectedConstant;
        }

        throw new ParseException("\""
                                   + enumConstantName
                                   + "\" is not a valid enum constant for variable \""
                                   + enumClassFieldName
                                   + "\" ("
                                   + possibleValues.toString()
                                   + ")",
                                 0);
    }
}
