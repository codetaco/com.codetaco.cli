package com.codetaco.cli.impl.type;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.cli.type.WildFiles;
import com.codetaco.math.Equ;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

public enum ClaType {
    DEFAULT("default", DefaultCLA.class),
    FILE("file", FileCLA.class),
    URL("url", URLCLA.class),
    WILDFILE("wildfile", WildFilesCLA.class),
    DOUBLE("double", DoubleCLA.class),
    FLOAT("float", FloatCLA.class),
    PATTERN("pattern", PatternCLA.class),
    DATETIMEFORMATTER("datetimeformatter", DateTimeFormatterCLA.class),
    SIMPLEDATEFORMAT("simpledateformat", SimpleDateFormatCLA.class),
    DATE("date", DateCLA.class),
    CALENDAR("calendar", CalendarCLA.class),
    LOCALDATETIME("localdatetime", LocalDateTimeCLA.class),
    ZONEDDATETIME("zoneddatetime", ZonedDateTimeCLA.class),
    LOCALDATE("localdate", LocalDateCLA.class),
    LOCALTIME("localtime", LocalTimeCLA.class),
    LONG("long", LongCLA.class),
    ENUM("enum", EnumCLA.class),
    INTEGER("integer", IntegerCLA.class),
    STRING("string", StringCLA.class),
    BYTE("byte", ByteCLA.class),
    CHAR("character", CharacterCLA.class),
    BOOLEAN("boolean", BooleanCLA.class),
    SUBPARSER("begin", CmdLineCLA.class),
    EQU("equation", EquCLA.class),
    POJO("pojo", PojoCLA.class);

    static public ClaType forField(Field field, Arg argAnnotation) {
        Class<?> fieldType = field.getType();

        if (fieldType == String.class
              || fieldType == String[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.lang.String>")) {
            return STRING;
        }
        if (fieldType == int.class
              || fieldType == Integer.class
              || fieldType == int[].class
              || fieldType == Integer[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.lang.Integer>")) {
            return INTEGER;
        }
        if (fieldType == long.class
              || fieldType == Long.class
              || fieldType == long[].class
              || fieldType == Long[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.lang.Long>")) {
            return LONG;
        }
        if (fieldType == File.class
              || fieldType == File[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.io.File>")) {
            return FILE;
        }
        if (fieldType == java.net.URL.class
              || fieldType == URL[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.net.URL>")) {
            return URL;
        }
        if (fieldType == WildFiles.class
              || fieldType == WildFiles[].class
              || field.getGenericType().getTypeName().equals("java.util.List<com.codetaco.cli.WildFiles>")) {
            return WILDFILE;
        }
        if (fieldType == double.class
              || fieldType == Double.class
              || fieldType == double[].class
              || fieldType == Double[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.lang.Double>")) {
            return DOUBLE;
        }
        if (fieldType == float.class
              || fieldType == Float.class
              || fieldType == float[].class
              || fieldType == Float[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.lang.Float>")) {
            return FLOAT;
        }
        if (fieldType == Pattern.class
              || fieldType == Pattern[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.util.regex.Pattern>")) {
            return PATTERN;
        }
        if (fieldType == DateTimeFormatter.class
              || fieldType == DateTimeFormatter[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.time.format.DateTimeFormatter>")) {
            return DATETIMEFORMATTER;
        }
        if (fieldType == SimpleDateFormat.class
              || fieldType == SimpleDateFormat[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.text.SimpleDateFormat>")) {
            return SIMPLEDATEFORMAT;
        }
        if (fieldType == Date.class
              || fieldType == Date[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.util.Date>")) {
            return DATE;
        }
        if (fieldType == Calendar.class
              || fieldType == Calendar[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.util.Calendar>")) {
            return CALENDAR;
        }
        if (fieldType == LocalDateTime.class
              || fieldType == LocalDateTime[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.time.LocalDateTime>")) {
            return LOCALDATETIME;
        }
        if (fieldType == ZonedDateTime.class
              || fieldType == ZonedDateTime[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.time.ZonedDateTime>")) {
            return ZONEDDATETIME;
        }
        if (fieldType == LocalDate.class
              || fieldType == LocalDate[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.time.LocalDate>")) {
            return LOCALDATE;
        }
        if (fieldType == LocalTime.class
              || fieldType == LocalTime[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.time.LocalTime>")) {
            return LOCALTIME;
        }
        if (fieldType.isEnum()
              || (fieldType.isArray() && fieldType.getComponentType().isEnum())
        ) {
            return ENUM;
        }
        if (fieldType == byte.class
              || fieldType == Byte.class
              || fieldType == byte[].class
              || fieldType == Byte[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.lang.Byte>")) {
            return BYTE;
        }
        if (fieldType == char.class
              || fieldType == Character.class
              || fieldType == char[].class
              || fieldType == Character[].class
              || field.getGenericType().getTypeName().equals("java.util.List<java.lang.Character>")) {
            return CHAR;
        }
        if (fieldType == boolean.class
              || fieldType == Boolean.class) {
            return BOOLEAN;
        }
        if (fieldType == CmdLineCLA.class
              || fieldType == CmdLineCLA[].class
              || field.getGenericType().getTypeName().equals("java.util.List<com.codetaco.cli.CmdLineCLA>")) {
            return SUBPARSER;
        }
        if (fieldType == Equ.class
              || fieldType == Equ[].class
              || field.getGenericType().getTypeName().equals("java.util.List<com.codetaco.math.Equ>")) {
            return EQU;
        }
        /*
         * This point is arrived at for two somewhat different reasons. The most
         * common is when a subparser is being defined. A lesser reason is when
         * a complex object is being produced that is not a subparser. This
         * would require a factoryMethod and a factoryArgName of
         * SELF_REFERENCING_ARGNAME.
         */
        if (!argAnnotation.factoryMethod().equals("")
              && argAnnotation.factoryArgName().equalsIgnoreCase(Arg.SELF_REFERENCING_ARGNAME)) {
            return POJO;
        }

        return SUBPARSER;
    }

    private String typeName;

    private Class<ICmdLineArg<?>> argumentClass;

    ClaType(String name, Class<?> argClass) {
        typeName = name;
        argumentClass = (Class<ICmdLineArg<?>>) argClass;
    }

    public ICmdLineArg<?> argumentInstance(char commandPrefix, char keychar, String keyword)
      throws ParseException {
        try {
            ICmdLineArg<?> arg = argumentClass.newInstance();

            if (keychar != commandPrefix) {
                arg.setKeychar(keychar);
            }
            if (keyword != null) {
                arg.setKeyword(keyword);
            }
            arg.setType(this);

            return arg;

        } catch (Exception e) {
            throw new ParseException(name() + ": " + e.getMessage(), -1);
        }
    }

    public Object getTypeName() {
        return typeName;
    }

}
