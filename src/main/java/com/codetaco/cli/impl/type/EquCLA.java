package com.codetaco.cli.impl.type;

import com.codetaco.cli.CliException;
import com.codetaco.math.Equ;

import java.text.ParseException;

public class EquCLA extends AbstractCLA<ComparableEqu> {
    @Override
    public ComparableEqu convert(String valueStr,
                                 boolean _caseSensitive,
                                 Object target) {
        try {
            return ComparableEqu.compile(valueStr);

        } catch (Exception pse) {
            throw CliException.builder().cause(new ParseException(pse.getMessage(), 0)).build();
        }
    }

    @Override
    public String defaultInstanceClass() {
        return "com.codetaco.math.Equ";
    }

    @Override
    protected void exportCommandLineData(StringBuilder out,
                                         int occ) {
        uncompileQuoter(out, getValue(occ).toString());
    }

    @Override
    protected void exportNamespaceData(String prefix,
                                       StringBuilder out,
                                       int occ) {
        out.append(prefix);
        out.append("=");
        out.append(getValue(occ).toString());
        out.append("\n");
    }

    @Override
    protected void exportXmlData(StringBuilder out,
                                 int occ) {
        xmlEncode(getValue(occ).toString(), out);
    }

    @Override
    public String genericClassName() {
        return "com.codetaco.math.Equ";
    }

    @Override
    public Object getDelegateOrValue() {
        return getValue().delegate;
    }

    @Override
    public Object getDelegateOrValue(int occurrence) {
        return getValue(occurrence).delegate;
    }

    @Override
    public Equ getValueAsEquation() {
        return getValue().delegate;
    }

    @Override
    public Equ[] getValueAsEquationArray() {
        Equ[] result = new Equ[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r).delegate;
        }

        return result;
    }

    @Override
    public boolean supportsCaseSensitive() {
        return true;
    }

    @Override
    public boolean supportsInList() {
        return false;
    }

}
