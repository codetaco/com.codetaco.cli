package com.codetaco.cli.impl.type;

import java.net.URL;

public class URLCLA extends AbstractCLA<URL> {

    @Override
    public URL convert(final String valueStr,
                       final boolean _caseSensitive,
                       final Object target) {
        try {
            if (_caseSensitive) {
                return new URL(valueStr);
            }
            return new URL(valueStr.toLowerCase());
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public String defaultInstanceClass() {
        return "java.net.URL";
    }

    @Override
    protected void exportCommandLineData(final StringBuilder out,
                                         final int occ) {
        uncompileQuoter(out, getValue(occ).getPath());
    }

    @Override
    protected void exportNamespaceData(final String prefix,
                                       final StringBuilder out,
                                       final int occ) {
        out.append(prefix);
        out.append("=");
        out.append(getValue(occ).getPath());
        out.append("\n");
    }

    @Override
    protected void exportXmlData(final StringBuilder out,
                                 final int occ) {
        xmlEncode(getValue(occ).getPath(), out);
    }

    @Override
    public String genericClassName() {
        return "java.net.URL";
    }

    @Override
    public URL[] getValueAsURLArray() {
        final URL[] result = new URL[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r);
        }

        return result;
    }

    @Override
    public boolean supportsCaseSensitive() {
        return true;
    }

    @Override
    public boolean supportsInList() {
        return false;
    }

}
