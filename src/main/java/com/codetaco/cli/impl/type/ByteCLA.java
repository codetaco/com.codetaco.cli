package com.codetaco.cli.impl.type;

import com.codetaco.cli.misc.ByteUtil;

public class ByteCLA extends AbstractCLA<Byte> {

    @Override
    public Byte convert(String valueStr, boolean _caseSensitive, Object target) {
        if (valueStr.length() == 1) {
            if (_caseSensitive) {
                return new Byte((byte) valueStr.charAt(0));
            }
            return new Byte((byte) valueStr.toLowerCase().charAt(0));
        }

        byte byteForLit = ByteUtil.byteForLiteral(valueStr);
        if (byteForLit != -1) {
            return byteForLit;
        }

        int intValue = 0;
        try {
            intValue = Integer.parseInt(valueStr);
        } catch (NumberFormatException e) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(valueStr);
            errMsg.append(" is not a valid byte code.  Try one of these codes or the corresponding number: ");
            ByteUtil.append(errMsg);
            throw new NumberFormatException(errMsg.toString());
        }
        if (intValue > 0xff) {
            throw new NumberFormatException(intValue + " is too large, max = 255");
        }
        return new Byte((byte) intValue);
    }

    @Override
    protected Byte naturalDefault() {
        return 0;
    }

    @Override
    public String defaultInstanceClass() {
        return "byte";
    }

    @Override
    protected void exportCommandLineData(StringBuilder out, int occ) {
        out.append(" ");
        out.append(getValue(occ));
    }

    @Override
    protected void exportNamespaceData(String prefix, StringBuilder out, int occ) {
        out.append(prefix);
        out.append("=");
        out.append(getValue(occ));
        out.append("\n");
    }

    @Override
    protected void exportXmlData(StringBuilder out, int occ) {
        xmlEncode(getValue(occ).toString(), out);
    }

    @Override
    public String genericClassName() {
        return "java.lang.Byte";
    }

    @Override
    public byte[] getValueAsbyteArray() {
        byte[] result = new byte[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r).byteValue();
        }

        return result;
    }

    @Override
    public Byte[] getValueAsByteArray() {
        Byte[] result = new Byte[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r);
        }

        return result;
    }
}
