package com.codetaco.cli.impl.type;

import com.codetaco.cli.CliException;

import java.text.ParseException;
import java.time.format.DateTimeFormatter;

public class DateTimeFormatterCLA extends AbstractCLA<ComparableDateTimeFormatter> {

    @Override
    public ComparableDateTimeFormatter convert(final String valueStr,
                                               final boolean _caseSensitive,
                                               final Object target) {
        try {
            return ComparableDateTimeFormatter.compile(valueStr);

        } catch (final IllegalArgumentException pse) {
            throw CliException.builder().cause(new ParseException(pse.getMessage(), 0)).build();
        }
    }

    @Override
    public String defaultInstanceClass() {
        return "java.time.format.DateTimeFormatter";
    }

    @Override
    protected void exportCommandLineData(final StringBuilder out,
                                         final int occ) {
        uncompileQuoter(out, getValue(occ).pattern);
    }

    @Override
    protected void exportNamespaceData(final String prefix,
                                       final StringBuilder out,
                                       final int occ) {
        out.append(prefix);
        out.append("=");
        out.append(getValue(occ).pattern);
        out.append("\n");
    }

    @Override
    protected void exportXmlData(final StringBuilder out,
                                 final int occ) {
        xmlEncode(getValue(occ).pattern, out);
    }

    @Override
    public String genericClassName() {
        return "java.time.format.DateTimeFormatter";
    }

    @Override
    public Object getDelegateOrValue() {
        return getValue().delegate;
    }

    @Override
    public Object getDelegateOrValue(final int occurrence) {
        return getValue(occurrence).delegate;
    }

    @Override
    public DateTimeFormatter getValueAsDateTimeFormatter() {
        return getValue().delegate;
    }

    @Override
    public DateTimeFormatter[] getValueAsDateTimeFormatterArray() {
        final DateTimeFormatter[] result = new DateTimeFormatter[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r).delegate;
        }

        return result;
    }

    @Override
    public boolean supportsCaseSensitive() {
        return false;
    }

    @Override
    public boolean supportsInList() {
        return false;
    }
}
