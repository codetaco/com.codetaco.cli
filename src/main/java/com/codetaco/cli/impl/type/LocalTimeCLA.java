package com.codetaco.cli.impl.type;

import com.codetaco.cli.CliException;
import com.codetaco.date.CalendarFactory;
import com.codetaco.date.impl.TemporalHelper;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class LocalTimeCLA extends AbstractCLA<LocalTime> {
    DateTimeFormatter dtf;

    @Override
    public LocalTime convert(final String valueStr,
                             final boolean _caseSensitive,
                             final Object target) {
        try {
            if (dtf == null) {
                if (getFormat() != null) {
                    try {
                        dtf = DateTimeFormatter.ofPattern(getFormat());
                    } catch (final Exception e) {
                        throw new ParseException("time format: " + e.getMessage(), 0);
                    }
                }
            }

            try {
                if (dtf == null) {
                    return CalendarFactory.asLocalTime(valueStr);
                }
                return LocalDateTime.parse(valueStr, dtf).toLocalTime();
            } catch (final Exception e) {
                throw new ParseException(toString() + " " + getFormat() + ": " + e.getMessage(), 0);
            }
        } catch (Exception e) {
            throw CliException.builder().cause(e).build();
        }
    }

    @Override
    public String defaultInstanceClass() {
        return "java.time.LocalTime";
    }

    @Override
    protected void exportCommandLineData(final StringBuilder out,
                                         final int occ) {
        out.append('"');
        out.append(TemporalHelper.getOutputTF().format(getValue(occ)));
        out.append('"');
    }

    @Override
    protected void exportNamespaceData(final String prefix,
                                       final StringBuilder out,
                                       final int occ) {
        out.append(prefix);
        out.append("=");
        out.append(TemporalHelper.getOutputTF().format(getValue(occ)));
        out.append("\n");
    }

    @Override
    protected void exportXmlData(final StringBuilder out,
                                 final int occ) {
        out.append(TemporalHelper.getOutputTF().format(getValue(occ)));
    }

    @Override
    public String genericClassName() {
        return "java.time.LocalTime";
    }

    @Override
    public LocalTime[] getValueAsLocalTimeArray() {
        final LocalTime[] result = new LocalTime[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r);
        }

        return result;
    }

    @Override
    public boolean supportsCaseSensitive() {
        return true;
    }

    @Override
    public boolean supportsFormat() {
        return true;
    }

    @Override
    public boolean supportsInList() {
        return false;
    }
}
