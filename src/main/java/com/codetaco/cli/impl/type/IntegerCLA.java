package com.codetaco.cli.impl.type;

import com.codetaco.cli.CliException;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class IntegerCLA extends AbstractCLA<Integer> {
    protected NumberFormat FMT = new DecimalFormat("0");

    @Override
    public Integer convert(final String valueStr,
                           final boolean _caseSensitive,
                           final Object target) {
        try {
            return FMT.parse(valueStr).intValue();
        } catch (Exception e) {
            throw CliException.builder().cause(e).build();
        }
    }

    @Override
    public String defaultInstanceClass() {
        return "int";
    }

    @Override
    protected Integer naturalDefault() {
        return 0;
    }

    @Override
    protected void exportCommandLineData(final StringBuilder out,
                                         final int occ) {
        if (getValue(occ) < 0) {
            out.append("'");
        }
        out.append(FMT.format(getValue(occ)));
        if (getValue(occ) < 0) {
            out.append("'");
        }
    }

    @Override
    protected void exportNamespaceData(final String prefix,
                                       final StringBuilder out,
                                       final int occ) {
        out.append(prefix);
        out.append("=");
        out.append(FMT.format(getValue(occ)));
        out.append("\n");
    }

    @Override
    protected void exportXmlData(final StringBuilder out,
                                 final int occ) {
        out.append(FMT.format(getValue(occ)));
    }

    @Override
    public String genericClassName() {
        return "java.lang.Integer";
    }

    @Override
    public float[] getValueAsfloatArray() {
        final float[] result = new float[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r).floatValue();
        }

        return result;
    }

    @Override
    public Float[] getValueAsFloatArray() {
        final Float[] result = new Float[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = new Float(getValue(r));
        }

        return result;
    }

    @Override
    public int[] getValueAsintArray() {
        final int[] result = new int[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r).intValue();
        }

        return result;
    }

    @Override
    public Integer[] getValueAsIntegerArray() {
        final Integer[] result = new Integer[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = new Integer(getValue(r));
        }

        return result;
    }

    @Override
    public Long[] getValueAsLongArray() {
        final Long[] result = new Long[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = new Long(getValue(r));
        }

        return result;
    }
}
