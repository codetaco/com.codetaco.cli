package com.codetaco.cli.impl.type;

import com.codetaco.math.Equ;

public class ComparableEqu implements Comparable<Equ> {
    public static ComparableEqu compile(String valueStr) throws Exception {
        ComparableEqu cp = new ComparableEqu();
        cp.delegate = Equ.builder(Object.class)
                        .equation(valueStr)
                        .build();
        return cp;
    }

    Equ delegate;

    @Override
    public int compareTo(Equ o) {
        return delegate.toString().compareTo(o.toString());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ComparableEqu other = (ComparableEqu) obj;
        if (delegate.toString() == null) {
            return other.delegate.toString() == null;
        } else {
            return delegate.toString().equals(other.delegate.toString());
        }
    }

    @Override
    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = prime * result + ((delegate.toString() == null)
                                     ? 0
                                     : delegate.toString().hashCode());
        return result;
    }

    @Override
    public String toString() {
        return delegate.toString();
    }
}
