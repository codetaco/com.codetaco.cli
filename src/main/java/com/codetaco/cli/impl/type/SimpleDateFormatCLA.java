package com.codetaco.cli.impl.type;

import com.codetaco.cli.CliException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class SimpleDateFormatCLA extends AbstractCLA<ComparableSimpleDateFormat> {
    /**
     * {@inheritDoc}
     */
    @Override
    public ComparableSimpleDateFormat convert(final String valueStr,
                                              final boolean _caseSensitive,
                                              final Object target) {
        try {
            return ComparableSimpleDateFormat.compile(valueStr);

        } catch (final IllegalArgumentException pse) {
            throw CliException.builder().cause(new ParseException(pse.getMessage(), 0)).build();
        }
    }

    @Override
    public String defaultInstanceClass() {
        return "java.text.SimpleDateFormat";
    }

    @Override
    protected void exportCommandLineData(final StringBuilder out,
                                         final int occ) {
        uncompileQuoter(out, getValue(occ).pattern);
    }

    @Override
    protected void exportNamespaceData(final String prefix,
                                       final StringBuilder out,
                                       final int occ) {
        out.append(prefix);
        out.append("=");
        out.append(getValue(occ).pattern);
        out.append("\n");
    }

    @Override
    protected void exportXmlData(final StringBuilder out,
                                 final int occ) {
        xmlEncode(getValue(occ).pattern, out);
    }

    @Override
    public String genericClassName() {
        return "java.text.SimpleDateFormat";
    }

    @Override
    public Object getDelegateOrValue() {
        return getValue().delegate;
    }

    @Override
    public Object getDelegateOrValue(final int occurrence) {
        return getValue(occurrence).delegate;
    }

    @Override
    public SimpleDateFormat getValueAsSimpleDateFormat() {
        return getValue().delegate;
    }

    @Override
    public SimpleDateFormat[] getValueAsSimpleDateFormatArray() {
        final SimpleDateFormat[] result = new SimpleDateFormat[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r).delegate;
        }

        return result;
    }

    @Override
    public boolean supportsCaseSensitive() {
        return false;
    }

    @Override
    public boolean supportsInList() {
        return false;
    }
}
