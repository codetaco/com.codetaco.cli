package com.codetaco.cli.impl.type;

import com.codetaco.cli.CliException;

import java.text.ParseException;

public class CharacterCLA extends AbstractCLA<Character> {
    @Override
    public Character convert(String valueStr,
                             boolean _caseSensitive,
                             Object target) {
        if (valueStr.length() == 1) {
            if (_caseSensitive) {
                return new Character(valueStr.charAt(0));
            }
            return new Character(valueStr.toLowerCase().charAt(0));
        }
        throw CliException.builder().cause(new ParseException("invalid value for character cli: " + valueStr, 0)).build();
    }

    @Override
    public String defaultInstanceClass() {
        return "char";
    }

    @Override
    protected void exportCommandLineData(StringBuilder out,
                                         int occ) {
        out.append(" ");
        out.append(getValue(occ));
    }

    @Override
    protected Character naturalDefault() {
        return 0;
    }

    @Override
    protected void exportNamespaceData(String prefix,
                                       StringBuilder out,
                                       int occ) {
        out.append(prefix);
        out.append("=");
        out.append(getValue(occ));
        out.append("\n");
    }

    @Override
    protected void exportXmlData(StringBuilder out,
                                 int occ) {
        xmlEncode(getValue(occ).toString(), out);
    }

    @Override
    public String genericClassName() {
        return "java.lang.Character";
    }

    @Override
    public Character[] getValueAsCharacterArray() {
        Character[] result = new Character[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r);
        }

        return result;
    }

    @Override
    public char[] getValueAscharArray() {
        char[] result = new char[size()];

        for (int r = 0; r < size(); r++) {
            result[r] = getValue(r).charValue();
        }

        return result;
    }
}
