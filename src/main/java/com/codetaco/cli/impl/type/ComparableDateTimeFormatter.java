package com.codetaco.cli.impl.type;

import java.time.format.DateTimeFormatter;

/**
 * <p>
 * ComparablePattern class.
 * </p>
 *
 * @author Chris DeGreef fedupforone@gmail.com
 * @since 4.3.1
 */
public class ComparableDateTimeFormatter implements Comparable<String> {
    /**
     * <p>
     * compile.
     * </p>
     *
     * @param valueStr a {@link java.lang.String} object.
     * @return a {@link com.codetaco.cli.impl.type.ComparableDateTimeFormatter}
     *   object.
     */
    public static ComparableDateTimeFormatter compile(String valueStr) {
        ComparableDateTimeFormatter cp = new ComparableDateTimeFormatter();
        cp.delegate = DateTimeFormatter.ofPattern(valueStr);
        cp.pattern = valueStr;
        return cp;
    }

    /**
     * <p>
     * compile.
     * </p>
     *
     * @param valueStr        a {@link java.lang.String} object.
     * @param caseInsensitive a int.
     * @return a {@link com.codetaco.cli.impl.type.ComparableDateTimeFormatter}
     *   object.
     */
    public static ComparableDateTimeFormatter compile(String valueStr, int caseInsensitive) {
        ComparableDateTimeFormatter cp = new ComparableDateTimeFormatter();
        cp.delegate = DateTimeFormatter.ofPattern(valueStr);
        cp.pattern = valueStr;
        return cp;
    }

    String pattern;
    DateTimeFormatter delegate;

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(String o) {
        return pattern.compareTo(o);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ComparableDateTimeFormatter other = (ComparableDateTimeFormatter) obj;
        if (pattern == null) {
            return other.pattern == null;
        } else {
            return pattern.equals(other.pattern);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = prime * result + ((pattern == null)
                                     ? 0
                                     : pattern.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return pattern;
    }
}
