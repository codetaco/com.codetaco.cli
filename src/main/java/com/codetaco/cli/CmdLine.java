package com.codetaco.cli;

import com.codetaco.cli.impl.CmdLineImpl;
import com.codetaco.cli.impl.type.CmdLineCLA;
import com.codetaco.cli.impl.type.ICmdLineArg;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The type Cmd line.
 */
public final class CmdLine {

    private List<String> args;
    private CmdLineImpl cmdLineImpl;
    private ParserType parserType;
    private Object target;
    private File propertyFile;

    /**
     * The type Builder.
     */
    public static class Builder {

        private String name = "[cli]";
        private String help = null;
        private char commandPrefix = '-';
        private char notPrefix = '!';
        private List<String> args;
        private ParserType parserType = ParserType.COMMAND_LINE;
        private Object target;
        private File propertyFile;
        private List<File> includeDirectories;

        private List<ParseException> parseExceptions;

        private Builder() {
            includeDirectories = new ArrayList<>();
            args = new ArrayList<>();
        }

        /**
         * For windows builder.
         *
         * @return the builder
         */
        public Builder forWindows() {
            commandPrefix = '/';
            notPrefix = '-';
            return this;
        }

        /**
         * Include directory builder.
         *
         * @param includeDirectory the include directory
         * @return the builder
         */
        public Builder includeDirectory(File includeDirectory) {
            includeDirectories.add(includeDirectory);
            return this;
        }

        /**
         * Name builder.
         *
         * @param name the name
         * @return the builder
         */
        public Builder name(String name) {
            this.name = name;
            return this;
        }

        /**
         * Target builder.
         *
         * @param target the target
         * @return the builder
         */
        public Builder target(Object target) {
            this.target = target;
            return this;
        }

        /**
         * Property file builder.
         *
         * @param propertyFile the property file
         * @return the builder
         */
        public Builder propertyFile(File propertyFile) {
            this.propertyFile = propertyFile;
            return this;
        }

        /**
         * Help builder.
         *
         * @param helpMessage the help message
         * @return the builder
         */
        public Builder help(String helpMessage) {
            help = helpMessage;
            return this;
        }

        /**
         * Command prefix builder.
         *
         * @param commandPrefix the command prefix
         * @return the builder
         */
        public Builder commandPrefix(char commandPrefix) {
            this.commandPrefix = commandPrefix;
            return this;
        }

        /**
         * Not prefix builder.
         *
         * @param notPrefix the not prefix
         * @return the builder
         */
        public Builder notPrefix(char notPrefix) {
            this.notPrefix = notPrefix;
            return this;
        }

        /**
         * Args builder.
         *
         * @param args the args
         * @return the builder
         */
        public Builder args(String... args) {
            Collections.addAll(this.args, args);
            return this;
        }

        /**
         * Parser type builder.
         *
         * @param parserType the parser type
         * @return the builder
         */
        public Builder parserType(ParserType parserType) {
            this.parserType = parserType;
            return this;
        }

        /**
         * Build cmd line.
         *
         * @return the cmd line
         */
        public CmdLine build() {

            if (target == null) {
                throw CliException.builder()
                        .cause(new Exception("target is required, usually \"this\""))
                        .build();
            }
            CmdLine cmdLine = new CmdLine();
            cmdLine.args = args;
            cmdLine.parserType = parserType;
            cmdLine.propertyFile = propertyFile;
            cmdLine.target = target;

            cmdLine.cmdLineImpl = new CmdLineImpl(name, help, commandPrefix, notPrefix);
            includeDirectories.forEach(cmdLine.cmdLineImpl::addDefaultIncludeDirectory);

            try {
                cmdLine.load();
            } catch (Exception e) {
                e.fillInStackTrace();
                parseExceptions = cmdLine.cmdLineImpl.getParseExceptions();
                throw e;
            }

            return cmdLine;
        }

        /**
         * Gets parse exceptions.
         *
         * @return the parse exceptions
         */
        public List<ParseException> getParseExceptions() {
            return parseExceptions;
        }
    }

    /**
     * Builder builder.
     *
     * @return the builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Is parsed boolean.
     *
     * @param argument the argument
     * @return the boolean
     */
    public boolean isParsed(String argument) {
        ICmdLineArg arg = cmdLineImpl.arg(argument);
        if (arg == null) {
            return false;
        }
        return arg.isParsed();
    }

    /**
     * Is usage boolean.
     *
     * @return the boolean
     */
    public boolean isUsage() {
        return cmdLineImpl.isUsageRun();
    }

    /**
     * Subparser cmd line.
     *
     * @param argument the argument
     * @return the cmd line
     */
    public CmdLine subparser(String argument) {
        ICmdLineArg arg = cmdLineImpl.arg(argument);
        if (arg == null) {
            return null;
        }
        if (arg instanceof CmdLineCLA) {
            CmdLine subparser = CmdLine.builder().target(target).build();
            subparser.cmdLineImpl = (CmdLineImpl) (((CmdLineCLA) arg).getValue());
            return subparser;
        }
        return null;
    }

    /**
     * Subparsers list.
     *
     * @param argument the argument
     * @return the list
     */
    public List<CmdLine> subparsers(String argument) {
        ICmdLineArg arg = cmdLineImpl.arg(argument);
        if (arg == null) {
            return null;
        }
        if (arg instanceof CmdLineCLA) {

            List<CmdLine> arrayOfParsers = new ArrayList<>();

            ((List<CmdLineImpl>) arg.getValues()).forEach(cl -> {
                CmdLine subparser = CmdLine.builder().target(target).build();
                subparser.cmdLineImpl = cl;
                arrayOfParsers.add(subparser);
            });

            return arrayOfParsers;
        }
        return null;
    }

    private void load() {

        switch (parserType) {
            case COMMAND_LINE:
                cmdLineImpl.load(target, args.toArray(new String[]{}));
                break;
            case NAME_SPACE:
                if (propertyFile != null) {
                    cmdLineImpl.loadProperties(target, propertyFile);
                } else {
                    cmdLineImpl.loadProperties(target, args.toArray(new String[]{}));
                }
                break;
            case XML:
                if (propertyFile != null) {
                    cmdLineImpl.loadXml(target, propertyFile);
                } else {
                    cmdLineImpl.loadXml(target, args.toArray(new String[]{}));
                }
                break;
            default:
                throw CliException.builder()
                        .cause(new Exception("unknown parserType"))
                        .build();
        }
    }

    /**
     * Pull.
     *
     * @deprecated
     */
    @Deprecated
    public void pull() {
        cmdLineImpl.pull(target);
    }

    /**
     * Export string.
     *
     * @param parserType the parser type
     * @return the string
     * @deprecated
     */
    @Deprecated
    public String export(ParserType parserType) {
        return export(parserType, "");
    }

    /**
     * Export string.
     *
     * @param parserType the parser type
     * @param prefix     the prefix
     * @return the string
     * @deprecated
     */
    @Deprecated
    public String export(ParserType parserType, String prefix) {

        StringBuilder stringBuilder = new StringBuilder();
        switch (parserType) {
            case COMMAND_LINE:
                cmdLineImpl.exportCommandLine(stringBuilder);
                break;
            case NAME_SPACE:
                cmdLineImpl.exportNamespace(prefix, stringBuilder);
                break;
            case XML:
                cmdLineImpl.exportXml(prefix, stringBuilder);
                break;
            default:
                throw CliException.builder()
                        .cause(new Exception("unknown parserType"))
                        .build();
        }
        return stringBuilder.toString();
    }
}
