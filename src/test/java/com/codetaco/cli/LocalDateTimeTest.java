package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.date.CalendarFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class LocalDateTimeTest {

    @BeforeEach
    public void setZoneToLocalTime() {
        CalendarFactory.setZoneId(ZoneOffset.ofHours(-5));
    }

    @AfterEach
    public void setZoneToUTC() {
        CalendarFactory.setZoneId(ZoneOffset.UTC);
    }

    static private void assertValidReturnDate(LocalDateTime toBeVerified,
                                              int year,
                                              Month month,
                                              int _date,
                                              int hour,
                                              int minute,
                                              int second,
                                              int nano) {
        Assertions.assertEquals(year, toBeVerified.getYear());
        Assertions.assertEquals(month, toBeVerified.getMonth());
        Assertions.assertEquals(_date, toBeVerified.getDayOfMonth());
        Assertions.assertEquals(hour, toBeVerified.getHour());
        Assertions.assertEquals(minute, toBeVerified.getMinute());
        Assertions.assertEquals(second, toBeVerified.getSecond());
        Assertions.assertEquals(nano, toBeVerified.getNano());
    }

    @Arg
    DateTimeFormatter formatter;

    @Arg
    LocalDateTime ldtArg;

    @Arg(caseSensitive = true, defaultValues = "now")
    LocalDateTime ldtForHelp;

    @Arg
    LocalDateTime[] ldtArray;

    @Arg
    List<LocalDateTime> ldtList;

    @Test
    public void arrays() {

        CmdLine.builder()
          .target(this)
          .args("--ldtArray '2016-08-06' '2016-08-06T14:18-05:00'")
          .build();

        assertValidReturnDate(ldtArray[0], 2016, Month.AUGUST, 6, 0, 0, 0, 0);
        assertValidReturnDate(ldtArray[1], 2016, Month.AUGUST, 6, 14, 18, 0, 0);
    }

    @Test
    public void format() {

        CmdLine.builder()
          .target(this)
          .args("--formatter 'MMM-d-yy' --ldtArg '2016-08-06'")
          .build();

        Assertions.assertNotNull(formatter);
        Assertions.assertEquals("Aug-6-16", formatter.format(ldtArg));
    }

    @Test
    public void helpDefaultNow() {

        CmdLine.builder()
          .target(this)
          .args("--help")
          .build();
    }

    @Test
    public void json() {

        CmdLine.builder()
          .target(this)
          .args("--ldtArg '2016-03-24T11:19:52.000-05:00'")
          .build();

        assertValidReturnDate(ldtArg, 2016, Month.MARCH, 24, 11, 19, 52, 0);
    }

    @Test
    public void lists() {

        CmdLine.builder()
          .target(this)
          .args("--ldtList '2016-08-06' '2016-08-06T14:18-05:00'")
          .build();

        assertValidReturnDate(ldtList.get(0), 2016, Month.AUGUST, 6, 0, 0, 0, 0);
        assertValidReturnDate(ldtList.get(1), 2016, Month.AUGUST, 6, 14, 18, 0, 0);
    }

    @Test
    public void simpleUse() {

        CmdLine.builder()
          .target(this)
          .args("--ldtArg '2016-08-06'")
          .build();

        assertValidReturnDate(ldtArg, 2016, Month.AUGUST, 6, 0, 0, 0, 0);
    }
}
