package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class PullTests {
    static final public class InnerParm {
        @Arg
        private String innerParmString;
    }

    @Arg
    private String updatedStr;
    @Arg
    private Integer updatedInteger;
    @Arg
    private int updatedInt;
    @Arg
    private String[] updatedStrArray;
    @Arg
    private List<String> updatedStrList;
    @Arg
    private InnerParm innerParm;
    @Arg
    private InnerParm[] innerParmArray;
    @Arg
    private List<InnerParm> innerParmList;

    public PullTests() {
    }

    @Test
    public void updateArray() {

        CmdLine cl = CmdLine.builder()
                       .target(this)
                       .args("--updatedStrArray aaa, bbb")
                       .build();

        Assertions.assertEquals("aaa", updatedStrArray[0]);
        Assertions.assertEquals("bbb", updatedStrArray[1]);

        updatedStrArray = new String[]{"xxx", "yyy", "zzz"};

        cl.pull();
        String export = cl.export(ParserType.COMMAND_LINE);
        Assertions.assertEquals("--updatedStrArray 'xxx' 'yyy' 'zzz'", export);
    }

    @Test
    public void updateInnerParm() {

        CmdLine cl = CmdLine.builder()
                       .target(this)
                       .args("--innerParm(--innerParmString ccc)")
                       .build();

        Assertions.assertEquals("ccc", innerParm.innerParmString);

        innerParm = new InnerParm();
        innerParm.innerParmString = "ddd";

        cl.pull();
        String export = cl.export(ParserType.COMMAND_LINE);
        Assertions.assertEquals("--innerParm [--innerParmString 'ddd']", export);
    }

    @Test
    public void updateInnerParmArray() {

        CmdLine cl = CmdLine.builder()
                       .target(this)
                       .args("--innerParmArray(--innerParmString ccc)(--innerParmString ddd)")
                       .build();

        Assertions.assertEquals("ccc", innerParmArray[0].innerParmString);
        Assertions.assertEquals("ddd", innerParmArray[1].innerParmString);

        innerParmArray = new InnerParm[3];
        innerParmArray[0] = new InnerParm();
        innerParmArray[0].innerParmString = "xxx";
        innerParmArray[1] = new InnerParm();
        innerParmArray[1].innerParmString = "yyy";
        innerParmArray[2] = new InnerParm();
        innerParmArray[2].innerParmString = "zzz";

        cl.pull();
        String export = cl.export(ParserType.COMMAND_LINE);
        Assertions.assertEquals(
          "--innerParmArray [--innerParmString 'xxx'] [--innerParmString 'yyy'] [--innerParmString 'zzz']"
          , export);
    }

    @Test
    public void updateInnerParmList() {

        CmdLine cl = CmdLine.builder()
                       .target(this)
                       .args("--innerParmList(--innerParmString ccc)(--innerParmString ddd)")
                       .build();

        Assertions.assertEquals("ccc", innerParmList.get(0).innerParmString);
        Assertions.assertEquals("ddd", innerParmList.get(1).innerParmString);

        innerParmList.clear();
        InnerParm ip = null;
        innerParmList.add(ip = new InnerParm());
        ip.innerParmString = "xxx";
        innerParmList.add(ip = new InnerParm());
        ip.innerParmString = "yyy";
        innerParmList.add(ip = new InnerParm());
        ip.innerParmString = "zzz";

        cl.pull();
        String export = cl.export(ParserType.COMMAND_LINE);
        Assertions.assertEquals(
          "--innerParmList [--innerParmString 'xxx'] [--innerParmString 'yyy'] [--innerParmString 'zzz']",
          export);
    }

    @Test
    public void updateList() {

        CmdLine cl = CmdLine.builder()
                       .target(this)
                       .args("--updatedStrList aaa, bbb")
                       .build();

        Assertions.assertEquals("aaa", updatedStrList.get(0));
        Assertions.assertEquals("bbb", updatedStrList.get(1));

        updatedStrList = new ArrayList<>();
        updatedStrList.add("xxx");
        updatedStrList.add("yyy");
        updatedStrList.add("zzz");

        cl.pull();
        String export = cl.export(ParserType.COMMAND_LINE);
        Assertions.assertEquals("--updatedStrList 'xxx' 'yyy' 'zzz'", export);
    }

    @Test
    public void updateSimple() {

        CmdLine cl = CmdLine.builder()
                       .target(this)
                       .args("--updatedStr was")
                       .args("--updatedInteger 1")
                       .args("--updatedInt 2")
                       .build();

        Assertions.assertEquals("was", updatedStr);
        Assertions.assertEquals(1, updatedInteger.intValue());
        Assertions.assertEquals(2, updatedInt);

        updatedStr = "is";
        updatedInteger = 2;
        updatedInt = 0;

        cl.pull();
        String export = cl.export(ParserType.COMMAND_LINE);
        Assertions.assertEquals("--updatedStr 'is' --updatedInteger 2", export);
    }
}
