package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MultipleTest {
    static public class StringGroup {
        @Arg(shortName = 'i')
        String stringArray;
    }

    @Arg(multimin = 3)
    String[] stringArray;

    @Arg(multimin = 3)
    StringGroup[] stringGroupMulti;

    @Arg
    StringGroup stringGroup;

    @Test
    public void minOnlyFail() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--stringArray s")
              .build();

            Assertions.fail("error should have occurred");

        } catch (Exception e) {
            Assertions.assertEquals("insufficient required values for --stringArray", e.getMessage());
        }
    }

    @Test
    public void minOnlyFailGroup() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--stringGroupMulti( -i ONE )( -i TWO )")
              .build();

            Assertions.fail("error should have occurred");

        } catch (Exception e) {
            Assertions.assertEquals("insufficient required values for --stringGroupMulti", e.getMessage());
        }
    }

    @Test
    public void oneOnlyFailGroup() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--stringGroup( -i ONE )( -i TWO )")
              .build();

            Assertions.fail("error should have occurred");

        } catch (Exception e) {
            Assertions.assertEquals("multiple values not allowed for --stringGroup", e.getMessage());
        }
    }
}
