package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RepeatParmTest {
    @Arg(shortName = 'p', defaultValues = {"1", "2", "3", "4"})
    private int[] intArrayWithDefault;

    @Arg(shortName = 'o')
    private int[] intArray;

    @Arg(shortName = 'u')
    private int maxUpdates = 0;

    @Arg(shortName = 'v', defaultValues = "-999")
    private int maxUpdatesWithDefault = 0;

    @Arg(positional = true, longName = "pos")
    @Arg
    private String[] someStrings;

    @Arg(shortName = 'm')
    private boolean bool;

    @Test
    public void repeatedBoolean() {

        CmdLine.builder()
          .target(this)
          .args("-mmmmmmmmmm")
          .build();

        Assertions.assertTrue(bool);
    }

    @Test
    public void repeatedMultiple() {

        CmdLine.builder()
          .target(this)
          .args("--somestrings first --somestrings second")
          .build();

        Assertions.assertEquals("first", someStrings[0]);
        Assertions.assertEquals("second", someStrings[1]);
    }

    @Test
    public void repeatedNonMultiple() {
        try {

            CmdLine.builder()
              .target(this)
              .args("-u1-!u-u2")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception e) {
            Assertions.assertEquals(
              "multiple values not allowed for --maxUpdates(-u)",
              e.getMessage());
        }
    }

    @Test
    public void repeatedPositional() {

        CmdLine.builder()
          .target(this)
          .args("first second\"")
          .build();

        Assertions.assertEquals("first", someStrings[0]);
        Assertions.assertEquals("second", someStrings[1]);
    }

    @Test
    public void testTurnOffIntegerListOnly() {

        CmdLine.builder()
          .target(this)
          .args("-!o")
          .build();

        Assertions.assertNull(intArray);
    }

    @Test
    public void turnOffFirst() {

        CmdLine.builder()
          .target(this)
          .args("-!m -m")
          .build();

        Assertions.assertFalse(bool);
    }

    @Test
    public void turnOffInteger() {

        CmdLine.builder()
          .target(this)
          .args("-v1 -!v")
          .build();

        Assertions.assertEquals(-999, maxUpdatesWithDefault);
    }

    @Test
    public void turnOffIntegerList() {

        CmdLine.builder()
          .target(this)
          .args("--intArray 1,2,3 -!intArray")
          .build();

        Assertions.assertNull(intArray);
    }

    @Test
    public void turnOffIntegerListWithDef() {

        CmdLine.builder()
          .target(this)
          .args("-p 1,2,3 -!p")
          .build();

        Assertions.assertEquals(4, intArrayWithDefault.length);
    }

    @Test
    public void turnOffIntegerOnly() {

        CmdLine.builder()
          .target(this)
          .args("-!v")
          .build();

        Assertions.assertEquals(-999, maxUpdatesWithDefault);
    }

    @Test
    public void turnOffLast() {

        CmdLine.builder()
          .target(this)
          .args("-m -!m")
          .build();

        Assertions.assertFalse(bool);
    }
}
