package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.cli.annotation.ArgCallback;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.List;

public class CallbackTests {
    static public class Cfg {
        @Arg(inList = {"lower", "UPPER"})
        String lowerCaseResult;

        @Arg(inList = {"lower", "UPPER"}, caseSensitive = true)
        String caseMattersResult;

        @ArgCallback(postParse = true)
        private void callMeWhenDone() throws ParseException {
            System.out.println("Cfg called on postParse");
        }
    }

    @Arg
    private Cfg[] cfgArray;

    @Arg
    private List<Cfg> cfgList;

    @Test
    public void callbackToSingleInstance() {
        Cfg target = new Cfg();

        CmdLine.builder()
          .target(target)
          .args("--lowerCaseResult LOWER")
          .build();

        Assertions.assertEquals("lower", target.lowerCaseResult);
    }

    @Test
    public void callbackToArray() {

        CmdLine.builder()
          .target(this)
          .args("--cfgArray (--LCR LOWER)(--LCaseR UPPER)")
          .build();

        Assertions.assertEquals("lower", cfgArray[0].lowerCaseResult);
    }

    @Test
    public void callbackToList() {

        CmdLine.builder()
          .target(this)
          .args("--cfgList (--LCR LOWER)(--lCR UPPER)")
          .build();

        Assertions.assertEquals("lower", cfgList.get(0).lowerCaseResult);
    }

    @ArgCallback(postParse = true)
    private void callMeWhenDone() {
        System.out.println("CallbackTests called on postParse");
    }

}
