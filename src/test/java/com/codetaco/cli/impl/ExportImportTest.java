package com.codetaco.cli.impl;

import com.codetaco.cli.CmdLine;
import com.codetaco.cli.ParserType;
import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.management.MemoryType;

public class ExportImportTest {
    static public class BooleanALong {
        @Arg(shortName = 'a', longName = "long")
        boolean along;

        @Arg(shortName = 'b', longName = "short")
        boolean bshort;
    }

    static public class Export1 {
        @Arg(longName = "boolean")
        private boolean b1;

        @Arg
        private int[] hello;
    }

    static public class ExportEmums {
        @Arg(shortName = 'a', defaultValues = "Heap")
        MemoryType type1;

        @Arg(shortName = 'b', defaultValues = "Heap", caseSensitive = true)
        MemoryType type2;
    }

    static public class ExportNumbers {
        @Arg(shortName = 'f')
        float[] floater;

        @Arg(shortName = 'i')
        int[] integers;
    }

    static public class HelloGoodbye {
        @Arg(longName = "Hello")
        private String hello;

        @Arg
        private String goodbye;
    }

    static public class Master01 {
        @Arg
        Master02 my;
    }

    static public class Master02 {
        @Arg
        Master03[] hello;
    }

    static public class Master03 {
        @Arg
        int moon;

        @Arg
        int world;
    }

    static public class Master04 {
        @Arg(shortName = 'a')
        Master05 a;
    }

    static public class Master05 {
        @Arg(shortName = 'b')
        Master06[] a;
    }

    static public class Master06 {
        @Arg(shortName = 'c')
        Master07 a;
    }

    static public class Master07 {
        @Arg(shortName = 'd')
        String[] a;
    }

    static public class Master08 {
        @Arg(shortName = 'a')
        Master09 a;
    }

    static public class Master09 {
        @Arg(shortName = 'b', positional = true)
        Master10 a;
    }

    static public class Master10 {
        @Arg(shortName = 'c')
        String[] a;
    }

    static public class Master11 {
        @Arg(shortName = 'a')
        Master12 a;
    }

    static public class Master12 {
        @Arg(shortName = 'b')
        Master13 a;
        @Arg(shortName = 'd')
        Master14 d;
    }

    static public class Master13 {
        @Arg(shortName = 'c')
        String a;
    }

    static public class Master14 {
        @Arg(shortName = 'z')
        String a;
    }

    static public class Master15 {
        @Arg(shortName = 'a')
        Master16 a;
    }

    static public class Master16 {
        @Arg(shortName = 'b', positional = true)
        Master17[] a;
    }

    static public class Master17 {
        @Arg(shortName = 'c')
        String a;
    }

    static public class Master18 {
        @Arg(shortName = 'a')
        Master19 a;
    }

    static public class Master19 {
        @Arg(shortName = 'b', positional = true)
        Master20[] a;
    }

    static public class Master20 {
        @Arg(shortName = 'c')
        Master21 a;
    }

    static public class Master21 {
        @Arg(shortName = 'd', positional = true)
        String[] a;
    }

    static public class Master22 {
        @Arg(shortName = 'a')
        Master23 a;
    }

    static public class Master23 {
        @Arg(shortName = 'b')
        Master24 a;
    }

    static public class Master24 {
        @Arg(shortName = 'c')
        String a;
    }

    static public class Master25 {
        @Arg(shortName = 'a')
        Master26 a;
    }

    static public class Master26 {
        @Arg(shortName = 'b')
        Master27 b;
    }

    static public class Master27 {
        @Arg(shortName = 'c')
        Master28 c;
    }

    static public class Master28 {
        @Arg(shortName = 'z', positional = true)
        String z;
    }

    static public class Master29 {
        @Arg(shortName = 'a')
        Master30 a;
    }

    static public class Master30 {
        @Arg(shortName = 'b')
        Master31 a;
    }

    static public class Master31 {
        @Arg(shortName = 'c')
        String[] a;
    }

    static public class Master32 {
        @Arg(shortName = 'a')
        Master33 a;
    }

    static public class Master33 {
        @Arg(shortName = 'b')
        Master34 a;
    }

    static public class Master34 {
        @Arg(shortName = 'c')
        Master35 a;
    }

    static public class Master35 {
        @Arg(shortName = 'd', positional = true)
        String[] a;
    }

    static public class Master36 {
        @Arg(shortName = 'a')
        Master37 a;
    }

    static public class Master37 {
        @Arg(shortName = 'b', positional = true)
        Master38 a;
    }

    static public class Master38 {
        @Arg(shortName = 'c')
        String a;
    }

    static public class Master39 {
        @Arg
        Master40[] hello;
    }

    static public class Master40 {
        @Arg
        private int moon;

        @Arg
        private int world;
    }

    static public class Master41 {
        @Arg
        Master39[] my;
    }

    static public class Master42 {
        @Arg(shortName = 'a')
        Master43 a;
        @Arg
        Master40 hello;
    }

    static public class Master43 {
        @Arg(shortName = 'b')
        Master44 a;
    }

    static public class Master44 {
        @Arg(shortName = 'c')
        Master45 a;
    }

    static public class Master45 {
        @Arg(shortName = 'd')
        String[] a;
    }

    static private void verify(Object target,
                               CmdLine cmdLine,
                               String expectedNamespace,
                               String expectedXML,
                               String expectedCommandLine) {

        verifyParserType(ParserType.NAME_SPACE, "", target, cmdLine, expectedNamespace);
        verifyParserType(ParserType.XML, "cmdline", target, cmdLine, expectedXML);
        verifyParserType(ParserType.COMMAND_LINE, "", target, cmdLine, expectedCommandLine);
    }

    private static void verifyParserType(ParserType parserType,
                                         String prefix,
                                         Object target,
                                         CmdLine cmdLine,
                                         String expectedExport) {
        if (expectedExport == null) {
            return;
        }
        try {
            String exported = cmdLine.export(parserType, prefix);
            String[] exportedAsLines = exported.split("\n");
            Assertions.assertEquals(expectedExport,
                                    exported);

            CmdLine verifiableCmdLine = CmdLine.builder()
                                          .target(target)
                                          .parserType(parserType)
                                          .args(exportedAsLines)
                                          .build();
            String verifyExport = verifiableCmdLine.export(parserType, prefix);

            Assertions.assertEquals(exported,
                                    verifyExport);
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail(parserType + " " + expectedExport + " " + e.getMessage());
        }
    }

    @Test
    public void exportBoolean1() {
        Object target = new BooleanALong();

        CmdLine cmdLine = CmdLine.builder()
                            .target(target)
                            .args("-a")
                            .build();
        verify(target, cmdLine,
               "a=\n",
               "<cmdline><a/></cmdline>",
               "-a");
    }

    @Test
    public void exportBoolean2() {
        Object target = new BooleanALong();

        CmdLine cmdLine = CmdLine.builder()
                            .target(target)
                            .args("--long")
                            .build();
        verify(target, cmdLine,
               "a=\n",
               "<cmdline><a/></cmdline>",
               "-a");
    }

    @Test
    public void exportBoolean3() {
        Object target = new BooleanALong();

        CmdLine cmdLine = CmdLine.builder()
                            .target(target)
                            .args("--long -b")
                            .build();
        verify(target, cmdLine,
               "a=\nb=\n",
               "<cmdline><a/><b/></cmdline>",
               "-a -b");
    }

    @Test
    public void exportBoolean4() {
        Object target = new BooleanALong();

        CmdLine cmdLine = CmdLine.builder()
                            .target(target)
                            .args("-b--long")
                            .build();
        verify(target, cmdLine,
               "a=\nb=\n",
               "<cmdline><a/><b/></cmdline>",
               "-a -b");
    }

    @Test
    public void exportBoolean5() {
        Object target = new BooleanALong();

        CmdLine cmdLine = CmdLine.builder()
                            .target(target)
                            .args("-b")
                            .build();
        verify(target, cmdLine,
               "b=\n",
               "<cmdline><b/></cmdline>",
               "-b");
    }

    @Test
    public void exportEnumDefault1() {
        Object target = new ExportEmums();

        CmdLine cmdLine = CmdLine.builder()
                            .target(target)
                            .args("")
                            .build();
        verify(target, cmdLine,
               "",
               "<cmdline></cmdline>",
               "");
    }

    @Test
    public void exportEnumDefault2() {
        Object target = new ExportEmums();

        CmdLine cmdLine = CmdLine.builder()
                            .target(target)
                            .args("-aHEAP")
                            .build();
        verify(target, cmdLine,
               "",
               "<cmdline></cmdline>",
               "");
    }

    @Test
    public void exportEnumDefault3() {
        Object target = new ExportEmums();

        CmdLine cmdLine = CmdLine.builder()
                            .target(target)
                            .args("-aheap")
                            .build();
        verify(target, cmdLine,
               "",
               "<cmdline></cmdline>",
               "");
    }

    @Test
    public void exportEnumDefaultWithCaseMattering1() {
        Object target = new ExportEmums();

        CmdLine cmdLine = CmdLine.builder()
                            .target(target)
                            .args("-bHeap")
                            .build();
        verify(target, cmdLine,
               "",
               "<cmdline></cmdline>",
               "");
    }

    @Test
    public void exportFloat1() {
        Object target = new ExportNumbers();

        CmdLine cmdLine = CmdLine.builder()
                            .target(target)
                            .args("-f 1500.25")
                            .build();
        verify(target, cmdLine,
               "f[0]=1500.25\n",
               "<cmdline><f>1500.25</f></cmdline>",
               "-f1500.25");
    }

    @Test
    public void exportFloat2() {
        Object target = new ExportNumbers();

        CmdLine cmdLine = CmdLine.builder()
                            .target(target)
                            .args("-f 1 2.001")
                            .build();
        verify(target, cmdLine,
               "f[0]=1\nf[1]=2.001\n",
               "<cmdline><f>1</f><f>2.001</f></cmdline>",
               "-f1 2.001");
    }

    @Test
    public void exportFloat3() {
        Object target = new ExportNumbers();

        CmdLine cmdLine = CmdLine.builder()
                            .target(target)
                            .args("-f -1500.25")
                            .build();
        verify(target, cmdLine,
               "f[0]=-1500.25\n",
               "<cmdline><f>-1500.25</f></cmdline>",
               "-f'-1500.25'");
    }

    @Test
    public void exportInt1() {
        Object target = new ExportNumbers();

        CmdLine cmdLine = CmdLine.builder()
                            .target(target)
                            .args("-i 1500")
                            .build();
        verify(target, cmdLine,
               "i[0]=1500\n",
               "<cmdline><i>1500</i></cmdline>",
               "-i1500");
    }

    @Test
    public void exportInt2() {
        Object target = new ExportNumbers();

        CmdLine cmdLine = CmdLine.builder()
                            .target(target)
                            .args("-i 1 2")
                            .build();
        verify(target, cmdLine,
               "i[0]=1\ni[1]=2\n",
               "<cmdline><i>1</i><i>2</i></cmdline>",
               "-i1 2");
    }

    @Test
    public void testArray() {
        Export1 target = new Export1();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("hello[0]=0")
                            .args("hello[1]=1")
                            .build();
        verify(target, cmdLine,
               "hello[0]=0\nhello[1]=1\n",
               "<cmdline><hello>0</hello><hello>1</hello></cmdline>",
               "--hello 0 1");
    }

    @Test
    public void testboolean() {
        Export1 target = new Export1();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("boolean=")
                            .build();
        verify(target, cmdLine,
               "boolean=\n",
               "<cmdline><boolean/></cmdline>",
               "--boolean");
    }

    @Test
    public void testEmbeddedLevelArray() {
        Master01 target = new Master01();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("my.hello[0].world=10")
                            .args("my.hello[0].moon=1")
                            .args("my.hello[1].moon=2")
                            .build();
        verify(target, cmdLine,
               "my.hello[0].moon=1\nmy.hello[0].world=10\nmy.hello[1].moon=2\n",
               "<cmdline><my><hello><moon>1</moon><world>10</world></hello><hello><moon>2</moon></hello></my></cmdline>",
               "--my [--hello [--moon 1 --world 10] [--moon 2]]");
    }

    @Test
    public void namespace() {
        Master04 target = new Master04();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a.b.c.d=alpha")
                            .build();
        verify(target, cmdLine,
               "a.b[0].c.d[0]=alpha\n",
               "<cmdline><a><b><c><d>alpha</d></c></b></a></cmdline>",
               "-a[-b[-c[-d'alpha']]]");
    }

    @Test
    public void namespacePos2b() {
        Master08 target = new Master08();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a..c=alpha")
                            .build();
        verify(target, cmdLine,
               "a..c[0]=alpha\n",
               "<cmdline><a><noname><c>alpha</c></noname></a></cmdline>",
               "-a[ [-c'alpha']]");
    }

    @Test
    public void namespacePos2Changes() {
        Object target = new Master11();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a.b.c=alpha")
                            .args("a.d.z=omega")
                            .build();
        verify(target, cmdLine,
               "a.b.c=alpha\na.d.z=omega\n",
               "<cmdline><a><b><c>alpha</c></b><d><z>omega</z></d></a></cmdline>",
               "-a[-b[-c'alpha'] -d[-z'omega']]");
    }

    @Test
    public void namespacePos2twicea() {
        Object target = new Master08();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a..c=alpha")
                            .args("a..c=omega")
                            .build();
        verify(target, cmdLine,
               "a..c[0]=alpha\na..c[1]=omega\n",
               "<cmdline><a><noname><c>alpha</c><c>omega</c></noname></a></cmdline>",
               "-a[ [-c'alpha' 'omega']]");
    }

    @Test
    public void namespacePos2twiceaWithOrder() {
        Object target = new Master15();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a.[1].c=alpha")
                            .args("a.[0].c=omega")
                            .build();
        verify(target, cmdLine,
               "a.[0].c=omega\na.[1].c=alpha\n",
               "<cmdline><a><noname><c>omega</c></noname><noname><c>alpha</c></noname></a></cmdline>",
               "-a[ [-c'omega'] [-c'alpha']]");
    }

    @Test
    public void namespacePos2twiceb() {
        Object target = new Master18();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a..c.[0]=alpha")
                            .args("a..c.[1]=omega")
                            .build();
        verify(target, cmdLine,
               "a.[0].c.[0]=alpha\na.[0].c.[1]=omega\n",
               "<cmdline><a><noname><c><noname>alpha</noname><noname>omega</noname></c></noname></a></cmdline>",
               "-a[ [-c[ 'alpha' 'omega']]]");
    }

    @Test
    public void namespacePos4a() {
        Object target = new Master22();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a.b.c=alpha")
                            .build();
        verify(target, cmdLine,
               "a.b.c=alpha\n",
               "<cmdline><a><b><c>alpha</c></b></a></cmdline>",
               "-a[-b[-c'alpha']]");
    }

    @Test
    public void namespaceWhiteSpace1() {
        Object target = new Master22();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a.b.c = alpha")
                            .build();
        verify(target, cmdLine,
               "a.b.c=alpha\n",
               "<cmdline><a><b><c>alpha</c></b></a></cmdline>",
               "-a[-b[-c'alpha']]");
    }

    @Test
    public void namespaceWhiteSpace2() {
        Object target = new Master22();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a.b.c= alpha")
                            .build();
        verify(target, cmdLine,
               "a.b.c=alpha\n",
               "<cmdline><a><b><c>alpha</c></b></a></cmdline>",
               "-a[-b[-c'alpha']]");
    }

    @Test
    public void namespaceWhiteSpace3() {
        Object target = new Master22();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a.b.c =alpha")
                            .build();
        verify(target, cmdLine,
               "a.b.c=alpha\n",
               "<cmdline><a><b><c>alpha</c></b></a></cmdline>",
               "-a[-b[-c'alpha']]");
    }

    @Test
    public void namespaceWhiteSpace4() {
        Object target = new Master22();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args(" a.b.c=alpha")
                            .build();
        verify(target, cmdLine,
               "a.b.c=alpha\n",
               "<cmdline><a><b><c>alpha</c></b></a></cmdline>",
               "-a[-b[-c'alpha']]");
    }

    @Test
    public void namespaceWhiteSpace5() {
        Object target = new Master22();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a.b.c=alpha ")
                            .build();
        verify(target, cmdLine,
               "a.b.c=alpha\n",
               "<cmdline><a><b><c>alpha</c></b></a></cmdline>",
               "-a[-b[-c'alpha']]");
    }

    @Test
    public void namespaceWhiteSpace6() {
        Object target = new Master25();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a.b.c.[0] = alpha")
                            .build();
        verify(target, cmdLine,
               "a.b.c.=alpha\n",
               "<cmdline><a><b><c><noname>alpha</noname></c></b></a></cmdline>",
               "-a[-b[-c[ 'alpha']]]");
    }

    @Test
    public void namespaceWhiteSpace7() {
        Object target = new Master25();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a.b.c.[0] =alpha")
                            .build();
        verify(target, cmdLine,
               "a.b.c.=alpha\n",
               "<cmdline><a><b><c><noname>alpha</noname></c></b></a></cmdline>",
               "-a[-b[-c[ 'alpha']]]");
    }

    @Test
    public void namespacePos4b() {
        Object target = new Master25();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a.b.c.[0]=alpha")
                            .build();
        verify(target, cmdLine,
               "a.b.c.=alpha\n",
               "<cmdline><a><b><c><noname>alpha</noname></c></b></a></cmdline>",
               "-a[-b[-c[ 'alpha']]]");
    }

    @Test
    public void namespacePos4twicea() {
        Object target = new Master29();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a.b.c=alpha")
                            .args("a.b.c=omega")
                            .build();
        verify(target, cmdLine,
               "a.b.c[0]=alpha\na.b.c[1]=omega\n",
               "<cmdline><a><b><c>alpha</c><c>omega</c></b></a></cmdline>",
               "-a[-b[-c'alpha' 'omega']]");
    }

    @Test
    public void namespacePos4twiceaWithOrder() {
        Object target = new Master29();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a.b.c[1]=alpha")
                            .args("a.b.c[0]=omega")
                            .build();
        verify(target, cmdLine,
               "a.b.c[0]=omega\na.b.c[1]=alpha\n",
               "<cmdline><a><b><c>omega</c><c>alpha</c></b></a></cmdline>",
               "-a[-b[-c'omega' 'alpha']]");
    }

    @Test
    public void namespacePos4twiceb() {
        Object target = new Master32();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a.b.c.[0]=alpha")
                            .args("a.b.c.[1]=omega")
                            .build();
        verify(target, cmdLine,
               "a.b.c.[0]=alpha\na.b.c.[1]=omega\n",
               "<cmdline><a><b><c><noname>alpha</noname><noname>omega</noname></c></b></a></cmdline>",
               "-a[-b[-c[ 'alpha' 'omega']]]");
    }

    @Test
    public void namespacePosGroupNotMultiple() {
        Object target = new Master36();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("a..c=alpha")
                            .build();
        verify(target, cmdLine,
               "a..c=alpha\n",
               "<cmdline><a><noname><c>alpha</c></noname></a></cmdline>",
               "-a[ [-c'alpha']]");
    }

    @Test
    public void testPositionalArray() {
        Object target = new Master21();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("[0]=world")
                            .args("[1]=for now")
                            .build();
        verify(target, cmdLine,
               "[0]=world\n[1]=for now\n",
               "<cmdline><noname>world</noname><noname><![CDATA[for now]]></noname></cmdline>",
               " 'world' 'for now'");
    }

    @Test
    public void namespaceWhitespace9() {
        Object target = new Master21();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args(" [0]=world")
                            .args("[1]=for now")
                            .build();
        verify(target, cmdLine,
               "[0]=world\n[1]=for now\n",
               "<cmdline><noname>world</noname><noname><![CDATA[for now]]></noname></cmdline>",
               " 'world' 'for now'");
    }

    @Test
    public void testPositionalNotMultiple() {
        Object target = new Master28();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("=world")
                            .build();
        verify(target, cmdLine,
               "=world\n",
               "<cmdline><noname>world</noname></cmdline>",
               " 'world'");
    }

    @Test
    public void namespaceWhitespace10() {
        Object target = new Master28();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args(" =world")
                            .build();
        verify(target, cmdLine,
               "=world\n",
               "<cmdline><noname>world</noname></cmdline>",
               " 'world'");
    }

    @Test
    public void testQuoted() {
        Object target = new HelloGoodbye();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("Hello=world")
                            .args("goodbye='for now'")
                            .build();
        verify(target, cmdLine,
               "Hello=world\ngoodbye='for now'\n",
               "<cmdline><Hello>world</Hello><goodbye><![CDATA['for now']]></goodbye></cmdline>",
               null);
        /*
         * This kind of quoting is ridiculous on the command line within a test
         * case.
         */
    }

    @Test
    public void testString() {
        Object target = new HelloGoodbye();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("Hello=world")
                            .args("goodbye=for now")
                            .build();
        verify(target, cmdLine,
               "Hello=world\ngoodbye=for now\n",
               "<cmdline><Hello>world</Hello><goodbye><![CDATA[for now]]></goodbye></cmdline>",
               "--Hello 'world' --goodbye 'for now'");
    }

    @Test
    public void testTopLevelArray1() {
        Object target = new Master39();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("hello[0].world=0")
                            .args("hello[0].moon=1")
                            .build();
        verify(target, cmdLine,
               "hello[0].moon=1\n",
               "<cmdline><hello><moon>1</moon></hello></cmdline>",
               "--hello [--moon 1]");
    }

    @Test
    public void testTopLevelArray2() {
        Object target = new Master39();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("hello[0].world=0")
                            .args("hello[0].moon=1")
                            .args("hello[1].moon=2")
                            .build();
        verify(target, cmdLine,
               "hello[0].moon=1\nhello[1].moon=2\n",
               "<cmdline><hello><moon>1</moon></hello><hello><moon>2</moon></hello></cmdline>",
               "--hello [--moon 1] [--moon 2]");
    }

    @Test
    public void testTwoLevelArray() {
        Object target = new Master41();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("my[0].hello[0].world=0")
                            .args("my[0].hello[0].moon=1")
                            .args("my[1].hello[0].moon=2")
                            .build();
        verify(target, cmdLine,
               "my[0].hello[0].moon=1\nmy[1].hello[0].moon=2\n",
               "<cmdline><my><hello><moon>1</moon></hello></my><my><hello><moon>2</moon></hello></my></cmdline>",
               "--my [--hello [--moon 1]] [--hello [--moon 2]]");
    }

    @Test
    public void testTwoNamespaces() {
        Object target = new Master42();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("hello.world=1")
                            .args("a.b.c.d=alpha")
                            .args("hello.moon=2")
                            .build();
        verify(target, cmdLine,
               "a.b.c.d[0]=alpha\nhello.moon=2\nhello.world=1\n",
               "<cmdline><a><b><c><d>alpha</d></c></b></a><hello><moon>2</moon><world>1</world></hello></cmdline>",
               "-a[-b[-c[-d'alpha']]] --hello [--moon 2 --world 1]");
    }

    @Test
    public void testTwoTwoLevelArray() {
        Object target = new Master41();

        CmdLine cmdLine = CmdLine.builder()
                            .parserType(ParserType.NAME_SPACE)
                            .target(target)
                            .args("my[0].hello[0].world=0")
                            .args("my[0].hello[0].moon=1")
                            .args("my[0].hello[1].moon=3")
                            .args("my[1].hello[0].moon=2")
                            .build();
        verify(target, cmdLine,
               "my[0].hello[0].moon=1\nmy[0].hello[1].moon=3\nmy[1].hello[0].moon=2\n",
               "<cmdline><my><hello><moon>1</moon></hello><hello><moon>3</moon></hello></my><my><hello><moon>2</moon></hello></my></cmdline>",
               "--my [--hello [--moon 1] [--moon 3]] [--hello [--moon 2]]");
    }
}
