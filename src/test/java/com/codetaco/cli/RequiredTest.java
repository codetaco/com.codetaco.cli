package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RequiredTest {

    @Arg(required = true)
    private int maxUpdates = 0;

    @Test
    public void requireInteger() throws Exception {
        try {

            CmdLine.builder()
              .target(this)
              .args("")
              .build();

            Assertions.fail("should have failed");
        } catch (Exception e) {
            Assertions.assertEquals("missing required parameters: --maxUpdates ",
                                    e.getMessage());
        }
    }
}
