package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExceptionTest {
    static public class BadCamelCaps {
        @Arg
        boolean thickness;
    }

    @Test
    public void badCharCommand() {
        try {

            CmdLine.builder()
              .target(this)
              .args("-v")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception e) {
            Assertions.assertEquals("unexpected input: -v ", e.getMessage());
        }
    }

    @Test
    public void badWordCommand() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--version")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception e) {
            Assertions.assertEquals("unexpected input: --version ", e.getMessage());
        }
    }

    @Test
    public void missingRightBracket() {
        try {

            CmdLine.builder()
              .target(this)
              .args("-g(-a)(-b")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception e) {
            Assertions.assertEquals("Unmatched bracket", e.getMessage());
        }
    }

    @Test
    public void tooManyRightBracket() {
        try {

            CmdLine.builder()
              .target(this)
              .args("-g(-a))(-b")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception e) {
            Assertions.assertEquals("unexpected input: -g ( -a ) ) ( -b ", e.getMessage());
        }
    }
}
