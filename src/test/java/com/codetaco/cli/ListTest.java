package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ListTest {
    @Arg(shortName = 's', longName = "s", inList = {"abc", "abcd1", "abcde2"})
    @Arg(shortName = 't', longName = "t", inList = {"abc", "abcd1", "aBCde2"})
    @Arg(shortName = 'u', longName = "u", inList = {"abc", "abcd1", "aBCde2"}, caseSensitive = true)
    @Arg(shortName = 'v', longName = "v", inList = {"abc", "abc1", "abc2"})
    List<String> list;

    @Test
    public void ambiguousCase0() {
        try {

            CmdLine.builder()
              .target(this)
              .args("-s abcd")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception e) {
            Assertions.assertEquals("abcd is not valid for --s(-s)", e.getMessage());
        }
    }

    @Test
    public void ambiguousCase1() {

        CmdLine.builder()
          .target(this)
          .args("-t Abcde2")
          .build();

        Assertions.assertEquals("abcde2", list.get(0));
    }

    @Test
    public void ambiguousCase2() {

        CmdLine.builder()
          .target(this)
          .args("-u Abcde")
          .build();

        Assertions.assertEquals("aBCde2", list.get(0));
    }

    @Test
    public void ambiguousCase3() {
        try {

            CmdLine.builder()
              .target(this)
              .args("-u aB")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception e) {
            Assertions.assertEquals("aB is not valid for --u(-u)", e.getMessage());
        }
    }

    @Test
    public void exact() {

        CmdLine.builder()
          .target(this)
          .args("-v abc")
          .build();

        Assertions.assertEquals("abc", list.get(0));
    }
}
