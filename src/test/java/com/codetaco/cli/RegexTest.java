package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTest {

    @Arg
    Pattern regex;

    @Arg(longName = "regexB", caseSensitive = true)
    @Arg
    public Pattern[] regexA;

    public RegexTest() {

    }

    @Test
    public void invalidRegex() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--regex'[+'")
              .build();

            Assertions.fail("should have been an Exception");
        } catch (Exception e) {
            Assertions.assertEquals("Unclosed character class", e.getMessage().substring(0, 24));
        }
    }

    @Test
    public void validRegex() {

        CmdLine.builder()
          .target(this)
          .args("--regex '[0-9]+'")
          .build();

        Assertions.assertFalse(regex.matcher("grabNoNumbers").find());
        Matcher matcher = regex.matcher("grab54numbers61Only");
        Assertions.assertTrue(matcher.find());
        Assertions.assertEquals("54", matcher.group(0));
    }

    @Test
    public void validRegexArrayCaseDoesNotMatter() {

        CmdLine.builder()
          .target(this)
          .args("--regexa '[0-9]+' '[a-zA-Z]+'")
          .build();

        Assertions.assertFalse(regexA[1].matcher("123").find());
        Matcher matcher = regexA[1].matcher("54Alpha61Only");
        Assertions.assertTrue(matcher.find());
        Assertions.assertEquals("Alpha", matcher.group(0));
    }

    @Test
    public void validRegexArrayCaseMatters() {

        CmdLine.builder()
          .target(this)
          .args("--regexb '[0-9]+''[a-z]+'")
          .build();

        Assertions.assertFalse(regexA[1].matcher("123").find());
        Matcher matcher = regexA[1].matcher("54Alpha61Only");
        Assertions.assertTrue(matcher.find());
        Assertions.assertEquals("lpha", matcher.group(0));
    }

    @Test
    public void validRegexArrayCaseMattersA() {

        CmdLine.builder()
          .target(this)
          .args("--regexb '[0-9]+''[a-zA-Z]+'")
          .build();

        Assertions.assertFalse(regexA[1].matcher("123").find());
        Matcher matcher = regexA[1].matcher("54Alpha61Only");
        Assertions.assertTrue(matcher.find());
        Assertions.assertEquals("Alpha", matcher.group(0));
    }
}
