package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EnumTest {
    static public enum SplineBoxTest {
        edgetobottom,
        EdgeToCenter,
        EdgeToTop,
        Edge
    }

    static public enum TestEnum {
        KEY1,
        KEY2,
        KEY3
    }

    static public enum TestEnumForCamelCapsA {
        AllowingCamelCaps,
        NotAllowingCamelCaps
    }

    static public enum TestEnumForCamelCapsB {
        AllowingCamelCaps,
        AllowingCamelCaps2
    }

    @Arg(shortName = 'r')
    private TestEnum[] anArrayOfEnums;

    @Arg(longName = "as", inEnum = "com.codetaco.cli.EnumTest$TestEnum", caseSensitive = true)
    private String[] enumNames;

    @Arg(shortName = 'i', caseSensitive = true)
    public TestEnum enum1;

    @Arg(shortName = 'A', caseSensitive = true)
    public TestEnumForCamelCapsA enumA;

    @Arg(shortName = 'B', caseSensitive = true)
    public TestEnumForCamelCapsB enumB;

    @Arg(shortName = 'S', caseSensitive = true)
    public SplineBoxTest sbox;

    @Test
    public void arrays() {
        try {

            CmdLine.builder()
              .target(this)
              .args("-r key1 key2")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals("enum[] is not support at this time. Use String[] and @Arg(inEnum=\"fully qualified enum class name\")"
              , e.getMessage());
        }
    }

    @Test
    public void listOfStrings() {

        CmdLine.builder()
          .target(this)
          .args("--as key1 key2")
          .build();

        Assertions.assertEquals("KEY1", enumNames[0]);
        Assertions.assertEquals("KEY2", enumNames[1]);
    }

    @Test
    public void camelCapConvertsToNull() {

        CmdLine.builder()
          .target(this)
          .args("-S ETT")
          .build();

        Assertions.assertEquals(SplineBoxTest.EdgeToTop, sbox);
    }

    @Test
    public void minUniqueness() {

        CmdLine.builder()
          .target(this)
          .args("-A n")
          .build();

        Assertions.assertEquals(TestEnumForCamelCapsA.NotAllowingCamelCaps, enumA);
    }

    @Test
    public void camelCaps() {

        CmdLine.builder()
          .target(this)
          .args("-A ACC")
          .build();

        Assertions.assertEquals(TestEnumForCamelCapsA.AllowingCamelCaps, enumA);
    }

    @Test
    public void camelCapsWithNumber() {

        CmdLine.builder()
          .target(this)
          .args("-B aCC2")
          .build();

        Assertions.assertEquals(TestEnumForCamelCapsB.AllowingCamelCaps2, enumB);
    }

    @Test
    public void caseSensitivity() {

        CmdLine.builder()
          .target(this)
          .args("-i Key2")
          .build();

        Assertions.assertEquals(TestEnum.KEY2, enum1);
    }

    @Test
    public void enumListValidation() {

        CmdLine.builder()
          .target(this)
          .args("-i KeY2")
          .build();

        Assertions.assertEquals(TestEnum.KEY2, enum1);
    }

    @Test
    public void invalidEnum() {
        try {

            CmdLine.builder()
              .target(this)
              .args("-i KEYX")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals(
              "\"KEYX\" is not a valid enum constant for variable \"enum1\" (KEY1, KEY2, KEY3)",
              e.getMessage());
        }
    }

    @Test
    public void tooManyCamelCaps() {
        try {

            CmdLine.builder()
              .target(this)
              .args("-B ACC")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals(
              "\"ACC\" is not a unique enum constant for variable \"enumB\" (AllowingCamelCaps, AllowingCamelCaps2)",
              e.getMessage());
        }
    }

    @Test
    public void min2() {
        try {

            CmdLine.builder()
              .target(this)
              .args("-B AC2")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals(
              "\"ACC\" is not a unique enum constant for variable \"enumB\" (AllowingCamelCaps, AllowingCamelCaps2)",
              e.getMessage());
        }
    }

    @Test
    public void validEnum() {

        CmdLine.builder()
          .target(this)
          .args("-i KEY2")
          .build();

        Assertions.assertEquals(TestEnum.KEY2, enum1);
    }

    @Test
    public void exactMatch() {

        CmdLine.builder()
          .target(this)
          .args("-S Edge")
          .build();

        Assertions.assertEquals(SplineBoxTest.Edge, sbox);
    }

    @Test
    public void partialMatch() {

        CmdLine.builder()
          .target(this)
          .args("-S EdgeToTop")
          .build();

        Assertions.assertEquals(SplineBoxTest.EdgeToTop, sbox);
    }
}
