package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class IncludeTest {
    private static final File workDir = new File(System.getProperty("java.io.tmpdir"));

    static private File createSpecFile(String specs) {
        try {
            File file = File.createTempFile("IncludeTest.", ".fun", workDir);
            try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {
                out.write(specs);
                out.newLine();
            }
            return file;
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
            return null;
        }
    }

    @Arg(shortName = 'a')
    private boolean boola;

    @Arg(shortName = 'b')
    private boolean boolb;

    @Arg(caseSensitive = true)
    private String workDirectory;

    @Test
    public void includeExactPath() {
        File spec = createSpecFile("--workDirectory /temp");
        try {

            CmdLine.builder()
              .target(this)
              .args("-a @'" + spec.getAbsolutePath().replaceAll("\\\\", "\\\\\\\\") + "' -b")
              .build();

            Assertions.assertTrue(boola);
            Assertions.assertTrue(boolb);
            Assertions.assertEquals("/temp", workDirectory);

        } finally {
            Assertions.assertTrue(spec.delete());
        }
    }

    @Test
    public void includeNotFoundNotQuoted() {
        try {

            CmdLine.builder()
              .target(this)
              .args("@BadFileName.fun")
              .build();

            Assertions.fail("expected a file not found message");
        } catch (Exception e) {
            Assertions.assertEquals("@BadFileName.fun could not be found", e.getMessage());
        }
    }

    @Test
    public void includeNotFoundQuoted() {
        try {

            CmdLine.builder()
              .target(this)
              .args("@'BadFileName.fun' -b")
              .build();

            Assertions.fail("expected a file not found message");
        } catch (Exception e) {
            Assertions.assertEquals("@BadFileName.fun could not be found", e.getMessage());
        }
    }

    @Test
    public void includeNotFoundWithSpace() {
        try {

            CmdLine.builder()
              .target(this)
              .args("-a @ BadFileName.fun -b")
              .build();

            Assertions.fail("expected a file not found message");
        } catch (Exception e) {
            Assertions.assertEquals("@BadFileName.fun could not be found", e.getMessage());
        }
    }

    @Test
    public void includeUsingDefaultPath() {
        File spec = createSpecFile("--workDirectory /temp");
        try {

            CmdLine.builder()
              .target(this)
              .includeDirectory(spec.getParentFile())
              .args("-a @" + spec.getName() + " -b")
              .build();

            Assertions.assertTrue(boola);
            Assertions.assertTrue(boolb);
            Assertions.assertEquals("/temp", workDirectory);

        } finally {
            Assertions.assertTrue(spec.delete());
        }
    }
}
