package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExcludeArgsTest {

    static public class Name {
        @Arg(longName = "shortName", help = "this is a short name")
        String shortName;
        @Arg(longName = "longName", help = "this is a long name")
        String longName;
    }

    @Arg(
      longName = "name1",
      help = "name1")
    private Name name1;
    @Arg(
      longName = "name2",
      excludeArgs = "shortName",
      help = "name2")
    private Name name2;
    @Arg(
      longName = "name3",
      excludeArgs = "longName",
      help = "name3")
    private Name name3;

    @Test
    public void fullUse() {

        CmdLine.builder()
          .target(this)
          .args("--name1(--shortName shortname --longName longname)")
          .build();

    }

    @Test
    public void errorShortName() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--name2(--shortName shortname --longName longname)")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals("unexpected input: --shortName shortname ", e.getMessage());
        }
    }

    @Test
    public void errorLongName() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--name3(--shortName shortname --longName longname)")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals("unexpected input: --longName longname ", e.getMessage());
        }
    }
}
