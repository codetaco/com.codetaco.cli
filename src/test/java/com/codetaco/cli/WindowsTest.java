package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class WindowsTest {
    @Arg(shortName = 'a')
    boolean aBool;

    @Arg(shortName = 'b')
    boolean bBool;

    @Test
    public void compact() {

        CmdLine.builder()
          .forWindows()
          .target(this)
          .args("/ab")
          .build();

        Assertions.assertTrue(aBool);
        Assertions.assertTrue(bBool);
    }

    @Test
    public void negate() {

        CmdLine.builder()
          .forWindows()
          .target(this)
          .args("/ab /-b")
          .build();

        Assertions.assertTrue(aBool);
        Assertions.assertFalse(bBool);
    }

    @Test
    public void simple() {

        CmdLine.builder()
          .forWindows()
          .target(this)
          .args("/a /b")
          .build();

        Assertions.assertTrue(aBool);
        Assertions.assertTrue(bBool);
    }
}
