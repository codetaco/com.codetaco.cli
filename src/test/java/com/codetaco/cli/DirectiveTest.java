package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Month;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;

public class DirectiveTest {
    @Arg
    private ZonedDateTime dateVar;

    @Arg
    private long longVar;

    @Arg
    private int intVar;

    @Arg
    private float floatVar;

    @Arg
    private String strVar;

    @Test
    public void asDate() {

        CmdLine.builder()
          .target(this)
          .args("--dateVar _(date(todate('04-09-2008'), '=3day'))")
          .build();

        assertValidReturnDate(2008, Month.APRIL.getValue(), 3, 0, 0, 0, 0);
    }

    @Test
    public void asFloat() {

        CmdLine.builder()
          .target(this)
          .args("--floatVar _( 200.02 * 12)")
          .build();

        Assertions.assertEquals(2400.24F, floatVar);
    }

    @Test
    public void asInt() {

        CmdLine.builder()
          .target(this)
          .args("--intVar _( 200 * 12)")
          .build();

        Assertions.assertEquals(2400, intVar);
    }

    @Test
    public void asLong() {

        CmdLine.builder()
          .target(this)
          .args("--longVar _( 200 * 12)")
          .build();

        Assertions.assertEquals(2400, longVar);
    }

    private void assertValidReturnDate(int year,
                                       int month,
                                       int date,
                                       int hour,
                                       int minute,
                                       int second,
                                       int millisecond) {
        Assertions.assertEquals(year, dateVar.get(ChronoField.YEAR));
        Assertions.assertEquals(month, dateVar.get(ChronoField.MONTH_OF_YEAR));
        Assertions.assertEquals(date, dateVar.get(ChronoField.DAY_OF_MONTH));
        Assertions.assertEquals(hour, dateVar.get(ChronoField.HOUR_OF_DAY));
        Assertions.assertEquals(minute, dateVar.get(ChronoField.MINUTE_OF_HOUR));
        Assertions.assertEquals(second, dateVar.get(ChronoField.SECOND_OF_MINUTE));
        Assertions.assertEquals(millisecond, dateVar.get(ChronoField.MILLI_OF_SECOND));
    }

    @Test
    public void asString() {

        CmdLine.builder()
          .target(this)
          .args("--strVar _(cat('ab','yz'))")
          .build();

        Assertions.assertEquals("abyz", strVar);
    }

    @Test
    public void equalSignAsASpecialEquDirective() {

        CmdLine.builder()
          .target(this)
          .args("--dateVar =(date(todate('04-09-2008'), '=3day'))")
          .build();

        assertValidReturnDate(2008, Month.APRIL.getValue(), 3,
                              0, 0, 0, 0);
    }
}
