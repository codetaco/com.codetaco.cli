package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.cli.impl.type.AbstractCLA;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DashingTest {
    static public class CamelCaps {
        @Arg
        boolean camelCaps;
    }

    static public class CamelCapsPartial {
        @Arg
        boolean camelCaps;

        @Arg
        String ccx;
    }

    static public class EmbeddedDash {
        @Arg(shortName = 'a')
        boolean noDash;

        @Arg(shortName = 'n')
        boolean name;

        @Arg(shortName = 'b', longName = "bee-name")
        boolean withDoubleDash;
    }

    static public class MPhoneIssues {
        @Arg(shortName = 'a', allowMetaphone = true)
        boolean inputFileName;

        @Arg(shortName = 'a', allowMetaphone = true)
        boolean inputFileNum;
    }

    static public class MPhoneVersion {
        @Arg(allowMetaphone = true)
        boolean version;
    }

    @Test
    public void createCamelCapVersionOfKeyword() {
        Assertions.assertEquals("^[wW]$",
                                AbstractCLA.createCamelCapVersionOfKeyword("w").pattern());
        Assertions.assertEquals("^[wW]$",
                                AbstractCLA.createCamelCapVersionOfKeyword("W").pattern());
        Assertions.assertEquals("^[wW]h?a?t?$",
                                AbstractCLA.createCamelCapVersionOfKeyword("what").pattern());
        Assertions.assertEquals("^[wW]h?a?t?$",
                                AbstractCLA.createCamelCapVersionOfKeyword("What").pattern());
        Assertions.assertEquals("^[wW](H)?(A)?(T)?$",
                                AbstractCLA.createCamelCapVersionOfKeyword("WHAT").pattern());
        Assertions.assertEquals("^[wW]h?a?t?(Is?)?(Th?i?s?)?$",
                                AbstractCLA.createCamelCapVersionOfKeyword("whatIsThis").pattern());
    }

    @Test
    public void camelCaps() {
        CamelCaps target = new CamelCaps();

        CmdLine.builder()
          .target(target)
          .args("--CC")
          .build();

        Assertions.assertTrue(target.camelCaps);
    }

    @Test
    public void camelCapsCompetesWithPartialKeyword() {
        CamelCapsPartial target = new CamelCapsPartial();

        CmdLine.builder()
          .target(target)
          .args("--cC")
          .build();

        Assertions.assertTrue(target.camelCaps);
    }

    @Test
    public void embeddedDash1() {
        EmbeddedDash target = new EmbeddedDash();

        CmdLine.builder()
          .target(target)
          .args("-a-b")
          .build();

        Assertions.assertTrue(target.noDash);
        Assertions.assertTrue(target.withDoubleDash);
    }

    @Test
    public void embeddedDash2() {
        EmbeddedDash target = new EmbeddedDash();

        CmdLine.builder()
          .target(target)
          .args("-a --bee-name")
          .build();

        Assertions.assertTrue(target.noDash);
        Assertions.assertTrue(target.withDoubleDash);
    }

    @Test
    public void embeddedDash3() {
        EmbeddedDash target = new EmbeddedDash();

        CmdLine.builder()
          .target(target)
          .args("--bee-name -a")
          .build();

        Assertions.assertTrue(target.noDash);
        Assertions.assertTrue(target.withDoubleDash);
    }

    @Test
    public void embeddedDash4() {
        EmbeddedDash target = new EmbeddedDash();

        CmdLine.builder()
          .target(target)
          .args("-a--bee-name")
          .build();

        Assertions.assertTrue(target.noDash);
        Assertions.assertTrue(target.withDoubleDash);
    }

    @Test
    public void embeddedDash5() {
        try {
            EmbeddedDash target = new EmbeddedDash();

            CmdLine.builder()
              .target(target)
              .args("--bee-name-a")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception e) {
            Assertions.assertEquals("unexpected input: --bee-name-a ", e.getMessage());
        }
    }

    @Test
    public void embeddedDash6() {
        EmbeddedDash target = new EmbeddedDash();

        CmdLine.builder()
          .target(target)
          .args("--bee-name--noDash")
          .build();

        Assertions.assertTrue(target.noDash);
        Assertions.assertTrue(target.withDoubleDash);
    }

    @Test
    public void embeddedDash7() {
        EmbeddedDash target = new EmbeddedDash();

        CmdLine.builder()
          .target(target)
          .args("-a --b -n")
          .build();

        Assertions.assertTrue(target.noDash);
        Assertions.assertTrue(target.name);
        Assertions.assertTrue(target.withDoubleDash);
    }

    @Test
    public void embeddedDash8() {
        EmbeddedDash target = new EmbeddedDash();

        CmdLine.builder()
          .target(target)
          .args("-a --bee-n -n")
          .build();

        Assertions.assertTrue(target.noDash);
        Assertions.assertTrue(target.name);
        Assertions.assertTrue(target.withDoubleDash);
    }

    @Test
    public void embeddedDash9() {
        EmbeddedDash target = new EmbeddedDash();

        CmdLine.builder()
          .target(target)
          .args("-a --bee-n")
          .build();

        Assertions.assertTrue(target.noDash);
        Assertions.assertFalse(target.name);
        Assertions.assertTrue(target.withDoubleDash);
    }

    @Test
    public void metaphoneIssues() {
        MPhoneIssues target = new MPhoneIssues();

        CmdLine.Builder builder = CmdLine.builder();
        try {
            builder
              .target(target)
              .args("--help")
              .build();
            Assertions.fail("expected an exception");
        } catch (Exception e) {
            Assertions.assertEquals("multiple parse exceptions", e.getMessage());
            Assertions.assertEquals(2, builder.getParseExceptions().size());
            Assertions.assertEquals(
              "duplicate short name, found \"--inputFileName(-a)\"' and \"--inputFileNum(-a)\"",
              builder.getParseExceptions().get(0).getMessage());
            Assertions.assertEquals(
              "duplicate values for metaphone, found \"--inputFileName(-a)\"' and \"--inputFileNum(-a)\"",
              builder.getParseExceptions().get(1).getMessage());
        }
    }

    @Test
    public void metaphoneVersion() {
        MPhoneVersion target = new MPhoneVersion();

        CmdLine.builder()
          .target(target)
          .args("--vrshn")
          .build();

        Assertions.assertTrue(target.version);
    }
}
