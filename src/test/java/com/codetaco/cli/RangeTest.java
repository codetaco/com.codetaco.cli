package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RangeTest {
    @Arg(range = {"0"})
    float[] f1;

    @Arg(range = {"0", "1000"})
    int[] i1;

    @Arg(range = {"b", "famish"})
    String[] string;

    @Test
    public void floatRange1() {

        CmdLine.builder()
          .target(this)
          .args("--f1 0 1 9 0.1 999.999999")
          .build();
    }

    @Test
    public void floatRange2() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--f1 0 '-1' 999")
              .build();

            Assertions.fail("should have Assertions.failed");
        } catch (Exception e) {
            Assertions.assertEquals("-1.0 is not valid for --f1", e.getMessage());
        }
    }

    @Test
    public void floatRange3() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--f1 0 '-0.00001' 999.0")
              .build();

            Assertions.fail("should have Assertions.failed");
        } catch (Exception e) {
            Assertions.assertEquals("-1.0E-5 is not valid for --f1", e.getMessage());
        }

    }

    @Test
    public void integerRange1() {

        CmdLine.builder()
          .target(this)
          .args("--i1 0 1 9")
          .build();

    }

    @Test
    public void integerRange2() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--i1 0 '-1' 999")
              .build();

            Assertions.fail("should have Assertions.failed");
        } catch (Exception e) {
            Assertions.assertEquals("-1 is not valid for --i1", e.getMessage());
        }

    }

    @Test
    public void integerRange3() {

        CmdLine.builder()
          .target(this)
          .args("--i1 0 1 999")
          .build();
    }

    @Test
    public void integerRange4() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--i1 0 '-1' 999")
              .build();

            Assertions.fail("should have Assertions.failed");
        } catch (Exception e) {
            Assertions.assertEquals("-1 is not valid for --i1", e.getMessage());
        }
    }

    @Test
    public void integerRange5() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--i1 0 1 1000 1001")
              .build();

            Assertions.fail("should have Assertions.failed");
        } catch (Exception e) {
            Assertions.assertEquals("1001 is not valid for --i1", e.getMessage());
        }

    }

    @Test
    public void stringRange1() {

        CmdLine.builder()
          .target(this)
          .args("--string b c")
          .build();

    }

    @Test
    public void stringRange2() {

        CmdLine.builder()
          .target(this)
          .args("--string binary c")
          .build();

    }

    @Test
    public void stringRange3() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--string  b c android")
              .build();

            Assertions.fail("should have Assertions.failed");
        } catch (Exception e) {
            Assertions.assertEquals("android is not valid for --string", e.getMessage());
        }
    }

    @Test
    public void stringRange4() {

        CmdLine.builder()
          .target(this)
          .args("--string  b c")
          .build();

    }

    @Test
    public void stringRange5() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--string g b c")
              .build();

            Assertions.fail("should have Assertions.failed");
        } catch (Exception e) {
            Assertions.assertEquals("g is not valid for --string", e.getMessage());
        }
    }

    @Test
    public void stringRange6() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--string a b c")
              .build();

            Assertions.fail("should have Assertions.failed");
        } catch (Exception e) {
            Assertions.assertEquals("a is not valid for --string", e.getMessage());
        }
    }

    @Test
    public void stringRange7() {

        CmdLine.builder()
          .target(this)
          .args("--string famish")
          .build();
    }

    @Test
    public void stringRange8() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--string famished")
              .build();

            Assertions.fail("should have Assertions.failed");
        } catch (Exception e) {
            Assertions.assertEquals("famished is not valid for --string", e.getMessage());
        }
    }
}
