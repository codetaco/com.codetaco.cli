package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BooleanTest {
    static public final class ResetMoreThanOne {
        @Arg(shortName = 't')
        boolean tx;
        @Arg(shortName = 's', defaultValues = "true")
        boolean sx;
    }

    static public final class ResetOnlyOneOfTwo {
        @Arg(shortName = 'a')
        boolean ax;
        @Arg(shortName = 's')
        boolean sx;
    }

    static public final class SetToDefaultChar {
        @Arg(shortName = 'a')
        boolean ax;
    }

    static public final class SimpleSetFalse {
        @Arg(defaultValues = "true")
        boolean ax;
    }

    static public final class SimpleSetTrue {
        @Arg
        boolean ax;
    }

    public BooleanTest() {
    }

    @Test
    public void resetMoreThanOne() {
        ResetMoreThanOne resetMoreThanOne = new ResetMoreThanOne();

        CmdLine.builder()
          .target(resetMoreThanOne)
          .args("-ts -!t,s")
          .build();

        Assertions.assertTrue(resetMoreThanOne.sx);
        Assertions.assertFalse(resetMoreThanOne.tx);
    }

    @Test
    public void resetOnlyOneOfTwo() {
        ResetOnlyOneOfTwo resetOnlyOneOfTwo = new ResetOnlyOneOfTwo();

        CmdLine.builder()
          .target(resetOnlyOneOfTwo)
          .args("-as -!a")
          .build();

        Assertions.assertTrue(resetOnlyOneOfTwo.sx);
        Assertions.assertFalse(resetOnlyOneOfTwo.ax);
    }

    @Test
    public void setToDefaultChar() {
        SetToDefaultChar setToDefaultChar = new SetToDefaultChar();

        CmdLine.builder()
          .target(setToDefaultChar)
          .args("-a -!a")
          .build();

        Assertions.assertFalse(setToDefaultChar.ax);
    }

    @Test
    public void setToDefaultWord() {
        SetToDefaultChar setToDefaultChar = new SetToDefaultChar();

        CmdLine.builder()
          .target(setToDefaultChar)
          .args("--ax -!ax")
          .build();

        Assertions.assertFalse(setToDefaultChar.ax);
    }

    @Test
    public void simpleSetFalse() {
        SimpleSetFalse simpleSetFalse = new SimpleSetFalse();

        CmdLine.builder()
          .target(simpleSetFalse)
          .args("--ax")
          .build();

        Assertions.assertFalse(simpleSetFalse.ax);
    }

    @Test
    public void simpleSetTrue() {
        SimpleSetTrue simpleSetTrue = new SimpleSetTrue();

        CmdLine.builder()
          .target(simpleSetTrue)
          .args("--ax")
          .build();

        Assertions.assertTrue(simpleSetTrue.ax);
    }

}
