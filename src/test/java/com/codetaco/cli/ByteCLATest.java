package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ByteCLATest {
    static public class ByteCfg {
        @Arg
        byte byteValue;
        @Arg
        Byte byteValueObject;
        @Arg
        Byte[] byteArrayObject;
        @Arg
        byte[] byteArray;
    }

    public ByteCLATest() {
    }

    @Test
    public void byteArrayObject() {
        ByteCfg target = new ByteCfg();

        CmdLine.builder()
          .target(target)
          .args("--byteArrayObject 00 01 0 1")
          .build();

        Assertions.assertEquals(4, target.byteArrayObject.length);
        Assertions.assertEquals((byte) 0, target.byteArrayObject[0].byteValue());
        Assertions.assertEquals((byte) 1, target.byteArrayObject[1].byteValue());
        Assertions.assertEquals((byte) '0', target.byteArrayObject[2].byteValue());
        Assertions.assertEquals((byte) '1', target.byteArrayObject[3].byteValue());
    }

    @Test
    public void byteArrayPrimitive() {
        ByteCfg target = new ByteCfg();

        CmdLine.builder()
          .target(target)
          .args("--byteArray 00 01 0 1 --byteArray 255")
          .build();

        Assertions.assertEquals(5, target.byteArray.length);
        Assertions.assertEquals((byte) 0, target.byteArray[0]);
        Assertions.assertEquals((byte) 1, target.byteArray[1]);
        Assertions.assertEquals((byte) '0', target.byteArray[2]);
        Assertions.assertEquals((byte) '1', target.byteArray[3]);
        Assertions.assertEquals((byte) 255, target.byteArray[4]);
    }

    @Test
    public void hexLiteral() {
        ByteCfg target = new ByteCfg();

        CmdLine.builder()
          .target(target)
          .args("--byteArray null, SOH, cr lf")
          .build();

        Assertions.assertEquals(4, target.byteArray.length);
        Assertions.assertEquals((byte) 0, target.byteArray[0]);
        Assertions.assertEquals((byte) 1, target.byteArray[1]);
        Assertions.assertEquals((byte) 13, target.byteArray[2]);
        Assertions.assertEquals((byte) 10, target.byteArray[3]);
    }

    @Test
    public void singleByteAlpha() {
        ByteCfg target = new ByteCfg();

        CmdLine.builder()
          .target(target)
          .args("--byteValue a")
          .build();

        Assertions.assertEquals((byte) 97, target.byteValue);
    }

    @Test
    public void singleByteBadData() {
        try {
            ByteCfg target = new ByteCfg();

            CmdLine.builder()
              .target(target)
              .args("--byteValue abc")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception e) {
            Assertions.assertEquals(
              "abc is not a valid byte code.  Try one of these codes or the corresponding number: NULL(0) SOH(1) STX(2) ETX(3) EOT(4) ENQ(5) ACK(6) BEL(7) BS(8) HT(9) LF(10) VT(11) FF(12) CR(13) SO(14) SI(15) DLE(16) DC1(17) DC2(18) DC3(19) DC4(20) NAK(21) SYN(22) ETB(23) CAN(24) EM(25) SUB(26) ESC(27) FS(28) GS(29) RS(30) US(31) SP(32) ",
              e.getMessage());
        }
    }

    @Test
    public void singleByteNegative() {
        ByteCfg target = new ByteCfg();

        CmdLine.builder()
          .target(target)
          .args("--byteValue -52")
          .build();

        Assertions.assertEquals((byte) -52, target.byteValue);
    }

    @Test
    public void singleByteNumericMax() {
        ByteCfg target = new ByteCfg();

        CmdLine.builder()
          .target(target)
          .args("--byteValue 255")
          .build();

        Assertions.assertEquals((byte) 255, target.byteValue);
    }

    @Test
    public void singleByteNumericMin() {
        ByteCfg target = new ByteCfg();

        CmdLine.builder()
          .target(target)
          .args("--byteValue 00")
          .build();

        Assertions.assertEquals((byte) 0, target.byteValue);
    }

    @Test
    public void singleByteObject() {
        ByteCfg target = new ByteCfg();

        CmdLine.builder()
          .target(target)
          .args("--byteValueObject a")
          .build();

        Assertions.assertEquals((byte) 97, target.byteValueObject.byteValue());
    }

    @Test
    public void singleByteSingleDigit() {
        ByteCfg target = new ByteCfg();

        CmdLine.builder()
          .target(target)
          .args("--byteValue 01")
          .build();

        Assertions.assertEquals((byte) 1, target.byteValue);
    }

    @Test
    public void singleByteSingleDigitAlpha() {
        ByteCfg target = new ByteCfg();

        CmdLine.builder()
          .target(target)
          .args("--byteValue 1")
          .build();

        Assertions.assertEquals((byte) '1', target.byteValue);
    }

    @Test
    public void singleByteTooBig() {
        try {
            ByteCfg target = new ByteCfg();

            CmdLine.builder()
              .target(target)
              .args("--byteValue 256")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception e) {
            Assertions.assertEquals("256 is too large, max = 255", e.getMessage());
        }

    }
}
