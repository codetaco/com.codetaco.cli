package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PositionalTest {

    static public class Master01 {
        @Arg(shortName = 'a', positional = true)
        boolean item1;

        @Arg(shortName = 'd')
        Master02 master02;
    }

    static public class Master02 {
        @Arg(shortName = 'b', positional = true)
        boolean item2;
    }

    static public class Master03 {
        @Arg(shortName = 'a')
        boolean item1;

        @Arg(shortName = 'd', positional = true)
        Master04 master04;
    }

    static public class Master04 {
        @Arg(shortName = 'b')
        boolean item2;
    }

    static public class Master05 {
        @Arg(shortName = 'a', positional = true)
        boolean item1;

        @Arg(shortName = 'd', positional = true)
        Master06 master06;
    }

    static public class Master06 {
        @Arg(shortName = 'b', positional = true)
        boolean item2;
    }

    static public class Master07 {
        @Arg(positional = true)
        String[] str;
    }

    static public class Master08 {
        @Arg(positional = true)
        String[] str1;

        @Arg(positional = true)
        String[] str2;
    }

    static public class Master09 {
        @Arg(positional = true)
        String[] str;
    }

    static public class Master10 {
        @Arg(positional = true)
        String str;
    }

    static public class Master11 {
        @Arg(positional = true)
        String str1;

        @Arg(positional = true)
        String str2;
    }

    @Arg(shortName = 'g')
    Master01 master01;

    @Arg(shortName = 'h')
    Master03 master03;

    @Arg(shortName = 'j')
    Master05 master05;

    @Arg(positional = true)
    Master07[] master07;

    @Test
    public void groupingGroupsPositionalBooleans() {

        CmdLine.builder()
          .target(this)
          .args("-g( true -d(true) )")
          .build();

        Assertions.assertTrue(master01.item1);
        Assertions.assertTrue(master01.master02.item2);
    }

    @Test
    public void groupingGroupsPositionalGroups() {

        CmdLine.builder()
          .target(this)
          .args("-h( -a (-b) )")
          .build();

        Assertions.assertTrue(master03.item1);
        Assertions.assertTrue(master03.master04.item2);
    }

    @Test
    public void groupingGroupsPositionalGroupsAndBooleans() {

        CmdLine.builder()
          .target(this)
          .args("-j( true (true) )")
          .build();

        Assertions.assertTrue(master05.item1);
        Assertions.assertTrue(master05.master06.item2);
    }

    @Test
    public void groupMultiple() {

        CmdLine.builder()
          .target(this)
          .args("(input1 input3),(input2)")
          .build();

        Assertions.assertEquals("input1", master07[0].str[0]);
        Assertions.assertEquals("input3", master07[0].str[1]);
        Assertions.assertEquals("input2", master07[1].str[0]);
    }

    @Test
    public void multipleValuesWithMultiplePositionals() {
        try {

            CmdLine.builder()
              .target(new Master08())
              .args("(input1 input3),(input2)")
              .build();

            Assertions.fail("expected a parse exception");
        } catch (Exception e) {
            Assertions.assertEquals(
              "a multi-value positional cli must be the only positional cli, found \"--str1\"' and \"--str2\"",
              e.getMessage());
        }
    }

    @Test
    public void stringMultiple() {
        Master09 target = new Master09();

        CmdLine.builder()
          .target(target)
          .args("input1,input2")
          .build();

        Assertions.assertEquals("input1", target.str[0]);
        Assertions.assertEquals("input2", target.str[1]);
    }

    @Test
    public void testString() {
        Master10 target = new Master10();

        CmdLine.builder()
          .target(target)
          .args("input1")
          .build();

        Assertions.assertEquals("input1", target.str);
    }

    @Test
    public void twoStrings() {
        Master11 target = new Master11();

        CmdLine.builder()
          .target(target)
          .args("input1,input2")
          .build();

        Assertions.assertEquals("input1", target.str1);
        Assertions.assertEquals("input2", target.str2);
    }

    @Test
    public void twoStringsOnlyUsed1() {
        Master11 target = new Master11();

        CmdLine.builder()
          .target(target)
          .args("input1")
          .build();

        Assertions.assertEquals("input1", target.str1);
        Assertions.assertNull(target.str2);
    }
}
