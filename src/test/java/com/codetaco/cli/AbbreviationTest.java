package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AbbreviationTest {
    @Arg(shortName = 'a', longName = "Item")
    private String item;

    @Arg(shortName = 'b', longName = "Items")
    private String items;

    @Arg
    private String formatIn;
    @Arg
    private String formatOut;


    @Test
    public void camelCaseUpper() {
        CmdLine.builder()
          .target(this)
          .parserType(ParserType.COMMAND_LINE)
          .args("--FO camelCaseUpper")
          .build();
        Assertions.assertEquals("camelcaseupper", formatOut);
    }

    @Test
    public void lowercaseOnlyMatch() {
        CmdLine.builder()
          .target(this)
          .parserType(ParserType.COMMAND_LINE)
          .args("--formatout lowercaseOnlyMatch")
          .build();
        Assertions.assertEquals("lowercaseonlymatch", formatOut);
    }

    @Test
    public void camelCaseLower() {
        CmdLine.builder()
          .target(this)
          .parserType(ParserType.COMMAND_LINE)
          .args("--formatout camelCaseLower")
          .build();
        Assertions.assertEquals("camelcaselower", formatOut);
    }

    @Test
    public void exactMatchOnShorterOtherwiseAmbiguous1() {
        CmdLine.builder()
          .target(this)
          .parserType(ParserType.COMMAND_LINE)
          .args("--items longer")
          .build();
        Assertions.assertEquals("longer", items);
    }

    @Test
    public void exactMatchOnShorterOtherwiseAmbiguous2() {
        CmdLine.builder()
          .target(this)
          .args("--item shorter")
          .build();
        Assertions.assertEquals("shorter", item);
    }

}
