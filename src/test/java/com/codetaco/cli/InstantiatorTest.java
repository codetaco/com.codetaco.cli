package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class InstantiatorTest {
    static abstract public class AbstractColor {
        static public AbstractColor create(String name) {
            if ("red".equals(name)) {
                return new RedColor();
            }
            if ("blue".equals(name)) {
                return new BlueColor();
            }
            return null;
        }
    }

    static public class BlueColor extends AbstractColor {
    }

    abstract public static class Finder {
        static public Finder find(String key) {
            Finder instance = null;
            if ("what".equalsIgnoreCase(key)) {
                instance = new WhatFinder();
            } else if ("when".equalsIgnoreCase(key)) {
                instance = new WhenFinder();
            } else if ("where".equalsIgnoreCase(key)) {
                instance = new WhereFinder();
            } else {
                instance = new SuitFinder(key);
            }
            instance.value = key;
            return instance;
        }

        @Arg(positional = true, caseSensitive = true)
        public String value;

        final String testValue() {
            return value;
        }
    }

    static public class RedColor extends AbstractColor {
    }

    public enum Suit {
        HEARTS,
        SPADES,
        CLUBS,
        DIAMONDS
    }

    public static class SuitFinder extends Finder {
        public SuitFinder(String key) {
            value = key;
        }
    }

    public static class WhatFinder extends Finder {
    }

    public static class WhenFinder extends Finder {
    }

    public static class WhereFinder extends Finder {
    }

    @Arg(factoryMethod = "find",
      caseSensitive = true,
      factoryArgName = Arg.SELF_REFERENCING_ARGNAME,
      inEnum = "com.codetaco.cli.InstantiatorTest$Suit")
    public List<Finder> suits;

    @Arg(factoryMethod = "create", factoryArgName = Arg.SELF_REFERENCING_ARGNAME, inList = {"red", "blue"})
    public AbstractColor color;

    @Arg(shortName = 'm',
      factoryMethod = "fXnd",
      factoryArgName = Arg.SELF_REFERENCING_ARGNAME)
    @Arg(shortName = 'p',
      longName = "finderP",
      caseSensitive = true,
      factoryMethod = "find",
      factoryArgName = Arg.SELF_REFERENCING_ARGNAME)
    public Finder finder;

    @Arg(longName = "listFinderWithCase",
      caseSensitive = true,
      factoryMethod = "find",
      factoryArgName = Arg.SELF_REFERENCING_ARGNAME)
    @Arg(shortName = 'o',
      factoryMethod = "find",
      factoryArgName = Arg.SELF_REFERENCING_ARGNAME)
    public List<Finder> listFinder;

    @Arg(shortName = 'g', factoryMethod = "find", factoryArgName = "value")
    public List<Finder> listGroupFinder;

    @Arg(shortName = 'n',
      longName = "arrayFinderN",
      factoryMethod = "find",
      factoryArgName = Arg.SELF_REFERENCING_ARGNAME)
    @Arg(shortName = 'i',
      factoryMethod = "com.codetaco.cli.InstantiatorTest$Finder.find",
      factoryArgName = Arg.SELF_REFERENCING_ARGNAME)
    public Finder[] arrayFinder;

    @Arg(caseSensitive = true)
    public List<String> stringList;

    @Arg
    public List<Integer> integerList;

    @Test
    public void arrayVariableExactClassName() {

        CmdLine.builder()
          .target(this)
          .args("-i WHAT WHEN WHERE")
          .build();

        Assertions.assertNotNull(arrayFinder);
        Assertions.assertEquals(3, arrayFinder.length);
        Assertions.assertTrue(arrayFinder[0] instanceof WhatFinder);
        Assertions.assertTrue(arrayFinder[1] instanceof WhenFinder);
        Assertions.assertTrue(arrayFinder[2] instanceof WhereFinder);
    }

    @Test
    public void arrayVariableShortClassName() {

        CmdLine.builder()
          .target(this)
          .args("-n WHAT WHEN WHERE")
          .build();

        Assertions.assertNotNull(arrayFinder);
        Assertions.assertEquals(3, arrayFinder.length);
        Assertions.assertTrue(arrayFinder[0] instanceof WhatFinder);
        Assertions.assertTrue(arrayFinder[1] instanceof WhenFinder);
        Assertions.assertTrue(arrayFinder[2] instanceof WhereFinder);
    }

    @Test
    public void badMethod() {
        try {

            CmdLine.builder()
              .target(this)
              .args("-m WHAT")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception e) {
            Assertions.assertEquals(
              "NoSuchMethodException expected: public static com.codetaco.cli.InstantiatorTest$Finder fXnd(java.lang.String) on com.codetaco.cli.InstantiatorTest$Finder",
              e.getMessage());
        }
    }

    @Test
    public void classFromVariable() {

        CmdLine.builder()
          .target(this)
          .args("-p WHAT")
          .build();

        Assertions.assertNotNull(finder);
        Assertions.assertTrue(finder instanceof WhatFinder);
        Assertions.assertEquals("WHAT", finder.value);
    }

    @Test
    public void colorExample1() {

        CmdLine.builder()
          .target(this)
          .args("--color red")
          .build();

        Assertions.assertNotNull(color);
        Assertions.assertTrue(color instanceof RedColor);
    }

    @Test
    public void enumKey() {

        CmdLine.builder()
          .target(this)
          .args("--suits H C")
          .build();

        Assertions.assertNotNull(suits);
        Assertions.assertEquals(2, suits.size());
        Assertions.assertTrue(suits.get(0) instanceof SuitFinder);
        Assertions.assertTrue(suits.get(1) instanceof SuitFinder);
        Assertions.assertEquals("HEARTS", suits.get(0).value);
        Assertions.assertEquals("CLUBS", suits.get(1).value);
    }

    @Test
    public void integerListByDefault() {

        CmdLine.builder()
          .target(this)
          .args("--integerList 1 2 3")
          .build();

        Assertions.assertNotNull(integerList);
        Assertions.assertEquals(3, integerList.size());
        Assertions.assertEquals(new Integer(1), integerList.get(0));
        Assertions.assertEquals(new Integer(2), integerList.get(1));
        Assertions.assertEquals(new Integer(3), integerList.get(2));
    }

    @Test
    public void listGroupVariable() {

        CmdLine.builder()
          .target(this)
          .args("-g (WHAT)(WHEN)(WHERE)")
          .build();

        Assertions.assertNotNull(listGroupFinder);
        Assertions.assertEquals(3, listGroupFinder.size());
        Assertions.assertTrue(listGroupFinder.get(0) instanceof WhatFinder);
        Assertions.assertTrue(listGroupFinder.get(1) instanceof WhenFinder);
        Assertions.assertTrue(listGroupFinder.get(2) instanceof WhereFinder);
        Assertions.assertEquals("WHAT", listGroupFinder.get(0).testValue());
        Assertions.assertEquals("WHEN", listGroupFinder.get(1).testValue());
        Assertions.assertEquals("WHERE", listGroupFinder.get(2).testValue());
    }

    @Test
    public void listStringVariable() {

        CmdLine.builder()
          .target(this)
          .args("--stringList WHAT WHEN WHERE")
          .build();

        Assertions.assertNotNull(stringList);
        Assertions.assertEquals(3, stringList.size());
        Assertions.assertEquals("WHAT", stringList.get(0));
        Assertions.assertEquals("WHEN", stringList.get(1));
        Assertions.assertEquals("WHERE", stringList.get(2));
    }

    @Test
    public void listVariable() {

        CmdLine.builder()
          .target(this)
          .args("--listFinderWithCase WHAT WHEN WHERE")
          .build();

        Assertions.assertNotNull(listFinder);
        Assertions.assertEquals(3, listFinder.size());
        Assertions.assertEquals("WHAT", listFinder.get(0).value);
        Assertions.assertEquals("WHEN", listFinder.get(1).value);
        Assertions.assertEquals("WHERE", listFinder.get(2).value);
    }

    @Test
    public void listVariableDefaultClassDefaultMethodClass() {

        CmdLine.builder()
          .target(this)
          .args("-o WHAT WHEN WHERE")
          .build();

        Assertions.assertNotNull(listFinder);
        Assertions.assertEquals(3, listFinder.size());
        Assertions.assertTrue(listFinder.get(0) instanceof WhatFinder);
        Assertions.assertTrue(listFinder.get(1) instanceof WhenFinder);
        Assertions.assertTrue(listFinder.get(2) instanceof WhereFinder);
    }

    @Test
    public void stringKey() {

        CmdLine.builder()
          .target(this)
          .args("--finderP WHAT")
          .build();

        Assertions.assertNotNull(finder);
        Assertions.assertEquals("WHAT", finder.value);
    }
}
