package com.codetaco.cli.help;

import org.junit.jupiter.api.Assertions;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;
import java.net.URL;

public class HelpHelp {
    private PrintStream standardOut;
    private PrintStream overrideOut;
    private ByteArrayOutputStream baos;

    public void compare(String fileName,
                        boolean debug) throws Exception {

        String[] actualLines = baos.toString().split("\n");
        if (debug) {
            for (String actual : actualLines) {
                System.out.print(actual);
            }
            Assertions.fail("always fails in debug mode");
        }

        URL url = getClass().getClassLoader().getResource(fileName);
        if (url == null) {
            Assertions.fail("not found: <classpath>/" + fileName);
        }
        File file = new File(url.toURI());
        try (BufferedReader sorted = new BufferedReader(new FileReader(file))) {
            for (String actual : actualLines) {
                String expected = sorted.readLine();
                if (expected == null) {
                    Assertions.fail("more lines produced than expected");
                } else {
                    Assertions.assertEquals(expected.trim(), actual.trim());
                }
            }
        }
    }

    public void startCapture() {
        baos = new ByteArrayOutputStream();
        overrideOut = new PrintStream(baos);
        standardOut = System.out;
        System.setOut(overrideOut);
    }

    public void stopCapture() {
        System.setOut(standardOut);
        overrideOut.flush();
        overrideOut.close();
    }
}
