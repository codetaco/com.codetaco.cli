package com.codetaco.cli.help.test1;

import com.codetaco.cli.CmdLine;
import com.codetaco.cli.annotation.Arg;
import com.codetaco.cli.help.HelpHelp;
import org.junit.jupiter.api.Test;

public class HelpTest {
    static public class Test1 {
        @Arg(shortName = 'a', help = "This is developer supplied narrative.", defaultValues = "abc")
        String item;

        @Arg(shortName = 'b', help = "This is developer supplied narrative.")
        String[] items;

        @Arg(shortName = 'c', help = "This is developer supplied narrative.")
        Test2 test2;

        @Arg(shortName = 'd', help = "This is developer supplied narrative.")
        Test2[] test2multi;
    }

    static public class Test2 {
        @Arg(positional = true, help = "This is developer supplied narrative.")
        String item;

        @Arg(shortName = 'b', help = "This is developer supplied narrative.")
        String items;
    }

    @Test
    public void testLevelHelp3() throws Exception {
        HelpHelp help = new HelpHelp();
        help.startCapture();
        CmdLine.builder()
          .target(new Test1())
          .name("The name of the Program")
          .help("A longer version of the top level documentation help")
          .args("--help")
          .build();
        help.stopCapture();
        help.compare("help3.txt", false);
    }

    @Test
    public void testLevelHelp1() throws Exception {
        HelpHelp help = new HelpHelp();
        help.startCapture();
        CmdLine.builder()
          .target(new Test1())
          .name("The name of the Program")
          .help("A longer version of the top level documentation help")
          .args("--help:1")
          .build();
        help.stopCapture();
        help.compare("help1.txt", false);
    }

    @Test
    public void testLevelHelp2() throws Exception {
        HelpHelp help = new HelpHelp();
        help.startCapture();
        CmdLine.builder()
          .target(new Test1())
          .name("The name of the Program")
          .help("A longer version of the top level documentation help")
          .args("--help:2")
          .build();
        help.stopCapture();
        help.compare("help2.txt", false);
    }

    @Test
    public void testQuestion() throws Exception {
        HelpHelp help = new HelpHelp();
        help.startCapture();
        CmdLine.builder()
          .target(new Test1())
          .name("The name of the Program")
          .help("A longer version of the top level documentation help")
          .args("-?")
          .build();
        help.stopCapture();
        help.compare("help1.txt", false);
    }
}
