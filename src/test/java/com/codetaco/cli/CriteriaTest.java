package com.codetaco.cli;

import com.codetaco.cli.CmdLineTest.DoubleParm;
import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CriteriaTest {
    static public class CritError {
        @Arg(range = {"4"}, inList = {"1", "2"})
        float var;
    }

    static public class CritError2 {
        @Arg(range = {"4"}, matches = "abc")
        float var;
    }

    @Test
    public void rangeAndList() {
        try {

            CmdLine.builder()
              .target(new DoubleParm())
              .args("--help")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals(
              "Only one criteria is allowed for \"float -i\", found \" --range '4.0'\" and \" --list '1.0' '2.0'\"",
              e.getMessage());
        }
    }

    @Test
    public void rangeAndMatches() {
        try {

            CmdLine.builder()
              .target(new DoubleParm())
              .args("--help")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals(
              "Only one criteria is allowed for \"float -i\", found \" --range '4.0'\" and \" --matches 'abc'\"",
              e.getMessage());
        }
    }
}
