package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.date.CalendarFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.List;

public class LocalTimeTest {

    @BeforeEach
    public void setZoneToLocalTime() {
        CalendarFactory.setZoneId(ZoneOffset.ofHours(-5));
    }

    @AfterEach
    public void setZoneToUTC() {
        CalendarFactory.setZoneId(ZoneOffset.UTC);
    }

    static private void assertValidReturnDate(LocalTime toBeVerified,
                                              int hour,
                                              int minute,
                                              int second,
                                              int nano) {
        Assertions.assertEquals(hour, toBeVerified.getHour());
        Assertions.assertEquals(minute, toBeVerified.getMinute());
        Assertions.assertEquals(second, toBeVerified.getSecond());
        Assertions.assertEquals(nano, toBeVerified.getNano());
    }

    @Arg
    LocalTime ldtArg;

    @Arg(caseSensitive = true, defaultValues = "now")
    LocalTime ldtForHelp;

    @Arg
    LocalTime[] ldtArray;

    @Arg
    List<LocalTime> ldtList;

    @Test
    public void arrays() {

        CmdLine.builder()
          .target(this)
          .args("--ldtArray '2016-08-06' '2016-08-06T14:18-05:00'")
          .build();

        assertValidReturnDate(ldtArray[0], 0, 0, 0, 0);
        assertValidReturnDate(ldtArray[1], 14, 18, 0, 0);
    }

    @Test
    public void helpDefaultNow() {

        CmdLine.builder()
          .target(this)
          .args("--help")
          .build();
    }

    @Test
    public void json() {

        CmdLine.builder()
          .target(this)
          .args("--ldtArg '2016-03-24T11:19:52.000-05:00'")
          .build();

        assertValidReturnDate(ldtArg, 11, 19, 52, 0);
    }

    @Test
    public void lists() {

        CmdLine.builder()
          .target(this)
          .args("--ldtList '2016-08-06' '2016-08-06T14:18-05:00'")
          .build();

        assertValidReturnDate(ldtList.get(0), 0, 0, 0, 0);
        assertValidReturnDate(ldtList.get(1), 14, 18, 0, 0);
    }

    @Test
    public void simpleUse() {

        CmdLine.builder()
          .target(this)
          .args("--ldtArg '2016-08-06'")
          .build();

        assertValidReturnDate(ldtArg, 0, 0, 0, 0);
    }
}
