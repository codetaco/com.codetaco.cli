package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.cli.impl.variables.IVariableAssigner;
import com.codetaco.cli.impl.variables.NullAssigner;
import com.codetaco.cli.impl.variables.VariableAssigner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class VariableTest {

    static public class MyGroup {
        @Arg(shortName = 's', positional = true)
        String testString;

        @Arg(excludeArgs = "innerGroup")
        MyGroup innerGroup;
    }

    @Arg(shortName = 'i')
    private boolean testboolean;

    @Arg(longName = "BClass")
    private Boolean testBoolean;

    @Arg
    private int testInt;

    @Arg
    private Integer testInteger;

    @Arg
    private Integer[] testIntegerArray;

    @Arg
    private int[] testIntArray;

    @Arg
    private Float testFloat;

    @Arg(longName = "ClassFloatArray")
    private Float[] testFloatArray;

    @Arg(longName = "PrimitiveFloatArray")
    private float[] testfloatArray;

    @Arg
    private String testString;

    @Arg
    private String[] testStringArray;

    @Arg(shortName = 'g')
    private MyGroup testGroup;

    @Arg(instanceClass = "com.codetaco.cli.VariableTest$MyGroup")
    private Object testObjectGroup;

    @Arg
    private MyGroup testGroupArray[];

    @Arg
    private List<MyGroup> testGroupList;

    public VariableTest() {

    }

    @Test
    public void groupWithoutOwnVariable() {

        CmdLine cl = CmdLine.builder()
                       .target(this)
                       .args("-g('1string')")
                       .build();

        Assertions.assertTrue(cl.isParsed("-g"));
        Assertions.assertTrue(cl.subparser("-g").isParsed("-s"));

        Assertions.assertEquals("1string", testGroup.testString);
    }

    @Test
    public void nullAssigner() {
        IVariableAssigner previousAssigner = VariableAssigner.setInstance(new NullAssigner());
        IVariableAssigner nullAssigner = VariableAssigner.setInstance(previousAssigner);
        try {
            VariableAssigner.setInstance(nullAssigner);

            CmdLine.builder()
              .target(this)
              .args("-g('1string')")
              .build();

            Assertions.assertNull(testString);
        } finally {
            VariableAssigner.setInstance(previousAssigner);
        }
    }

    @Test
    public void variablebooleanAssignment() {

        CmdLine.builder()
          .target(this)
          .args("-i")
          .build();

        Assertions.assertTrue(testboolean);
    }

    @Test
    public void variableBooleanAssignment() {

        CmdLine.builder()
          .target(this)
          .args("--bclass")
          .build();

        Assertions.assertTrue(testBoolean);
    }

    @Test
    public void variablefloatArrayAssignment() {

        CmdLine cl = CmdLine.builder()
                       .target(this)
                       .args("--primitiveFloatArray 2.1 1.2")
                       .build();

        Assertions.assertTrue(cl.isParsed("--primitiveFloatArray"));

        Assertions.assertEquals(2.1F, testfloatArray[0]);
        Assertions.assertEquals(1.2F, testfloatArray[1]);
    }

    @Test
    public void variableFloatArrayAssignment() {

        CmdLine.builder()
          .target(this)
          .args("--ClassFloatArray 2.1 1.2")
          .build();

        Assertions.assertEquals(2.1F, testFloatArray[0].floatValue());
        Assertions.assertEquals(1.2F, testFloatArray[1].floatValue());
    }

    @Test
    public void variableFloatAssignment() {

        CmdLine.builder()
          .target(this)
          .args("--testFloat 12.3")
          .build();

        Assertions.assertEquals(12.3F, testFloat.floatValue());
    }

    @Test
    public void variableGroupArrayStringAssignment() {

        CmdLine cl = CmdLine.builder()
                       .target(this)
                       .args("--testGroupArray('1string')('2string')('-s3')")
                       .build();

        Assertions.assertTrue(cl.isParsed("--testGroupArray"));
        Assertions.assertTrue(cl.subparsers("--testGroupArray").get(0).isParsed("-s"));
        Assertions.assertTrue(cl.subparsers("--testGroupArray").get(1).isParsed("-s"));
        Assertions.assertTrue(cl.subparsers("--testGroupArray").get(2).isParsed("-s"));

        Assertions.assertEquals("1string", testGroupArray[0].testString);
        Assertions.assertEquals("2string", testGroupArray[1].testString);
        Assertions.assertEquals("-s3", testGroupArray[2].testString);
    }

    @Test
    public void variableGroupGroupStringAssignment() {

        CmdLine.builder()
          .target(this)
          .args("--testGroup(--innerGroup('a group string'))")
          .build();

        Assertions.assertEquals("a group string", testGroup.innerGroup.testString);
    }

    @Test
    public void variableGroupListStringAssignment() {

        CmdLine.builder()
          .target(this)
          .args("--testGroupList('1string')('2string')('-s3')")
          .build();

        Assertions.assertEquals("1string", testGroupList.get(0).testString);
        Assertions.assertEquals("2string", testGroupList.get(1).testString);
        Assertions.assertEquals("-s3", testGroupList.get(2).testString);
    }

    @Test
    public void variableGroupStringAssignment() {

        CmdLine.builder()
          .target(this)
          .args("--testGroup('a group string')")
          .build();

        Assertions.assertEquals("a group string", testGroup.testString);
    }

    @Test
    public void variableGroupStringAssignmentToObjectList() {

        CmdLine.builder()
          .target(this)
          .args("--testObjectGroup('a group string')")
          .build();

        Assertions.assertNotNull(testObjectGroup);
        Assertions.assertEquals("a group string", ((MyGroup) testObjectGroup).testString);
    }

    @Test
    public void variableIntArrayAssignment() {

        CmdLine.builder()
          .target(this)
          .args("--testIntArray 2 1")
          .build();

        Assertions.assertEquals(2, testIntArray[0]);
        Assertions.assertEquals(1, testIntArray[1]);
    }

    @Test
    public void variableintAssignment() {

        CmdLine.builder()
          .target(this)
          .args("--testInt 123")
          .build();

        Assertions.assertEquals(123, testInt);
    }

    @Test
    public void variableIntegerArrayAssignment() {

        CmdLine.builder()
          .target(this)
          .args("--testIntegerArray 2 1")
          .build();

        Assertions.assertEquals(2, testIntegerArray[0].intValue());
        Assertions.assertEquals(1, testIntegerArray[1].intValue());
    }

    @Test
    public void variableIntegerAssignment() {

        CmdLine.builder()
          .target(this)
          .args("--testInteger 123")
          .build();

        Assertions.assertEquals(123, testInteger.intValue());
    }

    @Test
    public void variableStringArrayAssignment() {

        CmdLine.builder()
          .target(this)
          .args("--testStringArray 'this is a test string' anotheronetoo")
          .build();

        Assertions.assertEquals("this is a test string", testStringArray[0]);
        Assertions.assertEquals("anotheronetoo", testStringArray[1]);
    }

    @Test
    public void variableStringAssignment() {

        CmdLine.builder()
          .target(this)
          .args("--testString 'this is a test string'")
          .build();

        Assertions.assertEquals("this is a test string", testString);
    }
}
