package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BracketTest {

    static public class Group {
        @Arg(shortName = 'a')
        boolean b1;

        @Arg(shortName = 'b')
        boolean b2;
    }

    @Arg(shortName = 'g')
    Group[] group;

    @Test
    public void testMultiGroup() {

        com.codetaco.cli.CmdLine.builder()
          .target(this)
          .args("-g[-a][-b]")
          .build();

        Assertions.assertTrue(group[0].b1);
        Assertions.assertFalse(group[0].b2);
        Assertions.assertFalse(group[1].b1);
        Assertions.assertTrue(group[1].b2);
    }
}
