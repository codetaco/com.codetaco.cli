package com.codetaco.cli.annotation;

import com.codetaco.cli.CliException;
import com.codetaco.cli.CmdLine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AnnotateRegexCriteriaTest {
    @Arg(matches = "^[A-Z]+$", caseSensitive = true)
    private String stringForRegex;

    @Test
    public void regex() {
        CmdLine.builder()
          .target(this)
          .args("--s ABC")
          .build();

        Assertions.assertEquals("ABC", stringForRegex);
    }

    @Test
    public void regexNot() {
        try {
            CmdLine.builder()
              .target(this)
              .args("--s AzC")
              .build();

            Assertions.fail("expected exception");

        } catch (CliException e) {
            Assertions.assertEquals("AzC is not valid for --stringForRegex", e.getMessage());
        }
    }
}
