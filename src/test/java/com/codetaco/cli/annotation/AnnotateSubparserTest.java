package com.codetaco.cli.annotation;

import com.codetaco.cli.CmdLine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AnnotateSubparserTest {
    static final public class EmbeddedA {
        @Arg
        private String embeddedAString;

        @Arg
        private EmbeddedAC embeddedAC;
    }

    static final public class EmbeddedAC {
        @Arg
        private String embeddedACString;
    }

    static final public class EmbeddedB {
        @Arg
        private String embeddedBString;
    }

    static final public class RootConfig {
        @Arg
        private String rootConfigString;

        @Arg
        private EmbeddedA embeddedA;

        @Arg
        private EmbeddedB embeddedB;
    }

    private final RootConfig cfg = new RootConfig();

    @Test
    public void testDefaults() {
        CmdLine.builder()
          .target(this)
          .build();
        Assertions.assertNull(cfg.embeddedA);
    }

    @Test
    public void testUsage() {
        CmdLine.builder()
          .target(this)
          .args("--help")
          .build();
    }

    @Test
    public void testValues() {
        CmdLine.builder()
          .target(cfg)
          .args("--rootConfigString hi")
          .args("--embeddedA(--embeddedAString hello --embeddedAC(--embeddedACString heyThere))")
          .args("--embeddedB(--embeddedBString hola)")
          .build();

        Assertions.assertEquals("hi", cfg.rootConfigString);
        Assertions.assertEquals("hello", cfg.embeddedA.embeddedAString);
        Assertions.assertEquals("heythere", cfg.embeddedA.embeddedAC.embeddedACString);
        Assertions.assertEquals("hola", cfg.embeddedB.embeddedBString);
    }

}
