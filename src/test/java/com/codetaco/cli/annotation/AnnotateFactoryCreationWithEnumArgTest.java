package com.codetaco.cli.annotation;

import com.codetaco.cli.CmdLine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AnnotateFactoryCreationWithEnumArgTest {
    public static class Config {
        public static Config create(final String keyType) {
            final Config cfg = new Config();
            cfg.name = "createdAs" + keyType;
            return cfg;
        }

        private String name;
        @Arg
        private KeyType type;
    }

    public static enum KeyType {
        INT,
        STR,
        CHR,
        FLT
    }

    @Arg(factoryMethod = "create", factoryArgName = "type")
    private Config cfg;

    @Test
    public void enumArg() {

        CmdLine.builder()
          .target(this)
          .args("--cfg(--type I)")
          .build();

        Assertions.assertEquals("createdAsINT", cfg.name);

    }

}
