package com.codetaco.cli.annotation;

import com.codetaco.cli.CmdLine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * This class shows that an array can be of an interface type as long as the instanceClass is specified, there can be
 * multiple args contributing to the array, each implementation class can have distinct or inherited args.
 *
 * @author Chris DeGreef fedupforone@gmail.com
 * @since 4.1.2
 */
public class AnnotateInstanceOverrideTest {
    public static class FirstNameOnly extends NamedInstance {
    }

    public static interface INamed {
        public String getName();
    }

    public static abstract class NamedInstance implements INamed {
        @Arg(positional = true, caseSensitive = true, required = true)
        private String name;

        @Override
        public String getName() {
            return name;
        }
    }

    public static class WithLastName extends NamedInstance {
        @Arg(caseSensitive = true, required = true)
        private String lastName;
    }

    @Arg(shortName = 'p',
      longName = "pPoly",
      instanceClass = "com.codetaco.cli.annotation.AnnotateInstanceOverrideTest$WithLastName",
      multimin = 1)
    @Arg(shortName = 'q',
      longName = "qPoly",
      instanceClass = "com.codetaco.cli.annotation.AnnotateInstanceOverrideTest$FirstNameOnly",
      multimin = 2)
    private INamed[] polymorphic;

    @Test
    public void instanceOverride() {

        CmdLine.builder()
          .target(this)
          .args("-p(pName --l McLastname) -q(oneFirstName)(anotherFirstName)")
          .build();

        Assertions.assertEquals(WithLastName.class, polymorphic[0].getClass());
        Assertions.assertEquals(FirstNameOnly.class, polymorphic[1].getClass());
        Assertions.assertEquals(FirstNameOnly.class, polymorphic[2].getClass());
        Assertions.assertEquals("pName", polymorphic[0].getName());
        Assertions.assertEquals("oneFirstName", polymorphic[1].getName());
        Assertions.assertEquals("anotherFirstName", polymorphic[2].getName());
    }
}
