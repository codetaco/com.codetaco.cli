package com.codetaco.cli.annotation;

import com.codetaco.cli.CliException;
import com.codetaco.cli.CmdLine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AnnotateInListCriteriaTest {
    @Arg(inList = {"abc", "def"})
    private String stringForInList;

    @Arg(inList = {"1", "3", "5"})
    private int intForInList;

    @Test
    public void inList() {

        CmdLine.builder()
          .target(this)
          .args("--s ABC")
          .build();

        Assertions.assertEquals("abc", stringForInList);
    }

    @Test
    public void inListInt() {

        CmdLine.builder()
          .target(this)
          .args("--i 3")
          .build();

        Assertions.assertEquals(3, intForInList);
    }

    @Test
    public void notInList() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--s AzC")
              .build();

            Assertions.fail("expected exception");

        } catch (CliException e) {
            Assertions.assertEquals("azc is not valid for --stringForInList", e.getMessage());
        }
    }

    @Test
    public void notInListInt() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--i 2")
              .build();
            Assertions.fail("expected exception");

        } catch (CliException e) {
            Assertions.assertEquals("2 is not valid for --intForInList", e.getMessage());
        }
    }
}
