package com.codetaco.cli.annotation;

import com.codetaco.cli.CmdLine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class AnnotateFactoryCreationTests {
    public static abstract class AbstractMyClass {
    }

    public static class MyClass extends AbstractMyClass {
        static public MyClass create() {
            final MyClass thisOne = new MyClass();
            thisOne.name = "NONAME";
            return thisOne;
        }

        static public MyClass create(final String aName) {
            final MyClass thisOne = new MyClass();
            thisOne.nameOverride = true;
            thisOne.name = aName;
            return thisOne;
        }

        boolean nameOverride;

        @Arg(positional = true, caseSensitive = true)
        String name;

        public String name() {
            return name;
        }
    }

    @Arg(factoryMethod = "create")
    private MyClass noArg;

    @Arg(factoryMethod = "create", factoryArgName = "name")
    private MyClass withArg;

    @Arg(longName = "headerIn",
      help = "Column definitions defining the file header layout.",
      factoryMethod = "com.codetaco.cli.annotation.AnnotateFactoryCreationTests$MyClass.create",
      multimin = 1,
      excludeArgs = {"csvFieldNumber", "direction"})
    public List<AbstractMyClass> headerInDefs;

    @Test
    public void listWithFactory() {

        CmdLine.builder()
          .target(this)
          .args("--headerIn()")
          .build();

        Assertions.assertEquals("NONAME", ((MyClass) headerInDefs.get(0)).name());
    }

    @Test
    public void testNoParmOnFactory() {
        CmdLine.builder()
          .target(this)
          .args("--noArg()")
          .build();

        Assertions.assertEquals("NONAME", noArg.name());

    }

    @Test
    public void testParmOnFactory() {
        CmdLine.builder()
          .target(this)
          .args("--withArg('WHAT!') ")
          .build();

        Assertions.assertEquals("WHAT!", withArg.name());

    }

}
