package com.codetaco.cli.annotation;

import com.codetaco.cli.CliException;
import com.codetaco.cli.CmdLine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AnnotateRangeCriteriaTest {
    @Arg(range = {"5", "15"})
    private int rangedPrimitive;

    @Arg(range = {"5", "15"})
    private Integer rangedInstance;

    @Arg(range = {"5"})
    private int minOnly;

    @Test
    public void minOnly() {
        CmdLine.builder()
          .target(this)
          .args("--min 5")
          .build();

        Assertions.assertEquals(5, minOnly);
    }

    @Test
    public void minOnlyError() {
        try {
            CmdLine.builder()
              .target(this)
              .args("--min 3")
              .build();

            Assertions.fail("expected exception");
        } catch (CliException e) {
            Assertions.assertEquals("3 is not valid for --minOnly", e.getMessage());
        }
    }

    @Test
    public void unitializedInstance() {
        CmdLine.builder()
          .target(this)
          .build();

        Assertions.assertNull(rangedInstance);
    }

    @Test
    public void unitializedPrimative() {
        CmdLine.builder()
          .target(this)
          .build();

        Assertions.assertEquals(0, rangedPrimitive);
    }

}
