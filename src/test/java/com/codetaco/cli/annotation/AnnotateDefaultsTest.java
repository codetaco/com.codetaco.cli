package com.codetaco.cli.annotation;

import com.codetaco.cli.CmdLine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AnnotateDefaultsTest {
    @Arg(shortName = 's', defaultValues = {"abc", "def"})
    private String[] stringDefaults;

    @Test
    public void allowDefaults() {
        /*-
        final ICmdLine cmdParser = new CmdLineImpl("allowDefaults", "", '-', '!');
        final IParserInput userInput = CommandLineParser.getInstance('-', new String[] {});
        cmdParser.parse(userInput, this);
        */

        CmdLine.builder().target(this).args().build();

        Assertions.assertEquals(2, stringDefaults.length);
        Assertions.assertEquals("abc", stringDefaults[0]);
        Assertions.assertEquals("def", stringDefaults[1]);
    }

    @Test
    public void overrideDefaults() {
        CmdLine.builder()
          .target(this)
          .args("-s xyz")
          .build();

        Assertions.assertEquals(1, stringDefaults.length);
        Assertions.assertEquals("xyz", stringDefaults[0]);
    }

}
