package com.codetaco.cli.annotation;

import com.codetaco.cli.CliException;
import com.codetaco.cli.CmdLine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class AnnotateSimpleModifiersTest {
    @Arg(longName = "name", shortName = 'n', help = "Some help for the wayward user")
    private String nameOverride;

    @Arg(required = true, shortName = 'r')
    private boolean isRequired;

    @Arg(multimin = 2)
    private String[] atLeastTwo;

    @Arg(multimax = 2)
    private String[] atMostTwo;

    @Arg(allowMetaphone = true)
    private String allowAlternateNaming;

    @Arg(caseSensitive = true)
    private String caseSensitive;

    @Arg(positional = true, caseSensitive = true)
    private String positional;

    @Arg(format = "mm/DD/yyyy")
    private Date formatted;

    @Test
    public void testAlternateNaming() {
        CmdLine.builder()
          .target(this)
          .args("-r --allowAlternateNaming regular")
          .build();
        CmdLine.builder()
          .target(this)
          .args("-r --AAN camelCaps")
          .build();
        CmdLine.builder()
          .target(this)
          .args("-r --alowAlturnateNames metaphone")
          .build();
    }

    @Test
    public void testAtLeast2() {
        CmdLine.builder()
          .target(this)
          .args("-r --atLeastTwo one two")
          .build();

        try {
            CmdLine.builder()
              .target(this)
              .args("-r --atLeastTwo oneOnly")
              .build();
            Assertions.fail("expected an exception");

        } catch (CliException e) {
            Assertions.assertEquals("insufficient required values for --atLeastTwo", e.getMessage());
        }
    }

    @Test
    public void testAtMost2() {
        CmdLine.builder()
          .target(this)
          .args("-r --atMostTwo one")
          .build();
        CmdLine.builder()
          .target(this)
          .args("-r --atMostTwo one two")
          .build();
        CmdLine.builder()
          .target(this)
          .args("-r --atMostTwo one two three")
          .build();

        Assertions.assertEquals(2, atMostTwo.length);
        Assertions.assertEquals("three", positional);
    }

    @Test
    public void testCaseSensitive() {
        CmdLine.builder()
          .target(this)
          .args("-r --CaseSensitive AbCdE")
          .build();
        Assertions.assertEquals("AbCdE", caseSensitive);
    }

    @Test
    public void testDateFormatting() {
        CmdLine.builder()
          .target(this)
          .args("-r --formatted '07/28/2016'")
          .build();

        try {
            CmdLine.builder()
              .target(this)
              .args("-r --formatted '2016'")
              .build();
            Assertions.fail("expected an exception");

        } catch (CliException e) {
            Assertions.assertEquals("--formatted mm/DD/yyyy: Unparseable date: \"2016\"", e.getMessage());
        }
    }

    @Test
    public void testLongName() {
        CmdLine.builder()
          .target(this)
          .args("--name abc --isRequired")
          .build();
        Assertions.assertEquals("abc", nameOverride);
    }

    @Test
    public void testPostional() {
        CmdLine.builder()
          .target(this)
          .args("-r myPositionalValue")
          .build();
        Assertions.assertEquals("myPositionalValue", positional);

        try {
            CmdLine.builder()
              .target(this)
              .args("-r --positional shouldFail")
              .build();
            Assertions.fail("expected an exception");

        } catch (CliException e) {
            Assertions.assertEquals("unexpected input: --positional ", e.getMessage());
        }
    }

    @Test
    public void testShortName() {
        CmdLine.builder()
          .target(this)
          .args("-n abc -r")
          .build();
        Assertions.assertEquals("abc", nameOverride);
    }

}
