package com.codetaco.cli.annotation;

import com.codetaco.cli.CliException;
import com.codetaco.cli.CmdLine;
import com.codetaco.cli.type.WildFiles;
import com.codetaco.math.Equ;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.lang.management.MemoryType;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Pattern;

public class AnnotateAllTypesTest {
    @Arg
    public boolean pBooleanVar;
    @Arg()
    public int pIntVar;
    @Arg()
    public long pLongVar;

    @Arg()
    public float pFloatVar;
    @Arg()
    public double pDoubleVar;
    @Arg()
    public char pCharVar;
    @Arg()
    public byte pByteVar;
    @Arg()
    public Boolean booleanVar;
    @Arg()
    public Integer intVar;
    @Arg()
    public Long longVar;
    @Arg()
    public Float floatVar;

    @Arg()
    public Double doubleVar;
    @Arg()
    public Character charVar;
    @Arg()
    public Byte byteVar;
    @Arg()
    public String stringVar;

    @Arg()
    public int[] pIntVars;
    @Arg()
    public long[] pLongVars;
    @Arg()
    public float[] pFloatVars;
    @Arg()
    public double[] pDoubleVars;
    @Arg()
    public char[] pCharVars;
    @Arg()
    public byte[] pByteVars;

    @Arg()
    public Integer[] intVars;
    @Arg()
    public Long[] longVars;
    @Arg()
    public Float[] floatVars;
    @Arg()
    public Double[] doubleVars;
    @Arg()
    public Character[] charVars;
    @Arg()
    public Byte[] byteVars;
    @Arg()
    public String[] stringVars;

    @Arg()
    public List<Integer> intVarList;
    @Arg()
    public List<Long> longVarList;
    @Arg()
    public List<Float> floatVarList;
    @Arg()
    public List<Double> doubleVarList;
    @Arg()
    public List<Character> charVarList;
    @Arg()
    public List<Byte> byteVarList;
    @Arg()
    public List<String> stringVarList;

    @Arg()
    public File fileVar;
    @Arg()
    public Pattern patternVar;
    @Arg()
    public WildFiles wildFilesVar;
    @Arg()
    public Date dateVar;

    @Arg()
    public File[] fileVars;
    @Arg()
    public Pattern[] patternVars;
    @Arg()
    public Date[] dateVars;

    @Arg()
    public MemoryType enumVar;

    @Arg()
    public Equ equVar;
    @Arg()
    public Equ[] equVars;

    @Arg()
    public List<File> fileVarList;
    @Arg()
    public List<Pattern> patternVarList;
    @Arg()
    public List<Date> dateVarList;
    @Arg()
    public List<Equ> equVarList;

    TimeZone defaultTimeZone;

    @AfterEach
    public void resetTimezone() {
        TimeZone.setDefault(defaultTimeZone);
    }

    @BeforeEach
    public void setSpecificTimezone() {
        defaultTimeZone = TimeZone.getDefault();
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    @Test
    public void testAllDefaultValues() {

        try {
            CmdLine.builder()
              .target(this)
              .build();

            Assertions.assertFalse(pBooleanVar);
            Assertions.assertEquals(0, pIntVar);
            Assertions.assertEquals(0, pFloatVar, 0.001);
            Assertions.assertEquals(0, pDoubleVar, 0.001);
            Assertions.assertEquals(0, pCharVar);
            Assertions.assertEquals(0, pByteVar);

            Assertions.assertFalse(booleanVar);
            Assertions.assertNull(intVar);
            Assertions.assertNull(floatVar);
            Assertions.assertNull(doubleVar);
            Assertions.assertNull(stringVar);
            Assertions.assertNull(charVar);
            Assertions.assertNull(byteVar);

            Assertions.assertNull(pIntVars);
            Assertions.assertNull(pFloatVars);
            Assertions.assertNull(pDoubleVars);
            Assertions.assertNull(pCharVars);
            Assertions.assertNull(pByteVars);

            Assertions.assertNull(intVars);
            Assertions.assertNull(floatVars);
            Assertions.assertNull(doubleVars);
            Assertions.assertNull(stringVars);
            Assertions.assertNull(charVars);
            Assertions.assertNull(byteVars);

            Assertions.assertNull(dateVar);
            Assertions.assertNull(enumVar);
            Assertions.assertNull(equVar);
            Assertions.assertNull(fileVar);
            Assertions.assertNull(patternVar);
            Assertions.assertNull(wildFilesVar);

            Assertions.assertNull(dateVars);
            Assertions.assertNull(equVars);
            Assertions.assertNull(fileVars);
            Assertions.assertNull(patternVars);

            Assertions.assertNull(intVarList);
            Assertions.assertNull(longVarList);
            Assertions.assertNull(floatVarList);
            Assertions.assertNull(doubleVarList);
            Assertions.assertNull(charVarList);
            Assertions.assertNull(byteVarList);
            Assertions.assertNull(stringVarList);
            Assertions.assertNull(fileVarList);
            Assertions.assertNull(patternVarList);
            Assertions.assertNull(dateVarList);
            Assertions.assertNull(equVarList);

        } catch (CliException e) {
            Assertions.fail(e.getMessage());
        }
    }

    @Test
    public void testAllSpecifiedValues() {

        try {
            CmdLine.builder()
              .target(this)
              .args(" --pBooleanVar")
              .args(" --pIntVar 12")
              .args(" --pLongVar 67")
              .args(" --pFloatVar 12.34")
              .args(" --pDoubleVar 45")
              .args(" --pCharVar a")
              .args(" --pByteVar b")

              .args(" --booleanVar")
              .args(" --intVar 12")
              .args(" --longVar 67")
              .args(" --floatVar 12.34")
              .args(" --doubleVar 45")
              .args(" --stringVar 'WHAT!'")
              .args(" --charVar a")
              .args(" --byteVar b")

              .args(" --pIntVars 12 21")
              .args(" --pLongVars 67 76")
              .args(" --pFloatVars 12.34 43.21")
              .args(" --pDoubleVars 45 54")
              .args(" --pCharVars a b")
              .args(" --pByteVars b a")

              .args(" --intVars 12 21")
              .args(" --longVars 67 76")
              .args(" --floatVars 12.34 43.21")
              .args(" --doubleVars 45 54")
              .args(" --stringVars 'WHAT!' 'WHEN!'")
              .args(" --charVars a b")
              .args(" --byteVars b a")

              .args(" --dateVar '2016-07-27'")
              .args(" --enumVar HEAP")
              .args(" --equVar a")
              .args(" --fileVar b")
              .args(" --patternVar c")
              .args(" --wildFilesVar d")

              .args(" --dateVars  '2016-07-27'  '2016-07-28'")
              .args(" --equVars b a")
              .args(" --fileVars b a")
              .args(" --patternVars b a")

              .args(" --equVarList g h i")

              .args(" --intVarList 1 2 3")
              .args(" --longVarList 4 5 6")
              .args(" --floatVarList 7.1 7.2 7.3")
              .args(" --doubleVarList 8.1 8.2 8.3")
              .args(" --charVarList A B C")
              .args(" --byteVarList D E F")
              .args(" --stringVarList 'Hello world' 'good bye'")
              .args(" --fileVarList file1 file2 file3")
              .args(" --patternVarList '.*' '$[0-1]*^'")
              .args(" --dateVarList '2016-07-29'  '2016-07-30'")
              .build();

            Assertions.assertTrue(pBooleanVar);
            Assertions.assertEquals(12, pIntVar);
            Assertions.assertEquals(67, pLongVar);
            Assertions.assertEquals(12.34, pFloatVar, 0.001);
            Assertions.assertEquals(45, pDoubleVar, 0.001);
            Assertions.assertEquals('a', pCharVar);
            Assertions.assertEquals('b', pByteVar);

            Assertions.assertTrue(booleanVar);
            Assertions.assertEquals(12, intVar.intValue());
            Assertions.assertEquals(67, longVar.longValue());
            Assertions.assertEquals(12.34, floatVar, 0.001);
            Assertions.assertEquals(45, doubleVar, 0.001);
            Assertions.assertEquals("what!", stringVar);
            Assertions.assertEquals('a', charVar.charValue());
            Assertions.assertEquals('b', byteVar.byteValue());

            Assertions.assertEquals(12, pIntVars[0]);
            Assertions.assertEquals(67, pLongVars[0]);
            Assertions.assertEquals(12.34, pFloatVars[0], 0.001);
            Assertions.assertEquals(45, pDoubleVars[0], 0.001);
            Assertions.assertEquals('a', pCharVars[0]);
            Assertions.assertEquals('b', pByteVars[0]);
            Assertions.assertEquals(21, pIntVars[1]);
            Assertions.assertEquals(76, pLongVars[1]);
            Assertions.assertEquals(43.21, pFloatVars[1], 0.001);
            Assertions.assertEquals(54, pDoubleVars[1], 0.001);
            Assertions.assertEquals('b', pCharVars[1]);
            Assertions.assertEquals('a', pByteVars[1]);

            Assertions.assertEquals(12, intVars[0].intValue());
            Assertions.assertEquals(67, longVars[0].longValue());
            Assertions.assertEquals(12.34, floatVars[0], 0.001);
            Assertions.assertEquals(45, doubleVars[0], 0.001);
            Assertions.assertEquals("what!", stringVars[0]);
            Assertions.assertEquals('a', charVars[0].charValue());
            Assertions.assertEquals('b', byteVars[0].byteValue());
            Assertions.assertEquals(21, intVars[1].intValue());
            Assertions.assertEquals(76, longVars[1].longValue());
            Assertions.assertEquals(43.21, floatVars[1], 0.001);
            Assertions.assertEquals(54, doubleVars[1], 0.001);
            Assertions.assertEquals("when!", stringVars[1]);
            Assertions.assertEquals('b', charVars[1].charValue());
            Assertions.assertEquals('a', byteVars[1].byteValue());

            Assertions.assertEquals(1469577600000L, dateVar.getTime());
            Assertions.assertEquals(MemoryType.HEAP, enumVar);
            Assertions.assertEquals("a", equVar.toString());
            Assertions.assertEquals("b", fileVar.getName());
            Assertions.assertEquals("c", patternVar.pattern());
            Assertions.assertEquals("d", wildFilesVar.toString());

            Assertions.assertEquals(1469577600000L, dateVars[0].getTime());
            Assertions.assertEquals(1469664000000L, dateVars[1].getTime());

            Assertions.assertEquals("b", equVars[0].toString());
            Assertions.assertEquals("a", equVars[1].toString());

            Assertions.assertEquals("b", fileVars[0].getName());
            Assertions.assertEquals("a", fileVars[1].getName());

            Assertions.assertEquals("b", patternVars[0].pattern());
            Assertions.assertEquals("a", patternVars[1].pattern());

            Assertions.assertEquals(1, intVarList.get(0).intValue());
            Assertions.assertEquals(2, intVarList.get(1).intValue());
            Assertions.assertEquals(3, intVarList.get(2).intValue());

            Assertions.assertEquals(4, longVarList.get(0).longValue());
            Assertions.assertEquals(5, longVarList.get(1).longValue());
            Assertions.assertEquals(6, longVarList.get(2).longValue());

            Assertions.assertEquals(7.1, floatVarList.get(0).floatValue(), 0.01);
            Assertions.assertEquals(7.2, floatVarList.get(1).floatValue(), 0.01);
            Assertions.assertEquals(7.3, floatVarList.get(2).floatValue(), 0.01);

            Assertions.assertEquals(8.1, doubleVarList.get(0).doubleValue(), 0.01);
            Assertions.assertEquals(8.2, doubleVarList.get(1).doubleValue(), 0.01);
            Assertions.assertEquals(8.3, doubleVarList.get(2).doubleValue(), 0.01);

            Assertions.assertEquals('a', charVarList.get(0).charValue());
            Assertions.assertEquals('b', charVarList.get(1).charValue());
            Assertions.assertEquals('c', charVarList.get(2).charValue());

            Assertions.assertEquals('d', byteVarList.get(0).byteValue());
            Assertions.assertEquals('e', byteVarList.get(1).byteValue());
            Assertions.assertEquals('f', byteVarList.get(2).byteValue());

            Assertions.assertEquals("hello world", stringVarList.get(0));
            Assertions.assertEquals("good bye", stringVarList.get(1));

            Assertions.assertEquals("file1", fileVarList.get(0).getName());
            Assertions.assertEquals("file2", fileVarList.get(1).getName());
            Assertions.assertEquals("file3", fileVarList.get(2).getName());

            Assertions.assertEquals(".*", patternVarList.get(0).pattern());
            Assertions.assertEquals("$[0-1]*^", patternVarList.get(1).pattern());

            Assertions.assertEquals(1469750400000L, dateVarList.get(0).getTime());
            Assertions.assertEquals(1469836800000L, dateVarList.get(1).getTime());

            Assertions.assertEquals("g", equVarList.get(0).toString());
            Assertions.assertEquals("h", equVarList.get(1).toString());
            Assertions.assertEquals("i", equVarList.get(2).toString());

        } catch (CliException e) {
            Assertions.fail(e.getMessage());
        }
    }

    @Test
    public void testDate() {
        try {
            CmdLine.builder()
              .target(this)
              .args(" --dateVar '2016-07-27'")
              .build();

            Assertions.assertEquals(1469577600000L, dateVar.getTime());

        } catch (CliException e) {
            Assertions.fail(e.getMessage());
        }
    }

    @Test
    public void testUsage() {

        try {
            CmdLine.builder()
              .target(this)
              .args("--help")
              .build();

        } catch (CliException e) {
            Assertions.fail(e.getMessage());
        }
    }
}
