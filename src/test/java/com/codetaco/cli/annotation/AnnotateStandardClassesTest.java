package com.codetaco.cli.annotation;

import com.codetaco.cli.CmdLine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.List;

public class AnnotateStandardClassesTest {
    @Arg
    @Arg(variable = "width", positional = true, help = "The width of the dimension")
    @Arg(variable = "height", positional = true, help = "The height of the dimension")
    private Dimension dim;

    @Arg
    @Arg(variable = "width", positional = true, help = "The width of the dimension")
    @Arg(variable = "height", positional = true, help = "The height of the dimension")
    private Dimension[] dimArray;

    @Arg
    @Arg(variable = "width", positional = true, help = "The width of the dimension")
    @Arg(variable = "height", positional = true, help = "The height of the dimension")
    private List<Dimension> dimList;

    @Test
    public void dim() {
        CmdLine.builder().target(this).args("--dim(1,2)").build();

        Assertions.assertEquals(1, dim.getWidth(), 0.1D);
        Assertions.assertEquals(2, dim.getHeight(), 0.1D);
    }

    @Test
    public void dimArray() {
        CmdLine.builder().target(this).args("--dimArray(1,2)(3,4)").build();
        Assertions.assertEquals(1, dimArray[0].getWidth(), 0.1D);
        Assertions.assertEquals(2, dimArray[0].getHeight(), 0.1D);
        Assertions.assertEquals(3, dimArray[1].getWidth(), 0.1D);
        Assertions.assertEquals(4, dimArray[1].getHeight(), 0.1D);
    }

    @Test
    public void dimList() {
        CmdLine.builder().target(this).args("--dimList(1,2)(3,4)").build();
        Assertions.assertEquals(1, dimList.get(0).getWidth(), 0.1D);
        Assertions.assertEquals(2, dimList.get(0).getHeight(), 0.1D);
        Assertions.assertEquals(3, dimList.get(1).getWidth(), 0.1D);
        Assertions.assertEquals(4, dimList.get(1).getHeight(), 0.1D);
    }
}
