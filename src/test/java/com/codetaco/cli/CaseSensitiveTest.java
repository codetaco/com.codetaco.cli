package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CaseSensitiveTest {
    static public class Cfg {
        @Arg(inList = {"lower", "UPPER"})
        String lowerCaseResult;

        @Arg(inList = {"lower", "UPPER"}, caseSensitive = true)
        String caseMattersResult;
    }

    @Test
    public void caseDoesNotMatter1() {
        Cfg target = new Cfg();

        CmdLine.builder()
          .target(target)
          .args("--lowerCaseResult LOWER")
          .build();

        Assertions.assertEquals("lower", target.lowerCaseResult);
    }

    @Test
    public void caseDoesNotMatter2() {
        Cfg target = new Cfg();

        CmdLine.builder()
          .target(target)
          .args("--lowerCaseResult Lower")
          .build();

        Assertions.assertEquals("lower", target.lowerCaseResult);
    }

    @Test
    public void caseDoesNotMatter3() {
        Cfg target = new Cfg();

        CmdLine.builder()
          .target(target)
          .args("--lowerCaseResult UPPER")
          .build();

        Assertions.assertEquals("upper", target.lowerCaseResult);
    }

    @Test
    public void caseDoesNotMatter4() {
        Cfg target = new Cfg();

        CmdLine.builder()
          .target(target)
          .args("--lowerCaseResult upper")
          .build();

        Assertions.assertEquals("upper", target.lowerCaseResult);
    }

    @Test
    public void caseMattersValid1() {
        Cfg target = new Cfg();

        CmdLine.builder()
          .target(target)
          .args("--caseMattersResult lower")
          .build();

        Assertions.assertEquals("lower", target.caseMattersResult);
    }

    @Test
    public void caseMattersValid2() {
        Cfg target = new Cfg();

        CmdLine.builder()
          .target(target)
          .args("--caseMattersResult UPPER")
          .build();

        Assertions.assertEquals("UPPER", target.caseMattersResult);
    }

    @Test
    public void extendingPartialSelectionWhenCaseDoesNotMatter() {
        Cfg target = new Cfg();

        CmdLine.builder()
          .target(target)
          .args("--lowerCaseResult LOW")
          .build();

        Assertions.assertEquals("lower", target.lowerCaseResult);
    }

    @Test
    public void extendingPartialSelectionWhenCaseMatters() {
        Cfg target = new Cfg();

        CmdLine.builder()
          .target(target)
          .args("--caseMattersResult up")
          .build();

        Assertions.assertEquals("UPPER", target.caseMattersResult);
    }
}
