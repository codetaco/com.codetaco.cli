package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.util.List;

public class URLTest {

    @Arg()
    private URL url;

    @Arg()
    private URL[] urlArray;

    @Arg()
    private List<URL> urlList;

    @Test
    public void specifyURL() {
        String urlAddress = "http://ip.jsontest.com/";

        CmdLine.builder()
          .target(this)
          .args("--url")
          .args(urlAddress)
          .build();

        Assertions.assertNotNull(url);
        Assertions.assertEquals(urlAddress, url.toString());
    }

    @Test
    public void specifyURLArray() {
        String urlAddress1 = "http://ip.jsontest.com/";
        String urlAddress2 = "http://date.jsontest.com/";

        CmdLine.builder()
          .target(this)
          .args("--urlArray")
          .args(urlAddress1)
          .args(urlAddress2)
          .build();

        Assertions.assertNotNull(urlArray);
        Assertions.assertEquals(2, urlArray.length);
        Assertions.assertEquals(urlAddress1, urlArray[0].toString());
        Assertions.assertEquals(urlAddress2, urlArray[1].toString());
    }

    @Test
    public void specifyURLList() {
        String urlAddress1 = "http://ip.jsontest.com/";
        String urlAddress2 = "http://date.jsontest.com/";

        CmdLine.builder()
          .target(this)
          .args("--urlList")
          .args(urlAddress1)
          .args(urlAddress2)
          .build();

        Assertions.assertNotNull(urlList);
        Assertions.assertEquals(2, urlList.size());
        Assertions.assertEquals(urlAddress1, urlList.get(0).toString());
        Assertions.assertEquals(urlAddress2, urlList.get(1).toString());
    }
}
