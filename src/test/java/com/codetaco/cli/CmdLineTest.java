package com.codetaco.cli;

import com.codetaco.cli.CmdLineTest.GroupMaster.Grouping;
import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CmdLineTest {
    static public class DoubleDash {
        @Arg(shortName = 'a')
        boolean avar;

        @Arg(shortName = 'b')
        boolean bee;
    }

    static class DoubleParm {
        @Arg(required = true)
        double var1;

        @Arg(multimax = 3, range = {"-15", "3000"})
        double[] var2;
    }

    static public class FloatParm {
        @Arg(required = true)
        float var1;

        @Arg(multimax = 3, range = {"-15", "3000"})
        float[] var2;
    }

    public static class GroupMaster {
        public static class Grouping {
            public static class GroupA {
                @Arg(shortName = 'a')
                boolean item1;
                @Arg(shortName = 'b')
                boolean item2;
            }

            @Arg(shortName = 'g')
            GroupA groupa;

            @Arg(shortName = 's')
            boolean itemS;
        }

        @Arg(shortName = 'm')
        Grouping master;
    }

    static final class IntCfg {
        @Arg(shortName = 'i', required = true)
        int int1;

        @Arg(shortName = 'm', required = true, multimax = 2, range = {"1", "3000"})
        int[] int2;
    }

    static final class LongCfg {
        @Arg(shortName = 'i', required = true)
        long long1;

        @Arg(shortName = 'm', required = true, multimax = 2, range = {"1", "3000"})
        long[] long2;
    }

    static public class NameValidation {
        @Arg(shortName = '1')
        boolean invalid;
    }

    static public class NameValidation1 {
        @Arg(longName = "1word")
        boolean invalid;
    }

    static public class Parmed {
        @Arg(shortName = 'i', required = true)
        String inputfile;

        @Arg(shortName = 'e')
        boolean ebcdic;

        @Arg(shortName = 'x')
        boolean dup1;

        @Arg(shortName = 'd')
        String[] dup2;
    }

    static public class PatternMatching {
        @Arg(shortName = 'p', matches = "one|two")
        String stringVar;

        @Arg(shortName = 'q', matches = "one|two")
        String[] stringArray;
    }

    static public class PositionBool {

        @Arg(shortName = 'i')
        String infile;

        @Arg(shortName = 'a')
        boolean p1;

        @Arg(shortName = 's', positional = true)
        String[] outfile;

        @Arg(shortName = 'b')
        boolean p2;
    }

    static public class StringCfg {
        @Arg(shortName = 'i')
        String[] stringArray;

        @Arg(shortName = 'x')
        String stringVar;

        @Arg
        boolean word1;
    }

    static public class TestBoolean {
        @Arg(shortName = 'i')
        boolean inputfile;

        @Arg(shortName = 'e')
        boolean ebcdic;

        @Arg(shortName = 'x')
        boolean dup1;

        @Arg(shortName = 'd')
        boolean dup2;

        @Arg(defaultValues = "true")
        boolean dup3;

        @Arg(longName = "typedefs-and-c++")
        boolean varForCPlusPlus;
    }

    @Test
    public void booleanConcats1() {
        TestBoolean target = new TestBoolean();

        CmdLine cl = CmdLine.builder()
                       .target(target)
                       .args("-i -e")
                       .build();

        Assertions.assertTrue(cl.isParsed("-i"));
        Assertions.assertTrue(cl.isParsed("-e"));
        Assertions.assertFalse(cl.isParsed("--NotAParam"));

        Assertions.assertTrue(target.inputfile);
        Assertions.assertTrue(target.ebcdic);
        Assertions.assertFalse(target.dup1);
        Assertions.assertFalse(target.dup2);
    }

    @Test
    public void booleanConcats2() {
        TestBoolean target = new TestBoolean();

        CmdLine.builder()
          .target(target)
          .args("--input --eb")
          .build();

        Assertions.assertTrue(target.inputfile);
        Assertions.assertTrue(target.ebcdic);
        Assertions.assertFalse(target.dup1);
        Assertions.assertFalse(target.dup2);
    }

    @Test
    public void booleanConcats3() {
        TestBoolean target = new TestBoolean();

        CmdLine.builder()
          .target(target)
          .args("--in -xe")
          .build();

        Assertions.assertTrue(target.inputfile);
        Assertions.assertTrue(target.ebcdic);
        Assertions.assertTrue(target.dup1);
        Assertions.assertFalse(target.dup2);
    }

    @Test
    public void booleanConcats4() {
        TestBoolean target = new TestBoolean();

        CmdLine.builder()
          .target(target)
          .args("-d")
          .build();

        Assertions.assertFalse(target.inputfile);
        Assertions.assertFalse(target.ebcdic);
        Assertions.assertFalse(target.dup1);
        Assertions.assertTrue(target.dup2);
    }

    @Test
    public void booleanConcats5() {
        TestBoolean target = new TestBoolean();

        CmdLine.builder()
          .target(target)
          .args("-dxie")
          .build();

        Assertions.assertTrue(target.inputfile);
        Assertions.assertTrue(target.ebcdic);
        Assertions.assertTrue(target.dup1);
        Assertions.assertTrue(target.dup2);
    }

    @Test
    public void cPlusPlusName() {
        TestBoolean target = new TestBoolean();

        CmdLine.builder()
          .target(target)
          .args("--typedefs-and-c++")
          .build();

        Assertions.assertTrue(target.varForCPlusPlus);
    }

    @Test
    public void dashDoubleDash1() {
        DoubleDash target = new DoubleDash();

        CmdLine.builder()
          .target(target)
          .args("-ab")
          .build();

        Assertions.assertTrue(target.avar);
        Assertions.assertTrue(target.bee);
    }

    @Test
    public void dashDoubleDash2() {
        DoubleDash target = new DoubleDash();

        CmdLine.builder()
          .target(target)
          .args("-a--bee")
          .build();

        Assertions.assertTrue(target.avar);
        Assertions.assertTrue(target.bee);
    }

    @Test
    public void dashDoubleDash3() {
        DoubleDash target = new DoubleDash();

        CmdLine.builder()
          .target(target)
          .args("--bee -a")
          .build();

        Assertions.assertTrue(target.avar);
        Assertions.assertTrue(target.bee);
    }

    @Test
    public void equalsOnCharCommand() {
        StringCfg target = new StringCfg();

        CmdLine.builder()
          .target(target)
          .args("-x=abc -i=def,xyz")
          .build();

        Assertions.assertEquals("abc", target.stringVar);
        Assertions.assertEquals("def", target.stringArray[0]);
        Assertions.assertEquals("xyz", target.stringArray[1]);
    }

    @Test
    public void equalsOnWordCommands1() {
        StringCfg target = new StringCfg();

        CmdLine.builder()
          .target(target)
          .args("--stringVar=abc")
          .build();

        Assertions.assertEquals("abc", target.stringVar);
    }

    @Test
    public void equalsOnWordCommands2() {
        StringCfg target = new StringCfg();

        CmdLine.builder()
          .target(target)
          .args("--stringVar=abc--stringArray=def,xyz")
          .build();

        Assertions.assertEquals("abc", target.stringVar);
        Assertions.assertEquals("def", target.stringArray[0]);
        Assertions.assertEquals("xyz", target.stringArray[1]);
    }

    @Test
    public void grouping() {
        Grouping target = new Grouping();

        CmdLine cl = CmdLine.builder()
                       .target(target)
                       .args("-g ( -a -b )")
                       .build();

        Assertions.assertTrue(cl.isParsed("-g"));
        Assertions.assertTrue(cl.subparser("-g").isParsed("-a"));
        Assertions.assertTrue(cl.subparser("-g").isParsed("-b"));

        Assertions.assertTrue(target.groupa.item1);
        Assertions.assertTrue(target.groupa.item2);
    }

    @Test
    public void groupingGroups() {
        GroupMaster target = new GroupMaster();

        CmdLine.builder()
          .target(target)
          .args("-m(-s-g(-ab))")
          .build();

        Assertions.assertTrue(target.master.itemS);
        Assertions.assertTrue(target.master.groupa.item1);
        Assertions.assertTrue(target.master.groupa.item2);
    }

    @Test
    public void integers() {
        IntCfg target = new IntCfg();

        CmdLine.builder()
          .target(target)
          .args("-i 1 -m1 '2026'")
          .build();

        Assertions.assertEquals(1, target.int1);
        Assertions.assertEquals(2, target.int2.length);
        Assertions.assertEquals(1, target.int2[0]);
        Assertions.assertEquals(2026, target.int2[1]);
    }

    @Test
    public void longs() {
        LongCfg target = new LongCfg();

        CmdLine.builder()
          .target(target)
          .args("-i 1 -m1 '2026'")
          .build();

        Assertions.assertEquals(1, target.long1);
        Assertions.assertEquals(2, target.long2.length);
        Assertions.assertEquals(1, target.long2[0]);
        Assertions.assertEquals(2026, target.long2[1]);
    }

    @Test
    public void multiValuedString1() {
        StringCfg target = new StringCfg();

        CmdLine.builder()
          .target(target)
          .args("-i i1")
          .build();

        Assertions.assertEquals("i1", target.stringArray[0]);
    }

    @Test
    public void multiValuedString2() {
        StringCfg target = new StringCfg();

        CmdLine.builder()
          .target(target)
          .args("-i i1 i2")
          .build();

        Assertions.assertEquals("i1", target.stringArray[0]);
        Assertions.assertEquals("i2", target.stringArray[1]);
    }

    @Test
    public void multiValuedString3() {
        StringCfg target = new StringCfg();

        CmdLine.builder()
          .target(target)
          .args("-i i1 i2 -x x1")
          .build();

        Assertions.assertEquals("i1", target.stringArray[0]);
        Assertions.assertEquals("i2", target.stringArray[1]);
        Assertions.assertEquals("x1", target.stringVar);
    }

    @Test
    public void numberInAKeyWordIsOK() {
        try {
            StringCfg target = new StringCfg();

            CmdLine.builder()
              .target(target)
              .args("--word1")
              .build();

        } catch (Exception p) {
            Assertions.fail("should have allowed a digit after the 1st char in a key word: " + p.getMessage());
        }
    }

    @Test
    public void numericKeyCharIsBad() {
        try {
            NameValidation target = new NameValidation();

            CmdLine.builder()
              .target(target)
              .args("--usage")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception p) {
            Assertions.assertEquals("The Key Character can not be a digit '1'", p.getMessage());
        }
    }

    @Test
    public void numericKeyCharIsBad2() {
        try {
            NameValidation1 target = new NameValidation1();

            CmdLine.builder()
              .target(target)
              .args("--usage")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception p) {
            Assertions.assertEquals("The first character of a Key Word can not be a digit \"1word\"", p.getMessage());
        }
    }

    @Test
    public void parmed1() {
        Parmed target = new Parmed();

        CmdLine.builder()
          .target(target)
          .args("-i infile")
          .build();

        Assertions.assertEquals("infile", target.inputfile);
        Assertions.assertFalse(target.ebcdic);
        Assertions.assertFalse(target.dup1);
        Assertions.assertNull(target.dup2);
    }

    @Test
    public void parmed2() {
        Parmed target = new Parmed();

        CmdLine.builder()
          .target(target)
          .args("-i infile -x")
          .build();

        Assertions.assertEquals("infile", target.inputfile);
        Assertions.assertFalse(target.ebcdic);
        Assertions.assertTrue(target.dup1);
        Assertions.assertNull(target.dup2);
    }

    @Test
    public void parmed3() {
        Parmed target = new Parmed();

        CmdLine.builder()
          .target(target)
          .args("-xi infile")
          .build();

        Assertions.assertEquals("infile", target.inputfile);
        Assertions.assertFalse(target.ebcdic);
        Assertions.assertTrue(target.dup1);
        Assertions.assertNull(target.dup2);
    }

    @Test
    public void parmed4() {
        Parmed target = new Parmed();

        CmdLine.builder()
          .target(target)
          .args("-xiinfile")
          .build();

        Assertions.assertEquals("infile", target.inputfile);
        Assertions.assertFalse(target.ebcdic);
        Assertions.assertTrue(target.dup1);
        Assertions.assertNull(target.dup2);
    }

    @Test
    public void patternMatching1() {
        PatternMatching target = new PatternMatching();

        CmdLine.builder()
          .target(target)
          .args("-ptwo")
          .build();

        Assertions.assertEquals("two", target.stringVar);
        Assertions.assertNull(target.stringArray);
    }

    @Test
    public void patternMatching2() {
        PatternMatching target = new PatternMatching();

        CmdLine.builder()
          .target(target)
          .args("-q two one")
          .build();

        Assertions.assertNull(target.stringVar);
        Assertions.assertEquals("two", target.stringArray[0]);
        Assertions.assertEquals("one", target.stringArray[1]);
    }

    @Test
    public void positionalBooleans() {
        PositionBool target = new PositionBool();

        CmdLine.builder()
          .target(target)
          .args("-a what -b -i when where")
          .build();

        Assertions.assertTrue(target.p1);
        Assertions.assertTrue(target.p2);
        Assertions.assertEquals("what", target.outfile[0]);
        Assertions.assertEquals("where", target.outfile[1]);
        Assertions.assertEquals("when", target.infile);
    }

    @Test
    public void testBoolean1() {
        TestBoolean target = new TestBoolean();

        CmdLine.builder()
          .target(target)
          .args("-i")
          .build();

        Assertions.assertTrue(target.inputfile);
        Assertions.assertFalse(target.ebcdic);
        Assertions.assertFalse(target.dup1);
        Assertions.assertFalse(target.dup2);
        Assertions.assertTrue(target.dup3);
    }

    @Test
    public void testBoolean2() {
        TestBoolean target = new TestBoolean();

        CmdLine.builder()
          .target(target)
          .args("-exi--dup3")
          .build();

        Assertions.assertTrue(target.inputfile);
        Assertions.assertTrue(target.ebcdic);
        Assertions.assertTrue(target.dup1);
        Assertions.assertFalse(target.dup2);
        Assertions.assertFalse(target.dup3);
    }

    @Test
    public void testDouble() {
        DoubleParm target = new DoubleParm();

        CmdLine.builder()
          .target(target)
          .args("--var1 1.0 --var2 1 2026.25 -14.355")
          .build();

        Assertions.assertEquals(1.0, target.var1, 0.01);
        Assertions.assertEquals(1.0, target.var2[0], 0.01);
        Assertions.assertEquals(2026.25, target.var2[1], 0.01);
        Assertions.assertEquals(-14.355, target.var2[2], 0.01);
    }

    @Test
    public void testFloat() {
        FloatParm target = new FloatParm();

        CmdLine.builder()
          .target(target)
          .args("--var1 1.0 --var2 1 2026.25 -14.355")
          .build();

        Assertions.assertEquals(1.0, target.var1, 0.01);
        Assertions.assertEquals(1.0, target.var2[0], 0.01);
        Assertions.assertEquals(2026.25, target.var2[1], 0.01);
        Assertions.assertEquals(-14.355, target.var2[2], 0.01);
    }
}
