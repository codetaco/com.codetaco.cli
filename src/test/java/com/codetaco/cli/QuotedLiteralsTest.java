package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class QuotedLiteralsTest {
    @Arg(shortName = 'a', caseSensitive = true)
    String[] string;

    @Arg(shortName = 'i', range = {"-5", "5"})
    private int intA;

    @Arg(shortName = 'f', range = {"-100", "-50"})
    private float floatA;

    @Test
    public void dosFileNames() {

        CmdLine.builder()
          .target(this)
          .args("-a c:\\temp\\somefile.txt\"")
          .build();

        Assertions.assertEquals("c:\\temp\\somefile.txt", string[0]);
    }

    @Test
    public void dosFileNames2() {

        CmdLine.builder()
          .target(this)
          .args("-a \"c:\\temp\\somefile.txt\"")
          .build();

        Assertions.assertEquals("c:\\temp\\somefile.txt", string[0]);
    }

    @Test
    public void doubleQuotes() {

        CmdLine.builder()
          .target(this)
          .args("-a \"quoted literal\"")
          .build();

        Assertions.assertEquals("quoted literal", string[0]);
    }

    @Test
    public void doubleQuotesFromStream() {
        File test = new File("src/test/java/com/codetaco/cli/QuoteTestData");
        try (BufferedReader in = new BufferedReader(new FileReader(test))) {
            String line = null;
            line = in.readLine();
            Assertions.assertEquals("--string 'echo \"This is a quoted string in a command\"'", line);

            CmdLine.builder()
              .target(this)
              .args(line)
              .build();

            Assertions.assertEquals(
              "echo \"This is a quoted string in a command\"",
              string[0]);
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    @Test
    public void negative1() {

        CmdLine.builder()
          .target(this)
          .args("-i '-5' -f '-50'")
          .build();

        Assertions.assertEquals(-5, intA);
        Assertions.assertEquals(-50, floatA);
    }

    @Test
    public void newLine() {

        CmdLine.builder()
          .target(this)
          .args("-a 'quoted\nliteral'")
          .build();

        Assertions.assertEquals("quoted\nliteral", string[0]);
    }

    @Test
    public void quotesInQuotes() {

        CmdLine.builder()
          .target(this)
          .args("-a '\"quoted literal\"', \"'quoted literal'\"")
          .build();

        Assertions.assertEquals("\"quoted literal\"", string[0]);
        Assertions.assertEquals("'quoted literal'", string[1]);
    }

    @Test
    public void singleQuotes() {

        CmdLine.builder()
          .target(this)
          .args("-a 'quoted literal'")
          .build();

        Assertions.assertEquals("quoted literal", string[0]);
    }

    @Test
    public void stringMultipleQuotes() {

        CmdLine.builder()
          .target(this)
          .args("-a'what''when'")
          .build();

        Assertions.assertEquals("what", string[0]);
        Assertions.assertEquals("when", string[1]);
    }

    @Test
    public void unixFileNames() {

        CmdLine.builder()
          .target(this)
          .args("-a/etc/apache2/conf/httpd.conf")
          .build();

        Assertions.assertEquals("/etc/apache2/conf/httpd.conf", string[0]);
    }

    @Test
    public void urlFileNames() {

        CmdLine.builder()
          .target(this)
          .args("-a http://www.littlegraycould.com/index.html 'http://www.littlegraycould.com/index.html?a=1&b=2'")
          .build();

        Assertions.assertEquals("http://www.littlegraycould.com/index.html", string[0]);
        Assertions.assertEquals("http://www.littlegraycould.com/index.html?a=1&b=2", string[1]);
    }
}
