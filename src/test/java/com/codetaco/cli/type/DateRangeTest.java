package com.codetaco.cli.type;

import com.codetaco.cli.CmdLine;
import com.codetaco.cli.annotation.Arg;
import com.codetaco.date.CalendarFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Month;
import java.time.ZonedDateTime;
import java.util.List;

public class DateRangeTest {

    static private void assertEndDate(DateRange toBeVerified,
                                      int year,
                                      Month month,
                                      int date) {
        Assertions.assertEquals(year, toBeVerified.getEndDate().getYear());
        Assertions.assertEquals(month, toBeVerified.getEndDate().getMonth());
        Assertions.assertEquals(date, toBeVerified.getEndDate().getDayOfMonth());
    }

    static private void assertStartDate(DateRange toBeVerified,
                                        int year,
                                        Month month,
                                        int date) {
        Assertions.assertEquals(year, toBeVerified.getStartDate().getYear());
        Assertions.assertEquals(month, toBeVerified.getStartDate().getMonth());
        Assertions.assertEquals(date, toBeVerified.getStartDate().getDayOfMonth());
    }

    @Arg
    private DateRange drArg;

    @Arg(caseSensitive = false, defaultValues = "now")
    private DateRange drForHelp;

    @Arg
    private DateRange[] drArray;

    @Arg
    private List<DateRange> drList;

    @Test
    public void arrays() {

        CmdLine.builder()
          .target(this)
          .args("--drArray (-s'2016-08-06')(-s'2016-08-06T14:18Z')")
          .build();

        assertStartDate(drArray[0], 2016, Month.AUGUST, 6);
        assertStartDate(drArray[1], 2016, Month.AUGUST, 6);
    }

    @Test
    public void helpDefaultNow() {

        CmdLine.builder()
          .target(this)
          .args("--help")
          .build();

    }

    @Test
    public void json() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-s'2016-03-24T11:19:52.000-05:00')")
          .build();

        assertStartDate(drArg, 2016, Month.MARCH, 24);
    }

    @Test
    public void lists() {

        CmdLine.builder()
          .target(this)
          .args("--drList (-s'2016-08-06')(-s'2016-08-06T14:18Z')")
          .build();

        assertStartDate(drList.get(0), 2016, Month.AUGUST, 6);
        assertStartDate(drList.get(1), 2016, Month.AUGUST, 6);
    }

    @Test
    public void containsDate() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-s 04-09-1960 -e 05-09-1960)")
          .build();

        ZonedDateTime ldt = CalendarFactory.asZoned("04-10-1960");
        Assertions.assertTrue(drArg.contains(CalendarFactory.asDate(ldt)));
    }

    @Test
    public void containsLocalDateTime() {
        ZonedDateTime ldt = CalendarFactory.asZoned("04-10-1960");

        CmdLine.builder()
          .target(this)
          .args("--drArg (-s 04-09-1960 -e 05-09-1960)")
          .build();

        Assertions.assertTrue(drArg.contains(ldt));
    }

    @Test
    public void containsLocalDate() {
        ZonedDateTime ldt = CalendarFactory.asZoned("04-10-1960");

        CmdLine.builder()
          .target(this)
          .args("--drArg (-s 04-09-1960 -e 05-09-1960)")
          .build();

        Assertions.assertTrue(drArg.contains(ldt.toLocalDate()));
    }

    @Test
    public void containsString() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-s 04-09-1960 -e 05-09-1960)")
          .build();

        Assertions.assertTrue(drArg.contains("04-10-1960"));
    }

    @Test
    public void containsFalseBefore() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-s 04-09-1960 -e 05-09-1960)")
          .build();

        Assertions.assertFalse(drArg.contains("04-08-1960"));
    }

    @Test
    public void containsFalseAfter() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-s 04-09-1960 -e 05-09-1960)")
          .build();

        Assertions.assertFalse(drArg.contains("05-10-1960"));
    }

    @Test
    public void containsInclusiveStart() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-s 04-09-1960 -e 05-09-1960)")
          .build();

        Assertions.assertTrue(drArg.contains("04-09-1960"));
    }

    @Test
    public void containsInclusiveEnd() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-s 04-09-1960 -e 05-09-1960)")
          .build();

        Assertions.assertTrue(drArg.contains("05-09-1960"));
    }

    @Test
    public void offOffOffOff() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--drArg ( )")
              .build();

            Assertions.fail("expected failure");
        } catch (Exception e) {
            Assertions.assertEquals("Start or End date must be set on a DateRange", e.getMessage());
        }
    }

    @Test
    public void offOffOnOff() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-e '2016-09-01' )")
          .build();

        assertStartDate(drArg, 2016, Month.SEPTEMBER, 1);
        assertEndDate(drArg, 2016, Month.SEPTEMBER, 1);
    }

    @Test
    public void offOffOnOn() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-e '2016-09-01' --EA '+5days')")
          .build();

        assertStartDate(drArg, 2016, Month.SEPTEMBER, 6);
        assertEndDate(drArg, 2016, Month.SEPTEMBER, 6);
    }

    @Test
    public void offOnOffOff() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--drArg (--SA '+5days' )")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals("Start or End date must be set on a DateRange", e.getMessage());
        }
    }

    @Test
    public void offOnOffOn() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--drArg (--SA '+5days' --EA '+1d' )")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals("Start or End date must be set on a DateRange", e.getMessage());
        }
    }

    @Test
    public void offOnOnOff() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (--eA '+5days' -s '2016-04-01' )")
          .build();

        assertEndDate(drArg, 2016, Month.APRIL, 6);
        assertStartDate(drArg, 2016, Month.APRIL, 1);
    }

    @Test
    public void offOnOnOn() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (--eA '+5days' -s '2016-04-01' --sA '-5month')")
          .build();

        assertEndDate(drArg, 2015, Month.NOVEMBER, 6);
        assertStartDate(drArg, 2015, Month.NOVEMBER, 1);
    }

    @Test
    public void onOffOffOff() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-s '2016-09-01')")
          .build();

        assertStartDate(drArg, 2016, Month.SEPTEMBER, 1);
        assertEndDate(drArg, 2016, Month.SEPTEMBER, 1);
    }

    @Test
    public void onOffOffOn() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-e '2016-09-01' --sA '-1month')")
          .build();

        assertEndDate(drArg, 2016, Month.SEPTEMBER, 1);
        assertStartDate(drArg, 2016, Month.AUGUST, 1);
    }

    @Test
    public void onOffOnOff() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-e '2016-09-01' -s '2016-04-01')")
          .build();

        assertEndDate(drArg, 2016, Month.SEPTEMBER, 1);
        assertStartDate(drArg, 2016, Month.APRIL, 1);
    }

    @Test
    public void onOffOnOn() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-e '2016-09-01' -s '2016-04-01' --sA '-5month')")
          .build();

        assertEndDate(drArg, 2016, Month.SEPTEMBER, 1);
        assertStartDate(drArg, 2015, Month.NOVEMBER, 1);
    }

    @Test
    public void onOnOffOff() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-s '2016-09-01' --SA '+5days')")
          .build();

        assertStartDate(drArg, 2016, Month.SEPTEMBER, 6);
        assertEndDate(drArg, 2016, Month.SEPTEMBER, 6);
    }

    @Test
    public void onOnOffOn() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-e '2016-09-01' --eA '+5days'  --sA '-5month')")
          .build();

        assertEndDate(drArg, 2016, Month.SEPTEMBER, 6);
        assertStartDate(drArg, 2016, Month.APRIL, 6);
    }

    @Test
    public void onOnOnOff() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-e '2016-09-01' --eA '+5days' -s '2016-04-01')")
          .build();

        assertEndDate(drArg, 2016, Month.SEPTEMBER, 6);
        assertStartDate(drArg, 2016, Month.APRIL, 1);
    }

    @Test
    public void onOnOnOn() {

        CmdLine.builder()
          .target(this)
          .args("--drArg (-e '2016-09-01' --eA '+5days' -s '2016-04-01' --sA '-5month')")
          .build();

        assertEndDate(drArg, 2016, Month.SEPTEMBER, 6);
        assertStartDate(drArg, 2015, Month.NOVEMBER, 1);
    }

    @Test
    public void onOnOnOnError() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--drArg (-s '2016-09-01' --SA '+5days' -e '2016-04-01' --EA '-5month')")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals("start date must be <= end date in a DateRange", e.getMessage());
        }
    }
}
