package com.codetaco.cli.type;

import com.codetaco.cli.CmdLine;
import com.codetaco.cli.annotation.Arg;
import com.codetaco.date.CalendarFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

public class TimeRangeTest {

    @BeforeEach
    public void setZoneToLocalTime() {
        CalendarFactory.setZoneId(ZoneOffset.ofHours(-5));
    }

    @AfterEach
    public void setZoneToUTC() {
        CalendarFactory.setZoneId(ZoneOffset.UTC);
    }

    static private void assertEndTime(TimeRange toBeVerified,
                                      int hour,
                                      int minute,
                                      int second) {
        Assertions.assertEquals(hour, toBeVerified.getEndTime().getHour());
        Assertions.assertEquals(minute, toBeVerified.getEndTime().getMinute());
        Assertions.assertEquals(second, toBeVerified.getEndTime().getSecond());
    }

    static private void assertStartTime(TimeRange toBeVerified,
                                        int hour,
                                        int minute,
                                        int second) {
        Assertions.assertEquals(hour, toBeVerified.getStartTime().getHour());
        Assertions.assertEquals(minute, toBeVerified.getStartTime().getMinute());
        Assertions.assertEquals(second, toBeVerified.getStartTime().getSecond());
    }

    @Arg
    private TimeRange trArg;

    @Arg(caseSensitive = false, defaultValues = "now")
    private TimeRange trForHelp;

    @Arg
    private TimeRange[] trArray;

    @Arg
    private List<TimeRange> trList;

    @Test
    public void arrays() {

        CmdLine.builder()
          .target(this)
          .args("--trArray (-s 11:21:01)(-s 11:21:02)")
          .build();

        assertStartTime(trArray[0], 11, 21, 01);
        assertStartTime(trArray[1], 11, 21, 02);
    }

    @Test
    public void containsFalseAfter() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-s 10:38:09 -e 12:38:09)")
          .build();

        Assertions.assertFalse(trArg.contains("12:59:00"));
    }

    @Test
    public void containsFalseBefore() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-s 10:38:09 -e 12:38:09)")
          .build();

        Assertions.assertFalse(trArg.contains("01:01:01"));
    }

    @Test
    public void containsInclusiveEnd() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-s 10:38:09 -e 12:38:09)")
          .build();

        Assertions.assertTrue(trArg.contains("12:38:09"));
    }

    @Test
    public void containsInclusiveStart() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-s 10:38:09 -e 12:38:09)")
          .build();

        Assertions.assertTrue(trArg.contains("10:38:09"));
    }

    @Test
    public void containsLocalTime() {
        ZonedDateTime ldt = CalendarFactory.asZoned("11:38:09");

        CmdLine.builder()
          .target(this)
          .args("--trArg (-s 10:38:09 -e 12:38:09)")
          .build();

        Assertions.assertTrue(trArg.contains(ldt.toLocalTime()));
    }

    @Test
    public void containsLocalDateTime() {
        ZonedDateTime ldt = CalendarFactory.asZoned("11:38:09");

        CmdLine.builder()
          .target(this)
          .args("--trArg (-s 10:38:09 -e 12:38:09)")
          .build();

        Assertions.assertTrue(trArg.contains(ldt));
    }

    @Test
    public void containsZonedDateTime() {
        ZonedDateTime ldt = CalendarFactory.asZoned("11:38:09");

        CmdLine.builder()
          .target(this)
          .args("--trArg (-s 10:38:09 -e 12:38:09)")
          .build();

        Assertions.assertTrue(trArg.contains(ldt));
    }

    @Test
    public void containsString() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-s 10:38:09 -e 12:38:09)")
          .build();

        Assertions.assertTrue(trArg.contains("11:38:09"));
    }

    @Test
    public void containsTime() {
        ZonedDateTime ldt = CalendarFactory.asZoned("11:38:09");

        CmdLine.builder()
          .target(this)
          .args("--trArg (-s 10:38:09 -e 12:38:09)")
          .build();

        Assertions.assertTrue(trArg.contains(CalendarFactory.asLocalTime(ldt)));
    }

    @Test
    public void helpDefaultNow() {

        CmdLine.builder()
          .target(this)
          .args("--help")
          .build();

    }

    @Test
    public void json() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-s'2016-03-24T11:19:52.000Z')")
          .build();

        assertStartTime(trArg, 11, 19, 52);
    }

    @Test
    public void lists() {

        CmdLine.builder()
          .target(this)
          .args("--trList (-s 11:21:01)(-s 11:21:02)")
          .build();

        assertStartTime(trList.get(0), 11, 21, 1);
        assertStartTime(trList.get(1), 11, 21, 2);
    }

    @Test
    public void offOffOffOff() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--trArg ( )")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals("Start or End time must be set on a TimeRange", e.getMessage());
        }
    }

    @Test
    public void offOffOnOff() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-e '16:09:01' )")
          .build();

        assertStartTime(trArg, 16, 9, 1);
        assertEndTime(trArg, 16, 9, 1);
    }

    @Test
    public void offOffOnOn() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-e '16:09:01' --EA '+5min')")
          .build();

        assertStartTime(trArg, 16, 14, 1);
        assertEndTime(trArg, 16, 14, 1);
    }

    @Test
    public void offOnOffOff() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--trArg (--SA '+5min' )")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals("Start or End time must be set on a TimeRange", e.getMessage());
        }
    }

    @Test
    public void offOnOffOn() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--trArg (--SA '+5min' --EA '+1d' )")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals("Start or End time must be set on a TimeRange", e.getMessage());
        }
    }

    @Test
    public void offOnOnOff() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (--eA '+5min' -s '16:04:01' )")
          .build();

        assertEndTime(trArg, 16, 9, 1);
        assertStartTime(trArg, 16, 4, 1);
    }

    @Test
    public void offOnOnOn() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (--eA '+5min' -s '16:04:01' --sA '-5hours')")
          .build();

        assertEndTime(trArg, 11, 9, 1);
        assertStartTime(trArg, 11, 4, 1);
    }

    @Test
    public void onOffOffOff() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-s '16:09:01')")
          .build();

        assertStartTime(trArg, 16, 9, 1);
        assertEndTime(trArg, 16, 9, 1);
    }

    @Test
    public void onOffOffOn() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-e '16:09:01' --sA '-1second')")
          .build();

        assertEndTime(trArg, 16, 9, 1);
        assertStartTime(trArg, 16, 9, 0);
    }

    @Test
    public void onOffOnOff() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-e '16:09:01' -s '16:04:01')")
          .build();

        assertEndTime(trArg, 16, 9, 1);
        assertStartTime(trArg, 16, 4, 1);
    }

    @Test
    public void onOffOnOn() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-e '16:09:01' -s '16:04:01' --sA '-5hours')")
          .build();

        assertEndTime(trArg, 16, 9, 1);
        assertStartTime(trArg, 11, 4, 1);
    }

    @Test
    public void onOnOffOff() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-s '16:09:01' --SA '+5min')")
          .build();

        assertStartTime(trArg, 16, 14, 1);
        assertEndTime(trArg, 16, 14, 1);
    }

    @Test
    public void onOnOffOn() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-e '16:09:01' --eA '+5min'  --sA '-5hours')")
          .build();

        assertEndTime(trArg, 16, 14, 1);
        assertStartTime(trArg, 11, 14, 1);
    }

    @Test
    public void onOnOnOff() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-e '16:09:01' --eA '+5min' -s '16:04:01')")
          .build();

        assertEndTime(trArg, 16, 14, 1);
        assertStartTime(trArg, 16, 4, 1);
    }

    @Test
    public void onOnOnOn() {

        CmdLine.builder()
          .target(this)
          .args("--trArg (-e '16:09:01' --eA '+5min' -s '16:04:01' --sA '-5hours')")
          .build();

        assertEndTime(trArg, 16, 14, 1);
        assertStartTime(trArg, 11, 4, 1);
    }

    @Test
    public void onOnOnOnError() {
        try {

            CmdLine.builder()
              .target(this)
              .args("--trArg (-s '16:09:01' --SA '+5min' -e '16:04:01' --EA '-5hours')")
              .build();

        } catch (Exception e) {
            Assertions.assertEquals("start Time must be <= end time in a TimeRange", e.getMessage());
        }
    }
}
