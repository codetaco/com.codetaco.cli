package com.codetaco.cli.type;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class WildPathTest {

    @Test
    public void startingPathAbsolute() throws Exception {
        WildPath wp = new WildPath("/a/b/c/*.txt");
        Assertions.assertEquals("/a/b/c/", wp.startingPath());
    }

    @Test
    public void startingPathAbsoluteWildcards() throws Exception {
        WildPath wp = new WildPath("/**/b/c/*.txt");
        Assertions.assertEquals("/", wp.startingPath());
    }

    @Test
    public void startingPathQuestionmarkWildcards() throws Exception {
        WildPath wp;

        wp = new WildPath("/a/b?c/d/*.txt");
        Assertions.assertEquals("/a/", wp.startingPath());

        wp = new WildPath("/a/??/d/*.txt");
        Assertions.assertEquals("/a/", wp.startingPath());

        wp = new WildPath("log????/*.txt");
        Assertions.assertEquals(".", wp.startingPath());
    }

    @Test
    public void startingPathRelative() throws Exception {
        WildPath wp = new WildPath("a/b/c/*.txt");
        Assertions.assertEquals("a/b/c/", wp.startingPath());
    }

    @Test
    public void startingPathRelativeWildcards() throws Exception {
        WildPath wp = new WildPath("**/b/c/*.txt");
        Assertions.assertEquals(".", wp.startingPath());
    }

    @Test
    public void startingPathStarWildcards() throws Exception {
        WildPath wp;

        wp = new WildPath("/a/b/*/d/*.txt");
        Assertions.assertEquals("/a/b/", wp.startingPath());

        wp = new WildPath("/a/*/*/d/*.txt");
        Assertions.assertEquals("/a/", wp.startingPath());

        wp = new WildPath("/a/**/d/*.txt");
        Assertions.assertEquals("/a/", wp.startingPath());
    }
}
