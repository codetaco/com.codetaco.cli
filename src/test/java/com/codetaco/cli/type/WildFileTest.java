package com.codetaco.cli.type;

import com.codetaco.cli.CmdLine;
import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;

public class WildFileTest {

    @Arg(positional = true, multimin = 1)
    public WildFiles wildFile;

    @Test
    public void patternConversionsNix() {
        Assertions.assertEquals("^.*$",
                                WildPath.convertFileWildCardToRegx("*").pattern());
        Assertions.assertEquals("^.*\\..*$",
                                WildPath.convertFileWildCardToRegx("*.*").pattern());
        Assertions.assertEquals("^.$",
                                WildPath.convertFileWildCardToRegx("?").pattern());
        Assertions.assertEquals("^.*.*$",
                                WildPath.convertFileWildCardToRegx("**").pattern());
        Assertions.assertEquals("^.*.*/.*\\.java$",
                                WildPath.convertFileWildCardToRegx("**/*.java").pattern());
    }

    @Test
    public void patternConversionsNixMatching() {
        Assertions.assertTrue(
          Pattern.matches(WildPath.convertFileWildCardToRegx("*").pattern(),
                          "any.file.name/With.path"));

        Assertions.assertTrue(
          Pattern.matches(WildPath.convertFileWildCardToRegx("*.*").pattern(),
                          "any.file.name/With.path"));

        Assertions.assertFalse(
          Pattern.matches(WildPath.convertFileWildCardToRegx("*.*").pattern(),
                          "anyfilename/Withpath"));

        Assertions.assertTrue(
          Pattern.matches(WildPath.convertFileWildCardToRegx("*/?").pattern(),
                          "any path as long as it is a single char file name/a"));
    }

    @Test
    public void patternConversionsWin() {
        Assertions.assertEquals("^.*$",
                                WildPath.convertFileWildCardToRegx("*").pattern());
        Assertions.assertEquals("^.*\\..*$",
                                WildPath.convertFileWildCardToRegx("*.*").pattern());
        Assertions.assertEquals("^.$",
                                WildPath.convertFileWildCardToRegx("?").pattern());
        Assertions.assertEquals("^.*.*$",
                                WildPath.convertFileWildCardToRegx("**").pattern());
        Assertions.assertEquals("^.*.*\\\\.*\\.java$",
                                WildPath.convertFileWildCardToRegx("**\\\\*.java").pattern());
    }

    @Test
    public void recursiveDirSearchFileNotFound() throws Exception {

        CmdLine.builder()
          .target(this)
          .args("**/*NOTFINDABLE")
          .build();

        Assertions.assertEquals(0, wildFile.files().size());
    }

    @Test
    public void specificDirSearch() throws Exception {

        CmdLine.builder()
          .target(this)
          .args("src/main/java/com/codetaco/cli/*java")
          .build();

        Assertions.assertTrue(0 < wildFile.files().size());
    }

    @Test
    public void validRegex() throws Exception {

        CmdLine.builder()
          .target(this)
          .args("LICENSE READ*")
          .build();

        Assertions.assertEquals(2, wildFile.files().size());
    }

    @Test
    public void wildDirSearch1() throws Exception {

        CmdLine.builder()
          .target(this)
          .args("src/*/java/**/*java")
          .build();

        Assertions.assertTrue(0 < wildFile.files().size());
    }

    @Test
    public void wildDirSearch2() throws Exception {

        CmdLine.builder()
          .target(this)
          .args("**/*java")
          .build();

        Assertions.assertTrue(0 < wildFile.files().size());
    }
}
