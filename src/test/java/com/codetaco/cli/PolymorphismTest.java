package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class PolymorphismTest {
    static abstract public class Animal2 implements IAnimal2 {
        static public IAnimal2 create(String sound) {
            if ("meow".equals(sound)) {
                return new Cat2();
            }
            if ("woof".equals(sound)) {
                return new Dog2();
            }
            return new CatDog2();
        }

        @Arg(required = true)
        private String sound;
    }

    static public class Cat1 implements IAnimal {
    }

    static public class Cat2 extends Animal2 {
    }

    static public class CatDog1 implements IAnimal {
    }

    static public class CatDog2 extends Animal2 {
    }

    public static class Context1 {
        @Arg
        private List<Object> onlyObjects;

        @Arg(instanceClass = "com.codetaco.cli.PolymorphismTest$SpecificInstance")
        private List<Object> specificInstances;

        @Arg(longName = "type2", instanceClass = "com.codetaco.cli.PolymorphismTest$SpecificInstance2")
        @Arg(longName = "type1", instanceClass = "com.codetaco.cli.PolymorphismTest$SpecificInstance")
        private List<Object> twoTypes;

        @Arg(longName = "si2", instanceClass = "com.codetaco.cli.PolymorphismTest$SpecificInstance2")
        @Arg(longName = "si1", instanceClass = "com.codetaco.cli.PolymorphismTest$SpecificInstance")
        private SpecificInstance2 singleInstance1;

        @Arg(longName = "si4", instanceClass = "com.codetaco.cli.PolymorphismTest$SpecificInstance")
        @Arg(longName = "si3", instanceClass = "com.codetaco.cli.PolymorphismTest$SpecificInstance2")
        private SpecificInstance2 singleInstance2;

        @Arg(longName = "si6", instanceClass = "com.codetaco.cli.PolymorphismTest$SpecificInstance2")
        @Arg(longName = "si5", instanceClass = "com.codetaco.cli.PolymorphismTest$SpecificInstance2")
        private SpecificInstance2 singleInstance3;

        @Arg(
          factoryMethod = "create",
          factoryArgName = Arg.SELF_REFERENCING_ARGNAME)
        private List<IAnimal> animals1;

        @Arg(
          factoryMethod = "create",
          factoryArgName = "sound",
          instanceClass = "com.codetaco.cli.PolymorphismTest$Animal2")
        private List<IAnimal2> animals2;

        @Arg(
          factoryMethod = "create",
          factoryArgName = "sound")
        private List<Animal2> animals3;
    }

    static public class Dog1 implements IAnimal {
    }

    static public class Dog2 extends Animal2 {
    }

    static public interface IAnimal {
        static public IAnimal create(String sound) {
            if ("meow".equals(sound)) {
                return new Cat1();
            }
            if ("woof".equals(sound)) {
                return new Dog1();
            }
            return new CatDog1();
        }
    }

    static public interface IAnimal2 {
        static public IAnimal2 create(String sound) {
            if ("meow".equals(sound)) {
                return new Cat2();
            }
            if ("woof".equals(sound)) {
                return new Dog2();
            }
            return new CatDog2();
        }
    }

    public static class SpecificInstance {
        @Arg
        private int typeCode;
    }

    public static class SpecificInstance2 {
        @Arg
        private int typeCode;
    }

    @Test
    public void animalFactory1() {
        Context1 context1 = new Context1();

        CmdLine.builder()
          .target(context1)
          .args("--animals1 woof, meow WHAT!")
          .build();

        Assertions.assertEquals(3, context1.animals1.size());
        Assertions.assertTrue(context1.animals1.get(0) instanceof Dog1);
        Assertions.assertTrue(context1.animals1.get(1) instanceof Cat1);
        Assertions.assertTrue(context1.animals1.get(2) instanceof CatDog1);
    }

    @Test
    public void animalFactory2() {
        Context1 context1 = new Context1();

        CmdLine.builder()
          .target(context1)
          .args("--animals2 (--sound woof) (--sound meow) (--sound WHAT!)")
          .build();

        Assertions.assertEquals(3, context1.animals2.size());
        Assertions.assertTrue(context1.animals2.get(0) instanceof Dog2);
        Assertions.assertTrue(context1.animals2.get(1) instanceof Cat2);
        Assertions.assertTrue(context1.animals2.get(2) instanceof CatDog2);
    }

    @Test
    public void animalFactory3() {
        Context1 context1 = new Context1();

        CmdLine.builder()
          .target(context1)
          .args("--animals3 (--sound woof) (--sound meow) (--sound WHAT!)")
          .build();

        Assertions.assertEquals(3, context1.animals3.size());
        Assertions.assertTrue(context1.animals3.get(0) instanceof Dog2);
        Assertions.assertTrue(context1.animals3.get(1) instanceof Cat2);
        Assertions.assertTrue(context1.animals3.get(2) instanceof CatDog2);
    }

    @Test
    public void errorForImproperAsssignment1() {
        Context1 context1 = new Context1();
        try {

            CmdLine.builder()
              .target(context1)
              .args("--si2(--typeCode 2)  --si1(--typeCode 1)")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception e) {
            /*
             * This error occurs if a subsequent @Arg after the first has a type
             * that can not be stored in the variable, regardless of which order
             * the user specifies them on the command line.
             */
            Assertions.assertEquals(
              "Error in instance creation for \"--si1\", com.codetaco.cli.PolymorphismTest$SpecificInstance2 can not be reassigned to com.codetaco.cli.PolymorphismTest$SpecificInstance",
              e.getMessage());
        }
    }

    @Test
    public void errorForImproperAsssignment2() {
        Context1 context1 = new Context1();
        try {

            CmdLine.builder()
              .target(context1)
              .args("--si4(--typeCode 2)  --si3(--typeCode 1)")
              .build();

            Assertions.fail("expected exception");
        } catch (Exception e) {
            /*
             * This error occurs if the first @Arg has a type that can not be
             * stored in the variable, regardless of which order the user
             * specifies them on the command line.
             */
            Assertions.assertEquals(
              "IllegalArgumentException (singleInstance2)",
              e.getMessage());
        }
    }

    @Test
    public void onlyObjects() {
        Context1 context1 = new Context1();

        CmdLine.builder()
          .target(context1)
          .args("--onlyObjects ()()")
          .build();

        Assertions.assertEquals(2, context1.onlyObjects.size());
        Assertions.assertTrue(context1.onlyObjects.get(0) instanceof Object);
        Assertions.assertTrue(context1.onlyObjects.get(1) instanceof Object);
    }

    @Test
    public void onlySpecificInstances() {
        Context1 context1 = new Context1();

        CmdLine.builder()
          .target(context1)
          .args("--specificInstances(--typeCode 1)(--typeCode 2)")
          .build();

        Assertions.assertEquals(2, context1.specificInstances.size());
        Assertions.assertTrue(context1.specificInstances.get(0) instanceof SpecificInstance);
        Assertions.assertTrue(context1.specificInstances.get(1) instanceof SpecificInstance);
    }

    @Test
    public void twoTypesInAList() {
        Context1 context1 = new Context1();

        CmdLine.builder()
          .target(context1)
          .args("--type1(--typeCode 1) --type2(--typeCode 2)")
          .build();

        Assertions.assertEquals(2, context1.twoTypes.size());
        Assertions.assertTrue(context1.twoTypes.get(1) instanceof SpecificInstance);
        Assertions.assertTrue(context1.twoTypes.get(0) instanceof SpecificInstance2);
    }

    @Test
    public void weirdImproperAsssignment3() {
        Context1 context1 = new Context1();

        CmdLine.builder()
          .target(context1)
          .args("--si6(--typeCode 2)  --si5(--typeCode 1)")
          .build();
    }
}
