package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.math.Equ;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EquTest {
    @Arg(shortName = 'e')
    private Equ equ;

    @Test
    public void parensEquationsWithDot() {

        CmdLine.builder()
          .target(this)
          .args("-e'match(\"a(123)b\", \"a.([0-9]+).b\", 1)'")
          .build();

        Assertions.assertEquals("123", equ.evaluate());
    }

    @Test
    public void parensEquationsWithEscape() {

        CmdLine.builder()
          .target(this)
          .args("-e'matches(\"a(123)b\", \"a\\(([0-9]+)\\)b\")'")
          .build();

        Assertions.assertTrue((Boolean) equ.evaluate());
    }

    @Test
    public void parensEquationsWithSet() {

        CmdLine.builder()
          .target(this)
          .args("-e'contains(\"a(123)b\", \"a[(]([0-9]+)[)]b\")'")
          .build();

        Assertions.assertTrue((Boolean) equ.evaluate());
    }

    @Test
    public void quotedEquation() {

        CmdLine.builder()
          .target(this)
          .args("-e'contains(\"abc123def\", \"[0-9]+\")'")
          .build();

        Assertions.assertTrue((Boolean) equ.evaluate());
    }

    @Test
    public void simpleEquation() {

        CmdLine.builder()
          .target(this)
          .args("-e'2+3'")
          .build();

        Assertions.assertEquals(5, ((Long) equ.evaluate()).intValue());
    }
}
