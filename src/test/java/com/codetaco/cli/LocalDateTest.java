package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.date.CalendarFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.List;

public class LocalDateTest {

    @BeforeEach
    public void setZoneToLocalTime() {
        CalendarFactory.setZoneId(ZoneOffset.ofHours(-5));
    }

    @AfterEach
    public void setZoneToUTC() {
        CalendarFactory.setZoneId(ZoneOffset.UTC);
    }

    static private void assertValidReturnDate(LocalDate toBeVerified,
                                              int year,
                                              Month month,
                                              int _date) {
        Assertions.assertEquals(year, toBeVerified.getYear());
        Assertions.assertEquals(month, toBeVerified.getMonth());
        Assertions.assertEquals(_date, toBeVerified.getDayOfMonth());
    }

    @Arg
    LocalDate ldtArg;

    @Arg(caseSensitive = true, defaultValues = "now")
    LocalDate ldtForHelp;

    @Arg
    LocalDate[] ldtArray;

    @Arg
    List<LocalDate> ldtList;

    @Test
    public void arrays() {

        CmdLine.builder()
          .target(this)
          .args("--ldtArray '2016-08-06' '2016-08-06T14:18-05:00'")
          .build();

        assertValidReturnDate(ldtArray[0], 2016, Month.AUGUST, 6);
        assertValidReturnDate(ldtArray[1], 2016, Month.AUGUST, 6);
    }

    @Test
    public void helpDefaultNow() {

        CmdLine.builder()
          .target(this)
          .args("--help")
          .build();

    }

    @Test
    public void json() {

        CmdLine.builder()
          .target(this)
          .args("--ldtArg '2016-03-24T11:19:52.000-05:30'")
          .build();

        assertValidReturnDate(ldtArg, 2016, Month.MARCH, 24);
    }

    @Test
    public void lists() {

        CmdLine.builder()
          .target(this)
          .args("--ldtList '2016-08-06' '2016-08-06T14:18-05:00'")
          .build();

        assertValidReturnDate(ldtList.get(0), 2016, Month.AUGUST, 6);
        assertValidReturnDate(ldtList.get(1), 2016, Month.AUGUST, 6);
    }

    @Test
    public void simpleUse() {

        CmdLine.builder()
          .target(this)
          .args("--ldtArg '2016-08-06'")
          .build();

        assertValidReturnDate(ldtArg, 2016, Month.AUGUST, 6);
    }
}
