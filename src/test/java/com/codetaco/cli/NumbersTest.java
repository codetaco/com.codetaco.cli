package com.codetaco.cli;

import com.codetaco.cli.annotation.Arg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class NumbersTest {
    @Arg(shortName = 'a')
    int anInt;

    @Arg
    int i100;

    @Arg(longName = "i100-23")
    int i10023;

    @Arg(shortName = 'c', longName = "bee-name")
    int beeName;

    @Test
    public void negativeIntegers() {

        CmdLine.builder()
          .target(this)
          .args("-a -1 --bee-name -2")
          .build();

        Assertions.assertEquals(-1, anInt);
        Assertions.assertEquals(-2, beeName);
    }

    @Test
    public void negativeIntegersViaKeyCharEqualSign() {

        CmdLine.builder()
          .target(this)
          .args("-a=-100")
          .build();

        Assertions.assertEquals(-100, anInt);
    }

    @Test
    public void negativeIntegersViaKeyCharNoDelim() {

        CmdLine.builder()
          .target(this)
          .args("-a-100")
          .build();

        Assertions.assertEquals(-100, anInt);
    }

    @Test
    public void negativeIntegersViaKeyWord() {

        CmdLine.builder()
          .target(this)
          .args("--i100 -23")
          .build();

        Assertions.assertEquals(-23, i100);
    }

    @Test
    public void negativeIntegersViaKeyWordWithEmbeddedDash() {

        CmdLine.builder()
          .target(this)
          .args("--i100-23 -45")
          .build();

        Assertions.assertEquals(-45, i10023);
    }

    @Test
    public void negativeIntegersViaKeyWordWithEmbeddedDashShortened() {

        CmdLine.builder()
          .target(this)
          .args("--i100- -45")
          .build();

        Assertions.assertEquals(-45, i10023);
    }

    @Test
    public void positiveIntegers() {

        CmdLine.builder()
          .target(this)
          .args("-a 1 --bee-name 2")
          .build();

        Assertions.assertEquals(1, anInt);
        Assertions.assertEquals(2, beeName);
    }

    @Test
    public void positiveIntegersNoSpace() {

        CmdLine.builder()
          .target(this)
          .args("-a1 --bee-name 2")
          .build();

        Assertions.assertEquals(1, anInt);
        Assertions.assertEquals(2, beeName);
    }
}
