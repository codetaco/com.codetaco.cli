CommandLineInterface (CLI) by CodeTaco
======================================

.. image:: /images/codetaco.png
   :width: 120px
   :alt: CodeTaco logo
   :align: right

.. image:: /images/appImage.png
   :width: 120px
   :alt: CLI logo
   :align: left

   "CLI" is a library ...

Things to do
------------

`Read the Javadocs <http://www.javadoc.io/doc/com.codetaco/cli>`_

`Get the code to include this in your project's dependencies <https://search.maven.org/search?q=g:com.codetaco%20AND%20a:cli&core=gav>`_


:ref:`Keyword Index <genindex>`, :ref:`Search Page <search>`

See date for a table of contents example
